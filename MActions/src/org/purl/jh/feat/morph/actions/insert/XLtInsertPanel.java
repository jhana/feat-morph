package org.purl.jh.feat.morph.actions.insert;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.SimpleTagset;
import org.purl.jh.util.PairC;
import org.purl.jh.util.col.pred.PrefixFilter;
import org.purl.jh.util.col.pred.StringFilter;
import org.purl.jh.util.gui.SimpleDocListener;
import org.purl.jh.util.gui.list.FilterList;
import org.purl.jh.util.gui.list.FilterListModel;
import org.purl.jh.util.gui.list.FilterSelectionList;
import org.purl.jh.util.gui.list.Lists;

/**
 * Panel allowing to insert a new lemma, tag and possibly comment.
 * 
 * @todo Enter in direct tag editbox should close the dialog
 * @todo check direct checkbox -> focus to the adjacent editbox
 * @author  Jirka Hana
 */
public class XLtInsertPanel extends javax.swing.JPanel {
    private final InsertHistory history;
    private final Lt curLt;
    private final String curMForm;

    /** Tagset to choose tags from; might be null if unknown */
    private SimpleTagset tagset;

    private FilterList<String,PrefixFilter<String>> tagListX;

    /**
     * 
     * @param aForm form used to initialize certain values for easy entry, not modified
     * @param aCurLT lt used to initialize certain values for easy entry, not modified
     * @param aHistory 
     */
    public XLtInsertPanel(LTx aLtx,  Lt aCurLT, InsertHistory aHistory) {
        // --- init data ---
        curMForm = aLtx.getForm();
        curLt = aCurLT;
        history = aHistory;
        tagset = aLtx.getLayer().getTagset();
        
        // --- init gui ---
        initComponents();
        
        connectTagControls();
        
        // --- history ---
        historyList.setModel( Lists.getModel(history.getLTList()) );

        historyList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                
                PairC<String,String> lt = (PairC<String,String>) historyList.getSelectedValue();
                lemmaText.setText(lt.mFirst);

                String tag = lt.mSecond;
                boolean wasDirect = (tagListX.getModel().indexOfInAll(tag) == -1);
                if (allowDirectTagCheck.isSelected() != wasDirect) {
                    allowDirectTagCheck.setSelected(wasDirect);
                    allowDirectTagCheckActionPerformed(null);
                }
                if (wasDirect) {
                    directTagText.setText(tag);
                }
                else {
                    tagText.setText(lt.mSecond);       // filter all other out
                }
            }
        });
    }

    /** Add tagset to tagList, adds filtering of tagList by tagText */
    private void connectTagControls(/*LtInfo aLtInfo*/) {
        tagListX = (FilterList<String,PrefixFilter<String>>) tagList;

        if (tagset == null) {
            this.tagsetFld.setText("Layer has no tagset set");
        }
        else {
            this.tagsetFld.setText(tagset.getId() + " (" + tagset.getDescr() + ")");
        }

        PrefixFilter<String> filter = new PrefixFilter();
        
        List<String> tags = tagset == null ? Collections.emptyList() : new ArrayList<>(tagset.getTags());
        FilterListModel<String,PrefixFilter<String>> model = new FilterListModel(tags);

        model.setFilter(filter);
        enableDirectTagEntry(tags.isEmpty());  // no tagset, direct entry is the only possibility 

        tagListX.setModel(model, true);
        tagListX.setSelectedIndex(0);       

        // send cursor movement keys from the text field to the list
        final KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(
            new KeyEventDispatcher() {
                @Override
                public boolean dispatchKeyEvent(KeyEvent e) {
                    if(e.getSource() == tagText) {
                        int keyCode = e.getKeyCode();
                        if (keyCode == KeyEvent.VK_DOWN || keyCode == KeyEvent.VK_UP ||
                            keyCode == KeyEvent.VK_HOME || keyCode == KeyEvent.VK_END ||
                            keyCode == KeyEvent.VK_PAGE_DOWN || keyCode == KeyEvent.VK_PAGE_UP
                            ) {
                                manager.redispatchEvent(tagList,e);
                        return true;
                        }
                    }

                    return false;
                }
            });

        // filter tag list when tagText is updated
        tagText.getDocument().addDocumentListener( new SimpleDocListener() {
                @Override
                public void textUpdated() {
                    filter.setString(tagText.getText());
                }
            }
        );
    }

    
    
    protected void retrieveInput() {
        history.add(curMForm, getLemma(), getTag());
    }

    protected boolean validateInput() {
        if (getLemma().length() == 0) {
            JOptionPane.showMessageDialog(this, "Error - lemma cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        // @todo the following should not happen
        if (getTag() == null) {
            JOptionPane.showMessageDialog(this, "Error - tag cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public SimpleTagset getTagset() {
        return tagset;
    }
    
    public String getLemma() {
        return lemmaText.getText().trim();
    }
    
    public String getTag() {
        if (allowDirectTagCheck.isSelected()) {
            return directTagText.getText();
        }
        else {
            return tagListX.getCurItem();
        }
    }

    public String getComment() {
        return mCommentText.getText().trim();
    }
    
    private void enableDirectTagEntry(boolean allow) {
        allowDirectTagCheck.setSelected(allow);
        directTagText.setEnabled( allow );
        tagText.setEnabled(!allow);
        tagList.setEnabled(!allow);
        tagHelp.setEnabled(!allow);
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        useFormButton = new javax.swing.JButton();
        useLemmaButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lemmaText = new javax.swing.JTextField();
        useTagButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        tagText = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        historyList = new javax.swing.JList();
        mModelText = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        mCommentText = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tagList = new FilterSelectionList<String,StringFilter<String>>();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane3 = new javax.swing.JScrollPane();
        tagHelp = new javax.swing.JTextArea();
        allowDirectTagCheck = new javax.swing.JCheckBox();
        directTagText = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        tagsetLabel = new javax.swing.JLabel();
        tagsetFld = new javax.swing.JTextField();

        setBackground(javax.swing.UIManager.getDefaults().getColor("ToolBar.light"));

        useFormButton.setMnemonic('F');
        useFormButton.setText("Use Form");
        useFormButton.setToolTipText("Copy the current word to the lemma field");
        useFormButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useFormButtonPressed(evt);
            }
        });

        useLemmaButton.setMnemonic('M');
        useLemmaButton.setText("Use Lemma");
        useLemmaButton.setToolTipText("Copy the current lemma to the lemma field");
        useLemmaButton.setEnabled(false);
        useLemmaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useLemmaButtonPressed(evt);
            }
        });

        jLabel1.setDisplayedMnemonic('L');
        jLabel1.setText("Lemma:");

        lemmaText.setFocusAccelerator('l');

        useTagButton.setMnemonic('a');
        useTagButton.setText("Use Tag");
        useTagButton.setToolTipText("Copy the current tag to the lemma field");
        useTagButton.setEnabled(false);
        useTagButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useTagButtonPressed(evt);
            }
        });

        jLabel3.setDisplayedMnemonic('T');
        jLabel3.setText("Tag:");

        tagText.setToolTipText("Enter tag");
        tagText.setFocusAccelerator('t');

        jLabel4.setDisplayedMnemonic('H');
        jLabel4.setText("History:");

        jScrollPane1.setViewportView(historyList);

        jLabel5.setDisplayedMnemonic('c');
        jLabel5.setText("Comment:");

        mCommentText.setFocusAccelerator('c');
        mCommentText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCommentTextActionPerformed(evt);
            }
        });

        tagList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tagList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                tagListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(tagList);

        tagHelp.setEditable(false);
        tagHelp.setBackground(java.awt.SystemColor.activeCaptionBorder);
        tagHelp.setColumns(20);
        tagHelp.setLineWrap(true);
        tagHelp.setRows(5);
        tagHelp.setWrapStyleWord(true);
        jScrollPane3.setViewportView(tagHelp);

        allowDirectTagCheck.setMnemonic('o');
        allowDirectTagCheck.setText("Enter other tag:");
        allowDirectTagCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allowDirectTagCheckActionPerformed(evt);
            }
        });

        directTagText.setEnabled(false);
        directTagText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                directTagTextActionPerformed(evt);
            }
        });

        tagsetLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        tagsetLabel.setText("Tagset:");

        tagsetFld.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(mCommentText))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 75, Short.MAX_VALUE)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator1)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lemmaText))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane2)
                                    .addComponent(tagText)
                                    .addComponent(jScrollPane3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(allowDirectTagCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(directTagText))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(useFormButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(useLemmaButton)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(useTagButton)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(mModelText, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tagsetLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tagsetFld)))
                        .addGap(24, 24, 24)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {useFormButton, useLemmaButton, useTagButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(useFormButton)
                    .addComponent(jLabel4)
                    .addComponent(useLemmaButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(lemmaText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(mModelText))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(useTagButton)
                                    .addComponent(tagsetLabel)
                                    .addComponent(tagsetFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(tagText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(allowDirectTagCheck)
                            .addComponent(directTagText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(mCommentText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

private void useTagButtonPressed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useTagButtonPressed
    tagText.setText(curLt.getTag());
}//GEN-LAST:event_useTagButtonPressed

private void useFormButtonPressed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useFormButtonPressed
    lemmaText.setText(curMForm);          
}//GEN-LAST:event_useFormButtonPressed

private void useLemmaButtonPressed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useLemmaButtonPressed
    lemmaText.setText(curLt.getLemma());          
}//GEN-LAST:event_useLemmaButtonPressed

private void mCommentTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCommentTextActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_mCommentTextActionPerformed

private void allowDirectTagCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allowDirectTagCheckActionPerformed
    enableDirectTagEntry( allowDirectTagCheck.isSelected() );
}//GEN-LAST:event_allowDirectTagCheckActionPerformed

private void directTagTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_directTagTextActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_directTagTextActionPerformed

private void tagListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_tagListValueChanged
    String curTag = tagListX.getCurItem();
    if (curTag == null) {
        //tagHelp.setText("");
        tagHelp.setText(null);
    }
    else {
        tagHelp.setText("todo help" /*ltInfo.getTagHelper().help(curTag)*/);
    }

}//GEN-LAST:event_tagListValueChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox allowDirectTagCheck;
    private javax.swing.JTextField directTagText;
    private javax.swing.JList historyList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField lemmaText;
    private javax.swing.JTextField mCommentText;
    private javax.swing.JLabel mModelText;
    private javax.swing.JTextArea tagHelp;
    private javax.swing.JList tagList;
    private javax.swing.JTextField tagText;
    private javax.swing.JTextField tagsetFld;
    private javax.swing.JLabel tagsetLabel;
    private javax.swing.JButton useFormButton;
    private javax.swing.JButton useLemmaButton;
    private javax.swing.JButton useTagButton;
    // End of variables declaration//GEN-END:variables

}
