package org.purl.jh.feat.morph.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.NbBundle;
import org.purl.jh.law.nbdata.view.CurrentLt;
import org.purl.jh.feat.morph.actions.insert.XInsertDlg;
import org.purl.jh.law.data.m.Lt;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.da.actions.EditLt")
@ActionRegistration(displayName = "#CTL_EditCur", lazy = false)
@ActionReferences({
    @ActionReference(path = "Menu/Annotate", position = 1100),
    @ActionReference(path = "Shortcuts", name= "D-E"),
})
@NbBundle.Messages("CTL_EditCur=Edit Current Item")
public class EditCur extends CurrentLtAction {
    public EditCur() {
        this(null);
    }
    
    public EditCur(Lookup context) {
        super(EditCur.class, "CTL_EditCur", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new EditCur(context);
    }

    @Override
    protected void actionPerformed(CurrentLt curLt) {
        Lt sampleLt = curLt.getLt();
        
        XInsertDlg dlg = new XInsertDlg(null, sampleLt.getParent(), sampleLt);
        if (!dlg.accepted()) return;

        curLt.getCurrent().getMain().getLayer().setTagset(dlg.getTagset());

        curLt.getCurrent().ltSetLemmaTag(sampleLt, dlg.getLemma(), dlg.getTag());
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        setEnabled(lkpInfo.allInstances().size() == 1);
    }

}