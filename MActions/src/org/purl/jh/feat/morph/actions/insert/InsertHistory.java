package org.purl.jh.feat.morph.actions.insert;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import org.purl.jh.util.PairC;
//import util.PairC;


/**
 *
 * @author Jirka Hana (jirka ddot hana aat gmail ddot com)
 */
public class InsertHistory  {
    // todo use real clas f-l-t
    public static class ThreeStrings extends PairC<String,PairC<String,String>> {
        ThreeStrings(String aFirst, String aSecond, String aThird) {
            super(aFirst, new PairC<>(aSecond, aThird) );
        }
        
        public String first() {
            return mFirst;
        }

        public String second() {
            return mSecond.mFirst;
        }

        public String third() {
            return mSecond.mSecond;
        }
        
        public PairC<String,String> get23() {
            return mSecond;
        }
        
        public PairC<String,String> get12() {
            return new PairC<>(first(), second());
        }
    }
    
    final Set<ThreeStrings> flts = new HashSet<>();
    
    /** Creates a new instance of InsertHistory */
    public InsertHistory() {
    }
    
    public void add(String aForm, String aLemma, String aTag) {
        flts.add( new ThreeStrings(aForm, aLemma, aTag) );
    }
    
    public Set<PairC<String,String>> getLTs() {
        Set<PairC<String,String>> lts = new HashSet<>();
        for (ThreeStrings flt : flts) {
            lts.add(flt.mSecond);
        }
        
        return lts;
    }

    public List<PairC<String,String>> getLTList() {
        final List<PairC<String,String>> list = new ArrayList<>(getLTs());
        Collections.sort(list);

        return list;
    }
    
}
