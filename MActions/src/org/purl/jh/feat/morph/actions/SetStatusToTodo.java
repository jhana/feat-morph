package org.purl.jh.feat.morph.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.law.nbdata.view.Current;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.actions.SetStatusToTodo")
@ActionRegistration(displayName = "#CTL_SetStatusToTodo", lazy = false)
@ActionReferences({
    @ActionReference(path = "Menu/Annotate", position = 1200),
    @ActionReference(path = "Shortcuts", name= "DS-T"),
})
@NbBundle.Messages("CTL_SetStatusToTodo=Set item status to Todo")
public class SetStatusToTodo extends CurrentAction {
    public SetStatusToTodo() {
        this(null);
    }
 
    public SetStatusToTodo(Lookup context) {
        super(SetStatusToTodo.class, "CTL_SetStatusToTodo", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new SetStatusToTodo(context);
    }

    @Override
    protected void actionPerformed(Current cur) {
        cur.setStatus(Status.TODO);
    }

}