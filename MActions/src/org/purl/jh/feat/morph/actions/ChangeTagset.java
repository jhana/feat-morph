package org.purl.jh.feat.morph.actions;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.util.gui.StdDialog;

@ActionID(
        category = "Annotate",
        id = "org.purl.jh.feat.morph.actions.ChangeTagset"
)
@ActionRegistration(
        displayName = "#CTL_ChangeTagset"
)
@ActionReference(path = "Menu/Annotate", position = 1200)
@Messages("CTL_ChangeTagset=Change Tagset ...")
public final class ChangeTagset implements ActionListener {

    private final MLayer context;

    public ChangeTagset(MLayer context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        ChangeTagsetPanel panel = new ChangeTagsetPanel(context.getTagset());

        StdDialog dlg = new StdDialog((Frame)null, "", true) {
            @Override
            protected JPanel createMainPanel() {
                return panel;
            }
        };

        if (!dlg.accepted()) return;

        context.setTagset(panel.getTagset());
    }
}
