package org.purl.jh.feat.morph.actions;

import java.util.List;
import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.feat.morph.actions.insert.XInsertDlg;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.nbdata.view.Current;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.da.InsertLt")
@ActionRegistration(displayName = "#CTL_InsertLt", lazy = false)
@ActionReference(path = "Menu/Annotate", position = 1000)
@NbBundle.Messages("CTL_InsertLt=Insert new lemma and tag")
public class InsertLt extends CurrentAction {
    public InsertLt() {
        this(null);
    }
 
    public InsertLt(Lookup context) {
        super(InsertLt.class, "CTL_InsertLt", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new InsertLt(context);
    }

    @Override
    protected void actionPerformed(Current cur) {
        
        LTx ltx = cur.one();        // just select some, todo select the currently selected one
        Lt lt = ltx.isEmpty() ? null : ltx.iterator().next();
 
        XInsertDlg dlg = new XInsertDlg(null, ltx, lt);
        if (!dlg.accepted()) return;
        
        cur.getMain().getLayer().setTagset(dlg.getTagset());
        
        final List<Lt> inserted = cur.ltAdd("inserted", dlg.getLemma(), dlg.getTag(), true);
        
        if (!inserted.isEmpty()) {
            cur.ltSelectOnly(inserted.get(0));
        }
    }

}