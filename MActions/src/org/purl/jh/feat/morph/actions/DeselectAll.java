package org.purl.jh.feat.morph.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.law.nbdata.view.Current;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.da.DeselectAllAction")
@ActionRegistration(displayName = "#CTL_DeselectAll", lazy = false)
@ActionReference(path = "Menu/Annotate", position = 10)
@NbBundle.Messages("CTL_DeselectAll=Deselect All Tags")
public class DeselectAll extends CurrentAction {
    public DeselectAll() {
        this(null);
    }
 
    public DeselectAll(Lookup context) {
        super(DeselectAll.class, "CTL_DeselectAll", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new DeselectAll(context);
    }

    @Override
    protected void actionPerformed(Current cur) {
        cur.ltSelectAll(false);
    }
}