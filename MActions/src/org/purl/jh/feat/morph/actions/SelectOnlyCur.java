package org.purl.jh.feat.morph.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.law.nbdata.view.CurrentLt;
import org.purl.jh.law.data.m.Lt;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.da.actions.SelectOnlyCur")
@ActionRegistration(displayName = "#CTL_SelectOnlyCur", lazy = false)
@ActionReference(path = "Menu/Annotate", position = 30)
@NbBundle.Messages("CTL_SelectOnlyCur=Select Only Current Item")
public class SelectOnlyCur extends CurrentLtAction {
    public SelectOnlyCur() {
        this(null);
    }
 
    public SelectOnlyCur(Lookup context) {
        super(SelectOnlyCur.class, "CTL_SelectOnlyCur", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new SelectOnlyCur(context);
    }

    @Override
    protected void actionPerformed(CurrentLt curLt) {
        Lt sampleLt = curLt.getLt();

        curLt.getCurrent().ltSelectOnly(sampleLt);
    }


}