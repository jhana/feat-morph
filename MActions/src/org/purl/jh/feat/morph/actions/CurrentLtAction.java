package org.purl.jh.feat.morph.actions;

import com.google.common.base.Preconditions;
import java.util.Collection;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.purl.jh.law.nbdata.view.CurrentLt;

public abstract class CurrentLtAction extends BaseAction<CurrentLt> {
 
    public CurrentLtAction(Class<? extends CurrentLtAction> actionClass, String bundleKey, Lookup context) {
        super(CurrentLt.class, actionClass, bundleKey, context);
    }
 
    @Override
    public abstract Action createContextAwareInstance(Lookup context);

    @Override
    protected void actionPerformed(Collection<? extends CurrentLt> allInstances) {
        Preconditions.checkState(lkpInfo.allInstances().size() == 1);

        CurrentLt cur = lkpInfo.allInstances().iterator().next();
        actionPerformed(cur);
    }
    
    protected abstract void actionPerformed(CurrentLt cur);

    @Override
    protected boolean isEnabledImpl() {
        return lkpInfo.allInstances().size() == 1;
    }
    
}