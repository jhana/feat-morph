package org.purl.jh.feat.morph.actions;

import java.awt.event.ActionEvent;
import java.util.Collection;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public abstract class BaseAction<T> extends AbstractAction implements LookupListener, ContextAwareAction {
    private final Class<T> clazz;
    private final Lookup context;
    protected Lookup.Result<T> lkpInfo;
 
    protected BaseAction(Class<T> clazz, Class<? extends BaseAction<T>> actionClass, String bundleKey, Lookup context) {
        this.clazz = clazz;
        this.context = context == null ? Utilities.actionsGlobalContext() : context;
        putValue(Action.NAME, NbBundle.getMessage(actionClass, bundleKey)); // FIXME: not sure why it does not use annotations
    }

    @Override
    public abstract Action createContextAwareInstance(Lookup context);
    
    protected void init() {
        assert SwingUtilities.isEventDispatchThread() : "this shall be called just from AWT thread";
 
        if (lkpInfo != null) return;
 
        //The thing we want to listen for the presence or absence of on the global selection
        lkpInfo = context.lookupResult(clazz);
        lkpInfo.addLookupListener(this);
        resultChanged(null);
    }
 
    @Override
    public boolean isEnabled() {
        init();
        return super.isEnabled() && isEnabledImpl();
    }
    
    protected boolean isEnabledImpl() {
        return !lkpInfo.allInstances().isEmpty();
    }
 
    @Override
    public void actionPerformed(ActionEvent e) {
        init();
        actionPerformed(lkpInfo.allInstances());
    }

    protected abstract void actionPerformed(Collection<? extends T> allInstances);
    
    @Override
    public void resultChanged(LookupEvent ev) {
    }
}