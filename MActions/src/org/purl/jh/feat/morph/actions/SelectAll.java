package org.purl.jh.feat.morph.actions;

import com.google.common.base.Preconditions;
import java.util.Collection;
import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.law.nbdata.view.Current;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.da.SelectAll")
@ActionRegistration(displayName = "#CTL_SelectAll", lazy = false)
@ActionReference(path = "Menu/Annotate", position = 20)
@NbBundle.Messages("CTL_SelectAll=Select All Tags")
public class SelectAll extends CurrentAction {
    public SelectAll() {
        this(null);
    }
 
    public SelectAll(Lookup context) {
        super(SelectAll.class, "CTL_SelectAll", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new SelectAll(context);
    }

    @Override
    protected void actionPerformed(Current cur) {
        cur.ltSelectAll(true);
    }
}