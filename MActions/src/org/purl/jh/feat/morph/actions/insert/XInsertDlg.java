package org.purl.jh.feat.morph.actions.insert;

import java.awt.*;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.SimpleTagset;
import org.purl.jh.util.gui.StdDialog;

/**
 * Dialog for inserting lemma/tag into Lts structure
 */
public class XInsertDlg extends StdDialog {
    private final XLtInsertPanel panel;
    
    private final static InsertHistory history = new InsertHistory();
    
    /**
     * 
     * @param aFrame
     * @param aForm CURRENT FORM
     * @param aCurLT lt used to initialize certain values for easy entry; not modified, can be null if there are no items
     */
    public XInsertDlg(Frame aFrame, LTx aLtx, Lt aCurLT) {
        super(aFrame, "Enter Lemma & Tag", true);
        panel = new XLtInsertPanel(aLtx, aCurLT, history);
    }

    public SimpleTagset getTagset() {
        return getPanel().getTagset();
    }
    
    /**
     * @return the string as the user entered it.
     */
    public String getLemma() {
        return getPanel().getLemma();
    }
    
    public String getTag() {
        return getPanel().getTag();
    }

    public String getComment() {
        return getPanel().getComment();
    }
    

// =============================================================================
// Implementation  <editor-fold desc="Implementation">"
// =============================================================================
    private XLtInsertPanel getPanel() {
        return (XLtInsertPanel) panel;
    }

    
    @Override
    protected XLtInsertPanel createMainPanel() {
        return panel;
    }
    
    @Override
    protected void retrieveInput() {
        getPanel().retrieveInput();
    }

    @Override
    protected boolean validateInput() {
        return getPanel().validateInput();
    }

// </editor-fold>
}
