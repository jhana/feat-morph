package org.purl.jh.feat.morph.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.law.nbdata.view.Current;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.actions.SetStatusToDone")
@ActionRegistration(displayName = "#CTL_SetStatusToDone", lazy = false)
@ActionReferences({
    @ActionReference(path = "Menu/Annotate", position = 1250),
    @ActionReference(path = "Shortcuts", name= "DS-D"),
})
@NbBundle.Messages("CTL_SetStatusToDone=Set item status to Review")
public class SetStatusToDone extends CurrentAction {
    public SetStatusToDone() {
        this(null);
    }
 
    public SetStatusToDone(Lookup context) {
        super(SetStatusToDone.class, "CTL_SetStatusToDone", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new SetStatusToDone(context);
    }

    @Override
    protected void actionPerformed(Current cur) {
        cur.setStatus(Status.DONE);
    }

}