package org.purl.jh.feat.morph.actions;

import org.openide.filesystems.FileChooserBuilder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import org.openide.filesystems.FileUtil;
import java.io.IOException;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.purl.jh.nbpml.SaveToCookie;
import org.purl.jh.util.err.XException;

/** MOve somewhere to closer to a module with DataDataObject */
@ActionID(
        category = "File",
        id = "org.purl.jh.feat.morph.actions.SaveToAction"
)
@ActionRegistration(
        displayName = "#CTL_SaveToAction"
)
@ActionReference(path = "Menu/File", position = 1650)
@Messages("CTL_SaveToAction=Save To...")
public final class SaveToAction implements ActionListener {

    private final SaveToCookie context;

    public SaveToAction(SaveToCookie context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        File f = new FileChooserBuilder("save-to")
            .setTitle("Save To")
            .setApproveText("Save")
            .setFileFilter(new FileNameExtensionFilter("Vertical (*.vert)","vert"))
            .setFilesOnly(true)
            .showSaveDialog();
        
        if (f == null) return;

        if (!f.getName().endsWith(".vert")) {
            f = new File(f.getAbsolutePath() + ".vert");
        }

        if (f.exists()) {
            NotifyDescriptor.Confirmation msg = new NotifyDescriptor.Confirmation(
                String.format("The file %s already exists. Overwrite?", f),
                "Confirmation");
            Object notify = DialogDisplayer.getDefault().notify(msg);
            if (notify != NotifyDescriptor.YES_OPTION) return;
        }

        try {
            context.saveTo(FileUtil.toFileObject(f.getParentFile()), f.getName());
        } catch (IOException ex) {
            throw new XException(ex);
        }
    }
}
