package org.purl.jh.feat.morph.actions;

import com.google.common.base.Preconditions;
import java.util.Collection;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.purl.jh.law.nbdata.view.Current;

public abstract class CurrentAction extends BaseAction<Current> {
 
    public CurrentAction(Class<? extends CurrentAction> actionClass, String bundleKey, Lookup context) {
        super(Current.class, actionClass, bundleKey, context);
    }
 
    @Override
    public abstract Action createContextAwareInstance(Lookup context);

    @Override
    protected void actionPerformed(Collection<? extends Current> allInstances) {
        Preconditions.checkState(lkpInfo.allInstances().size() == 1, "%s Lookup: %s - %s", this.getClass(), lkpInfo.allInstances().size(), lkpInfo.allInstances());

        Current cur = lkpInfo.allInstances().iterator().next();
        actionPerformed(cur);
    }
    
    protected abstract void actionPerformed(Current cur);

}