package org.purl.jh.feat.morph.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.law.nbdata.view.Current;

@ActionID(category = "Annotate", id = "org.purl.jh.feat.morph.actions.SetStatusToReview")
@ActionRegistration(displayName = "#CTL_SetStatusToReview", lazy = false)
@ActionReferences({
    @ActionReference(path = "Menu/Annotate", position = 1300),
    @ActionReference(path = "Shortcuts", name= "DS-R"),
})
@NbBundle.Messages("CTL_SetStatusToReview=Set item status to Review")
public class SetStatusToReview extends CurrentAction {
    public SetStatusToReview() {
        this(null);
    }
 
    public SetStatusToReview(Lookup context) {
        super(SetStatusToReview.class, "CTL_SetStatusToReview", context);
    }
 
    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new SetStatusToReview(context);
    }

    @Override
    protected void actionPerformed(Current cur) {
        cur.setStatus(Status.TOREVIEW);
    }

}