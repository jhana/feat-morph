package org.purl.jh.law.nbdata;

import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.MLayer;

@lombok.Data
public class LtsLoc {
    private final MLayer layer;
    private final LTx lts;
}
