package org.purl.jh.law.nbdata.view;

import lombok.Data;
import org.purl.jh.law.data.m.Lt;

@Data
public class CurrentLt {
    private final Current current;
    // sample lt
    private final Lt lt;
}
