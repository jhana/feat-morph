package org.purl.jh.law.nbdata.view;

import com.google.common.base.Preconditions;
import lombok.Getter;
import org.netbeans.spi.navigator.NavigatorLookupHint;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.purl.jh.pml.Layer;
import org.purl.jh.pml.event.DataEvent;
import org.purl.jh.pml.event.DataListener;

/**
 * Create instance, then call init.
 * 
 * @param <L> 
 */
public abstract class ViewConnector<L extends Layer<?>> implements DataListener {
    // current layer and location(s) (both in lookup)
    @Getter protected L curLayer;
    @Getter protected Current curLocs;
    
    /** Contains at least the hint and the current location  */
    @Getter protected final Lookup lookup;
    @Getter protected InstanceContent lookupContent;

    //private final Class<? extends Layer<?>> layerClass;
    protected Lookup.Result<L> layerLookup;
    protected Lookup.Result<Current> locLookup;

    
    public ViewConnector(Class<L> layerClass, final String mime) {
        this(layerClass, mime, null, null);
    }

    
    public ViewConnector(Class<L> layerClass, final String mime, Lookup lookup, InstanceContent lookupContent) {
        Preconditions.checkArgument((lookup == null) == (lookupContent == null), "Either both null, or both injected");

        if (lookupContent == null) {
            this.lookupContent = new InstanceContent();
            this.lookup = new AbstractLookup(this.lookupContent);
        }
        else {
            this.lookupContent = lookupContent;
            this.lookup = lookup;
        }
        
        this.lookupContent.add((NavigatorLookupHint)() -> mime);
        
        this.layerLookup = Utilities.actionsGlobalContext().lookupResult(layerClass);
        this.locLookup   = Utilities.actionsGlobalContext().lookupResult(Current.class);
    }

    public void init() {
        layerLookup.addLookupListener(layerLookupListener);
        layerLookupChanged();
    }
    
    
    protected LookupListener layerLookupListener = (LookupEvent e) -> layerLookupChanged();
    
    protected LookupListener locLookupListener = (LookupEvent e) -> locLookupChanged();
    
    
    
    public abstract void layerChanged();
    public abstract void locChanged();
    
    
    protected void layerLookupChanged() {
        Layer old = curLayer;
        
        if (layerLookup.allInstances().isEmpty()) {
            curLayer = null;

            if (old != curLayer) {
                if (old != null) old.removeChangeListener(this);
        
                replaceInLookup(old,curLayer);

                layerChanged();
                locLookup.removeLookupListener(locLookupListener);
            }
        
        }
        else {
            curLayer = layerLookup.allInstances().iterator().next();

            if (old != curLayer) {
                if (old != null) old.removeChangeListener(this);
        
                replaceInLookup(old,curLayer);
                layerChanged();

                locLookup.addLookupListener(locLookupListener);
                locLookupChanged();
        
                curLayer.addChangeListener(this);
            }
        }
    }    
    
    protected void locLookupChanged() {
        // todo check the layer?
        
        Current old = curLocs;
        if (locLookup.allInstances().isEmpty()) {
            curLocs = Current.EMPTY;
        }
        else {
            curLocs = locLookup.allInstances().iterator().next();  // todo assert one only
        }

        if (old != curLocs) {       // todo equals
            locChanged();
            replaceInLookup(old,curLocs);
        }
    }    
    
    public <T> void replaceInLookup(T oldObj, T newObj) {
        if (oldObj == newObj) return;
        if (oldObj != null) lookupContent.remove(oldObj);
        if (newObj != null) lookupContent.add(newObj);
    }
    

    /**
     * Reaction to model change. The default implementation does nothing. 
     * @param aChangeEvent 
     */
    @Override
    public void handleChange(DataEvent<?> aChangeEvent) {
    }    
    
}
