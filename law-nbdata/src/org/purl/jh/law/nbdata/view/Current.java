package org.purl.jh.law.nbdata.view;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import lombok.Data;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.pml.event.DataEvent;
import org.purl.jh.pml.event.DataListener;

/** 
 * One or more currently selected items.
 * The set of items is immutable, but the items themselves might change (usually
 * by other code)
 */
@Data
public class Current implements DataListener {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(Current.class);
    
    public static final Current EMPTY = new Current(ImmutableList.<LTx>of(), null);
    
    private final ImmutableList<LTx> items;
    private final LTx main;
    
    private Boolean homogenous = null;
    private Boolean homogenous_status = null;
    
    public boolean isEmpty() {
        return items.isEmpty();
    }

    public boolean isSingleton() {
        return items.size() == 1;
    }

    public int size() {
        return items.size();
    }
    
    public LTx single() {
        Preconditions.checkState(isSingleton());
        return one();
    }

    public LTx one() {
        return items.iterator().next();
    }

    // todo multithreading (homogeneous might be reset during calculations)
    public boolean isHomogenous() {
        if (homogenous == null) {
            homogenous = calculateHomogeniity();
        }
        return homogenous;
    }
    
    private boolean calculateHomogeniity() {
        if (size() <= 1) {
            return true;
        }
        else {
            Iterator<? extends LTx> it = items.iterator();
            
            LTx sample = it.next();
            
            for (; it.hasNext();) {
                LTx ltx = it.next();
                if (!compatible(sample, ltx)) return false;
            }
            
            return true;
        }
    }

    public boolean isStatusHomogenous() {
        if (homogenous_status == null) {
            homogenous_status = calculateStatusHomogeniity();
        }
        return homogenous_status;
    }

    /** Check that all items have the same status */
    private boolean calculateStatusHomogeniity() {
        if (size() <= 1) {
            return true;
        }
        else {
            Iterator<? extends LTx> it = items.iterator();
            Status sample = it.next().getStatus();
            for (; it.hasNext();) {
                if (sample != it.next().getStatus()) return false;
            }
            
            return true;
        }
    }
    
    /**
     * 
     * Considers only lemmas and tags. 
     * The items must be equally ordered.
     * Selection is ignored
     * 
     * @return false if the selection is the same
     */
    private static boolean compatible(final LTx one, final LTx two) {
        if (one.size() != two.size()) return false;
         
        for (int i = 0; i < one.size(); i++) {
            Lt a = one.get(i);
            Lt b = two.get(i);
            
            if (! a.equalsOnItem(b)) return false;
        }
        
        return true;
    }

    @Override
    public void handleChange(DataEvent<?> aE) {
        // todo improve, reset only if affects current items
        homogenous = null;
    }

// =============================================================================    
// Modifications
// =============================================================================    
    
    public void setForm(String form) {
        if (items.isEmpty()) return;
        
        LTx one = items.iterator().next();
        
        if (items.size() == 1) {
            one.getLayer().setForm(one, form, null, null);
        }
        else {
            one.getLayer().setForm(items, form, null, null);
        }
    }

    public void setStatus(Status status) {
        if (items.isEmpty()) return;
        
        LTx one = items.iterator().next();
        
        if (items.size() == 1) {
            one.getLayer().setStatus(one, status, null, null);
        }
        else {
            one.getLayer().setStatus(items, status, null, null);
        }
    }

    public void setAutoStatus() {
        if (items.isEmpty()) return;
        
        LTx one = items.iterator().next();
        
        if (items.size() == 1) {
            one.getLayer().setAutoStatus(one, null, null);
        }
        else {
            one.getLayer().setAutoStatus(items, null, null);
        }
    }
    
    public List<Lt> ltAdd(String src, String lemma, String tag, boolean selected) {
        if (items.isEmpty()) return Collections.emptyList();
        
        LTx one = items.iterator().next();
        
        if (items.size() == 1) {
            List<Lt> added = new ArrayList<>();
            added.add( one.getLayer().ltAdd(one, src, lemma, tag, selected, null, null) );
            return added;
        }
        else {
            return one.getLayer().ltAdd(items, src, lemma, tag, selected, null, null);
        }
    }

    public boolean ltSelectAll(boolean select)  {
        if (items.isEmpty()) return false;
        
        LTx one = items.iterator().next();
        one.getLayer().ltSelectAll(items, select, null, null);

        return true;
    }

    public boolean ltSelect(Lt sampleLt, boolean select)  {
        if (items.isEmpty() || !isHomogenous()) return false;

        sampleLt.getLayer().ltSelect(items, sampleLt, select, null, null);

        return true;
    }   
    
    public boolean ltSelectOnly(Lt sampleLt)  {
        if (items.isEmpty() || !isHomogenous()) return false;

        sampleLt.getLayer().ltSelectOnly(items, sampleLt, null, null);
        return true;
    }   
        

    public boolean ltSetLemmaTag(Lt sampleLt, String lemma, String tag)  {
        if (items.isEmpty() || !isHomogenous()) return false;

        sampleLt.getLayer().ltSetLemmaTag(items, sampleLt.getLemma(), sampleLt.getTag(), lemma, tag, null, null);
        return true;
    }

//    public boolean ltSetComment(Lt sampleLt, String comment)  {
//        if (isHomogenous()) {
//            for (LTs ltx : items) {
//                Lt lt = findSimilarLt(ltx, sampleLt);
//                if (lt == null) continue;   // todo warning that something changed concurrently
//                ltx.getLayer().ltSetComment(lt, comment, null, null);
//            }
//            return true;
//        }
//        else {
//            return false;
//        }
//    }

    @Override
    public String toString() {
        if (main == null) return "#" + hashCode() + " main: <null>";

        final Iterable<String> forms = Iterables.transform(items, (LTx f) -> f.getForm());
        return "#" + hashCode() + " main: #" + main.hashCode() + " " + main.getForm() + " [" + Joiner.on(',').join(forms) + "]";
    }


}
