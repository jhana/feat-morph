package org.purl.jh.law.nbdata.view;

import org.openide.util.Lookup;
import org.openide.util.lookup.InstanceContent;
import org.purl.jh.pml.Layer;

/** for this view the layer cannot change */
public abstract class FixedLayerConnector<L extends Layer<?>> extends ViewConnector<L> {

    public FixedLayerConnector(Class<L> layerClass, String mime, Lookup lookup, InstanceContent lookupContent) {
        super(layerClass, mime, lookup, lookupContent);
    }
    
    public FixedLayerConnector(Class<L> layerClass, String mime) {
        super(layerClass, mime);
    }
    
    public FixedLayerConnector setLayer(L layer) {
        this.curLayer = layer;
        lookupContent.add(layer);
        return this;
    }

    
    @Override
    protected void layerLookupChanged() {
        L tmp = layerLookup.allInstances().isEmpty() ?
                null :
                layerLookup.allInstances().iterator().next();

        if (curLayer == tmp) {
            System.out.println("Registering locLookup");
            locLookup.addLookupListener(locLookupListener);
            locLookupChanged();
        }
        else {
            System.out.println("De-registering locLookup");
            locLookup.removeLookupListener(locLookupListener);
        }
    }    

    @Override
    public void layerChanged() {    // never called
    }

    @Override
    public abstract void locChanged();
}
