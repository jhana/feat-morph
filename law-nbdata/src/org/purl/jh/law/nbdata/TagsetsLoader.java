package org.purl.jh.law.nbdata;

import org.purl.jh.law.data.m.SimpleTagset;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.modules.Places;
import org.purl.jh.law.data.m.Tagsets;
import org.purl.jh.util.err.Err;

/**
 * Loads tagsets from a directory. Each tagset must be in a separate file.
 */
class TagsetsLoader {
    public void loadTagsets() {
        System.err.println("Loading tagsets");
        Path tsDir = Places.getUserDirectory().toPath().resolve("tagsets");
        if (Files.exists(tsDir)) {
            
            try {
                for (Path tsFile : Files.newDirectoryStream(tsDir)) {
                    try {
                        SimpleTagset ts = loadTagset(tsFile);
                        if (tsFile.toString().endsWith(".def")) {
                            Tagsets.INSTANCE.setDefTagset(ts);
                        }
                        
                        Err.fAssert(Tagsets.INSTANCE.getTagsets().containsKey(ts.getId()), "Duplicate tagset id %s (file: %s)", ts.getId(), tsFile);
                        Tagsets.INSTANCE.getTagsets().put(ts.getId(), ts);
                    } catch (Throwable ex) {
                        System.err.printf("Error loading tagset %s - %s%n", tsFile, ex.getMessage());
                    }
                }
            } catch (Throwable ex) {
                System.err.printf("Cannot access tagset directory (%s): %s%n", tsDir, ex.getMessage());;
            }
        }
        else {
            // todo log
            System.err.println("No tagset directory");
        }
    }

    private SimpleTagset loadTagset(Path tsFile) throws IOException {
        List<String> lines = Files.lines(tsFile).collect(Collectors.toList());
        Err.fAssert(lines.size() > 2, "Tagset specification must contain id and description lines before tags");
        
        String id = lines.get(0);
        String desc = lines.get(1);
        Set<String> tags = new HashSet<>( lines.subList(2, lines.size()));
        
        return new SimpleTagset(id, desc, tags);
    }
    
}
