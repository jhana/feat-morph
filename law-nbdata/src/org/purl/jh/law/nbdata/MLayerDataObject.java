package org.purl.jh.law.nbdata;

import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.purl.jh.law.data.io.VertReader;
import org.purl.jh.law.data.io.VertWriter;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.nbdata.view.MLayerView;
import org.purl.jh.nbpml.DataDataObject;
import org.purl.jh.nbpml.LayerProvider;
import org.purl.jh.nbpml.OpenAtSupport;
import org.purl.jh.util.err.ErrorHandler;

public class MLayerDataObject extends DataDataObject<MLayer> {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(MLayerDataObject.class);

    public MLayerDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        CookieSet cookies = getCookieSet();
        cookies.assign(LayerProvider.class, new LayerProvider() {
            @Override public MLayer getLayer(ErrorHandler aErr) {
                return MLayerDataObject.this.getData(aErr);
            }
        });

        log.fine("Creating dobj: %s", this.getClass());
        addToLookup(this);      // todo move to super class
        addToLookup(new OpenAtSupport(this, MLayerView.class));
    }
    
    @Override
    protected VertReader getReader() {
        return new VertReader();
    }

    @Override
    protected VertWriter getWriter() {
        return new VertWriter();
   }

    @Override
    public String toString() {
        return "LawLayerDataObject{" + super.toString() +
                ", modified: %s" + isModified() + 
                ", layer modified: %s" + getData().isModified() + '}';
    }
}
