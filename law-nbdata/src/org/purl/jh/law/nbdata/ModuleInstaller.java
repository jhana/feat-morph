package org.purl.jh.law.nbdata;

import org.openide.modules.ModuleInstall;

/**
 * Reads tagsets from user's directory when the module is loaded (i.e. restored)
 */
public class ModuleInstaller extends ModuleInstall {
    @Override
    public void restored() {
        new TagsetsLoader().loadTagsets();
    }
} 
