package org.purl.jh.feat.morph.navigator.quickfilter;

import ca.odell.glazedlists.matchers.TextMatcherEditor;
import com.google.common.base.Objects;
import java.awt.Color;
import java.util.regex.Pattern;
import javax.swing.JToggleButton;
import lombok.Getter;
import org.purl.jh.feat.morph.navigator.Util;
import org.purl.jh.util.gui.SimpleDocListener;

/**
 * This composite zoom filter box for a quick and easy filtering list of items:
 * todo separate into view and model
 */
public class QuickFilterBox extends javax.swing.JToolBar  {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(QuickFilterBox.class);
    private final static java.util.ResourceBundle bundle = org.openide.util.NbBundle.getBundle(QuickFilterBox.class);

    
    private javax.swing.JTextField filterTextBox;
    private final Color normalFilterBoxColor;
        
    private javax.swing.ButtonGroup modeGroup;
    private javax.swing.JToggleButton containsModeButton;
    private javax.swing.JToggleButton exactModeButton;
    private javax.swing.JToggleButton prefixModeButton;
    
    private javax.swing.JToggleButton regexModeButton;
    private javax.swing.JToggleButton ignoreDiaButton;
    
    @Getter private QuickFilterModel model;
    
    public QuickFilterBox() {
        model = new QuickFilterModel();

        initComponents();
        normalFilterBoxColor = filterTextBox.getForeground();
        setRollover(true);
        setBorderPainted(false);
        
        ModelListener modelListener = new ModelListener();
        model.addPropertyChangeListener(modelListener);
        
        loadAll();
    }

    private class ModelListener extends PropertyChangeListenerAdapter {
        @Override
        protected void textChanged(String newValue, String oldValue) {
            loadText();
        }

        @Override
        protected void modeChanged(int newValue, int oldValue) {
            loadMode();
        }

        @Override
        protected void regexChanged(boolean newValue, boolean oldValue) {
            loadRegex();
        }

        @Override
        protected void ignoreDiaChanged(boolean newValue, boolean oldValue) {
            loadIgnoreDia();
        }
    }

    private void loadAll() {
        loadText();
        loadMode();
        loadRegex();
        loadIgnoreDia();
    }

    protected void loadText() {
        if (!model.isRegex() || model.getText() == null || Util.checkRegex(model.getText())) {
            filterTextBox.setForeground(normalFilterBoxColor);  
        }
        else {
            filterTextBox.setForeground(Color.RED);
        }                    
                
        if ( Objects.equal(model.getText(), filterTextBox.getText())) return;

        filterTextBox.setText(model.getText());
    }

    protected void loadMode() {
        switch (model.getMode()) {
            case TextMatcherEditor.EXACT:       setSelected(exactModeButton, true); break;
            case TextMatcherEditor.CONTAINS:    setSelected(containsModeButton, true); break;
            case TextMatcherEditor.STARTS_WITH: setSelected(prefixModeButton, true); break;
            default: 
        }
    }

    protected void loadRegex() {
        setSelected(regexModeButton, model.isRegex());
    }

    protected void loadIgnoreDia() {
        setSelected(ignoreDiaButton, model.isIgnoreDia());
    }

    private static void setSelected(JToggleButton button, boolean value) {
        if (button.isSelected() == value) return;
        button.setSelected(value);
    }
    
    
    
    private void initComponents() {
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        //setLayout(new FlowLayout(FlowLayout.LEADING));
        //setBorder(null);

        filterTextBox = new javax.swing.JTextField();

        modeGroup = new javax.swing.ButtonGroup();
        containsModeButton = new javax.swing.JToggleButton();
        exactModeButton = new javax.swing.JToggleButton();
        prefixModeButton = new javax.swing.JToggleButton();

        regexModeButton = new javax.swing.JToggleButton();
        ignoreDiaButton = new javax.swing.JToggleButton();

        this.add(filterTextBox);
        filterTextBox.getDocument().addDocumentListener( new SimpleDocListener() {
            @Override
            public void textUpdated() {
                model.setText(filterTextBox.getText());
            }
        });                
        
        modeGroup.add(exactModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(exactModeButton, bundle.getString("QuickFilterBox.exactModeButton.text")); // NOI18N
        exactModeButton.setToolTipText(bundle.getString("QuickFilterBox.exactModeButton.toolTipText")); // NOI18N
        exactModeButton.setFocusable(false);
        exactModeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        exactModeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        exactModeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                model.setMode(TextMatcherEditor.EXACT);
            }
        });
        this.add(exactModeButton);

        modeGroup.add(prefixModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(prefixModeButton, bundle.getString("QuickFilterBox.prefixModeButton.text")); // NOI18N
        prefixModeButton.setToolTipText(bundle.getString("QuickFilterBox.prefixModeButton.toolTipText")); // NOI18N
        prefixModeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                model.setMode(TextMatcherEditor.STARTS_WITH);
            }
        });
        this.add(prefixModeButton);

        modeGroup.add(containsModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(containsModeButton, bundle.getString("QuickFilterBox.containsModeButton.text")); // NOI18N
        containsModeButton.setToolTipText(bundle.getString("QuickFilterBox.containsModeButton.toolTipText")); // NOI18N
        containsModeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                model.setMode(TextMatcherEditor.CONTAINS);
            }
        });
        this.add(containsModeButton);

        org.openide.awt.Mnemonics.setLocalizedText(regexModeButton, bundle.getString("QuickFilterBox.regexModeButton.text")); // NOI18N
        regexModeButton.setToolTipText(bundle.getString("QuickFilterBox.regexModeButton.toolTipText")); // NOI18N
        regexModeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                model.setRegex(regexModeButton.isSelected());
            }
        });
        this.add(regexModeButton);

        org.openide.awt.Mnemonics.setLocalizedText(ignoreDiaButton, bundle.getString("QuickFilterBox.ignoreDiaButton.text")); // NOI18N
        ignoreDiaButton.setToolTipText(bundle.getString("QuickFilterBox.ignoreDiaButton.toolTipText")); // NOI18N
        ignoreDiaButton.setFocusable(false);
        ignoreDiaButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ignoreDiaButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ignoreDiaButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                model.setIgnoreDia(ignoreDiaButton.isSelected());
            }
        });
        this.add(ignoreDiaButton);
    }
}
