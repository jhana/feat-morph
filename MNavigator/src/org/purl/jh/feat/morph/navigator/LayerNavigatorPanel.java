package org.purl.jh.feat.morph.navigator;

import com.google.common.collect.ImmutableList;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.spi.navigator.NavigatorLookupHint;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.purl.jh.law.data.m.LTs;
import org.purl.jh.law.nbdata.view.Current;
import org.purl.jh.pml.IdedElement;
import org.purl.jh.pml.Layer;

public abstract class LayerNavigatorPanel<T extends IdedElement> extends javax.swing.JPanel implements org.netbeans.spi.navigator.NavigatorPanel {
    private final NavigatorLookupHint hint;
     
    /** Contains at least the hint and the current location  */
    protected final Lookup lookup;
    protected InstanceContent lookupContent = new InstanceContent();

    private final Class<? extends Layer<?>> layerClass;
    private Lookup.Result<? extends Layer<?>> layerLookup;
    private LookupListener layerLookupListener = new LookupListener() {
        @Override
        public void resultChanged(LookupEvent e) {
            layerLookupChanged();
        }
    };
    protected Layer<?> layer;
    
    protected Current curLocs = Current.EMPTY;

    
    protected LayerNavigatorPanel(Class<? extends Layer<?>> layerClass, final String mime) {
        this.layerClass = layerClass;
        
        this.hint = new NavigatorLookupHint() {
            @Override
            public String getContentType() {
                return mime;
            }
        };
        
        this.lookup = new AbstractLookup(lookupContent);
    }

    @Override
    public void panelActivated(Lookup lkp) {
        lookupContent.add(hint);    // work around: see http://markmail.org/thread/mo7xb34hoztqbmdj

        layerLookup = Utilities.actionsGlobalContext().lookupResult(layerClass);
        layerLookup.addLookupListener(layerLookupListener);
        layerLookupChanged();
    }

    @Override
    public void panelDeactivated() {
        lookupContent.remove(hint);
        layerLookup.removeLookupListener(layerLookupListener);
    }
    
    
    protected void layerLookupChanged() {
        Layer<?> oldLayer = layer;
        if (layerLookup.allInstances().isEmpty()) {
            layer = null;
        }
        else {
            layer = layerLookup.allInstances().iterator().next();
        }
        
        if (layer == oldLayer) return;

        replaceInLookup(oldLayer, layer);

        SwingUtilities.invokeLater(new Runnable() { // todo cancel if resultChanged again (ineffective + racing condition)
            public void run() {
                updateNavigatorModel();
            }
        });
    }  
    
    private <T> void replaceInLookup(T oldObj, T newObj) {
        if (oldObj == newObj) return;
        if (oldObj != null) lookupContent.remove(oldObj);
        if (newObj != null) lookupContent.add(newObj);
    }

    protected abstract void updateNavigatorModel();
  
    @Override
    public Lookup getLookup() {
        return lookup;
    }
 
    @Override
    public abstract String getDisplayName();

    @Override
    public String getDisplayHint() {
        return getDisplayName();
    }

    @Override
    public JComponent getComponent() {
        return this;
    }
    
    /**
     * Items in the navigator were selected, show it in a TC.
     * 
     * @param elements elements that were selected
     * @param focus should the first item in TC be focused or just selected
     */
    protected void selected(final ImmutableList<T> elements, final boolean focus) {
        // todo compare curent and new
        
        final Current oldLocs = curLocs;
        layer.removeChangeListener(oldLocs);

        curLocs = new Current((ImmutableList<LTs>)elements); // todo
        layer.addChangeListener(curLocs);

        replaceInLookup(oldLocs, curLocs);
    }
    
}
