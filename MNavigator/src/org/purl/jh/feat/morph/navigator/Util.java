package org.purl.jh.feat.morph.navigator;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Util {
    public static boolean checkRegex(String regexStr) {
        try {
            Pattern.compile(regexStr);
            return true;
        }
        catch (PatternSyntaxException e) {
            return false;
        }
    }
}
