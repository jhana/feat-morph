package org.purl.jh.feat.morph.navigator;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.TextFilterator;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.matchers.TextMatcherEditor;
import ca.odell.glazedlists.swing.DefaultEventSelectionModel;
import ca.odell.glazedlists.swing.EventTableModel;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.purl.jh.feat.morph.navigator.quickfilter.QuickFilterModel;
import org.purl.jh.law.data.m.LTs;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.util.err.Err;

/**
 * todo remove from selection items that are filtered out
 * todo keep displaying it when clicked
 * @author j
 */
@NavigatorPanel.Registration(mimeType = "text/pdt-m+xml", displayName = "PDT M-Layer")
public class NavigatorPanel extends LayerNavigatorPanel<LTs> {
    
    private List<LTs> model;
    private EventList<LTs> filteredItems;
    
    private TextMatcherEditor<LTs> matcherEditor;
    
    public NavigatorPanel() {
        super(MLayer.class, "text/pdt-m+xml");
        initComponents();
        
        itemList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // todo go to the newlly selected item if  doubleclicked
            }
            
        });
        
    }

    @Override
    public String getDisplayName() {
        return "M-Layer forms";
    }
    
    @Override
    protected void updateNavigatorModel() {
        System.out.println("XXA: updateNavigatorModel");
        Err.fAssert(layer instanceof MLayer, "Wrong mime"); 
        MLayer mlayer = (MLayer) layer;
       
        
        model = mlayer == null ? Collections.<LTs>emptyList() : mlayer.getFlatLts();
        
        System.out.println("XXA: " + model.size());
        
        final EventList<LTs> items = new BasicEventList<>(model);
        
        //curFilter.getFilter().filter(model, items);

        final TextFilterator<LTs> filterator = new TextFilterator<LTs>() {
            @Override
            public void getFilterStrings(final List<String> baseList, final LTs element) {
                baseList.add(element.getForm().getFormStr());
            }
        };

        // override TextComponentMatcherEditor to handle errors in regex
        matcherEditor = new TextMatcherEditor<>(filterator);
        
        quickFilterBox1.getModel().addPropertyChangeListener(new java.beans.PropertyChangeListener() { 
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateMatcher();
            }
        });
        
        
        filteredItems = new FilterList<>(items, matcherEditor); 
        
        if (azSortButton.isSelected()) {
            System.out.println("Sorting navigator");
            filteredItems = new SortedList<>(filteredItems, new Comparator<LTs>() {
                @Override
                public int compare(LTs o1, LTs o2) {
                    return o1.getForm().getFormStr().compareTo(o2.getForm().getFormStr());
                }

            });
            ((SortedList)filteredItems).setMode(SortedList.AVOID_MOVING_ELEMENTS);
        }
        
        DefaultEventSelectionModel selectionModel = new DefaultEventSelectionModel(filteredItems);
        EventTableModel<LTs> tblModel = new EventTableModel<>(filteredItems, tblFormat);
        
        itemList.setModel(tblModel);
        itemList.setSelectionModel(selectionModel);

        filteredItems.addListEventListener( new ListEventListener<LTs>() {
            @Override
            public void listChanged(ListEvent<LTs> listChanges) {
                updateStatistics();
            }
        });

        selectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (itemList.getSelectedRow() == -1 || (itemList.getRowCount() <= itemList.getSelectedRow()) ) return;
                
                if (itemList.getSelectedRowCount() == 1) {
                    final LTs element = filteredItems.get(itemList.getSelectedRow()); 

                    selected(ImmutableList.of(element), false);
                }
                else {
                    // todo maybe make lightweight
                    ImmutableList.Builder<LTs> builder = ImmutableList.builder();
                    for (int i : itemList.getSelectedRows()) {
                        builder.add( filteredItems.get(i) );
                    }
                    selected(builder.build(), false);
                }
    
                updateStatistics();
            }
        });

        updateStatistics();
        
    }
    
    
    
    private TableFormat<? super LTs> tblFormat = new TableFormat<LTs>() {
        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int column) {
            return column == 0 ? "Form" : "LTs";
        }

        @Override
        public Object getColumnValue(LTs obj, int column) {
            return column == 0 ? obj.getForm().getFormStr() : Joiner.on(", ").join(obj.col());
        }
    };
    
    
    private void updateMatcher() {
        final QuickFilterModel m = quickFilterBox1.getModel();
        
        if (!m.isIgnoreDia()) matcherEditor.setStrategy(TextMatcherEditor.IDENTICAL_STRATEGY);
        
        if (m.isRegex()) {
            matcherEditor.setMode(TextMatcherEditor.REGULAR_EXPRESSION);
        }
        else {
            matcherEditor.setMode(m.getMode());
        }

        if (m.isIgnoreDia()) matcherEditor.setStrategy(TextMatcherEditor.NORMALIZED_STRATEGY);
        
        if (!m.isRegex() || Util.checkRegex(m.getText())) {
            matcherEditor.setFilterText(new String[] {m.getText()});
        }
    }
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        itemList = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        quickFilterBox1 = new org.purl.jh.feat.morph.navigator.quickfilter.QuickFilterBox();
        azSortButton = new javax.swing.JToggleButton();
        statisticsText = new javax.swing.JTextField();

        itemList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(itemList);

        jToolBar1.setRollover(true);
        jToolBar1.add(quickFilterBox1);

        org.openide.awt.Mnemonics.setLocalizedText(azSortButton, org.openide.util.NbBundle.getMessage(NavigatorPanel.class, "NavigatorPanel.azSortButton.text")); // NOI18N
        azSortButton.setFocusable(false);
        azSortButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        azSortButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        azSortButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                azSortButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(azSortButton);

        statisticsText.setEditable(false);
        statisticsText.setText(org.openide.util.NbBundle.getMessage(NavigatorPanel.class, "NavigatorPanel.statisticsText.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(statisticsText)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statisticsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void azSortButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_azSortButtonActionPerformed
        System.out.println("SORT. azSortButtonActionPerformed");
        updateNavigatorModel(); 
    }//GEN-LAST:event_azSortButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton azSortButton;
    private javax.swing.JTable itemList;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private org.purl.jh.feat.morph.navigator.quickfilter.QuickFilterBox quickFilterBox1;
    private javax.swing.JTextField statisticsText;
    // End of variables declaration//GEN-END:variables

    private void updateStatistics() {
        statisticsText.setText(String.format("All: %d Filtered: %d Selected: %d", model.size(), filteredItems.size(), curLocs.size() ) );
    }


}
