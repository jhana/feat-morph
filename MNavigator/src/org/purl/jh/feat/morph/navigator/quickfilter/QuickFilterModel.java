package org.purl.jh.feat.morph.navigator.quickfilter;

import ca.odell.glazedlists.matchers.TextMatcherEditor;
import com.google.common.base.Objects;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * 
 * todo consider attaching preferences via 
 */
@lombok.Data
public class QuickFilterModel {
            
    
    
    public static final String PROP_TEXT = "text";
    public static final String PROP_MODE = "mode";
    public static final String PROP_IGNOREDIA = "ignoreDia";
    public static final String PROP_REGEX = "regex";
    
    private String text = "";
    private int mode;
    private boolean ignoreDia;
    private boolean regex;

    
    public void setText(String value) {
        if ( Objects.equal(value, this.text)) return;
        String oldValue = this.text;
        this.text = value;
        propertySupport.firePropertyChange(PROP_TEXT, oldValue, value);
    }

    public void setMode(int value) {
        if (value == this.mode) return;
        
        if (value != TextMatcherEditor.EXACT) setRegex(false);
        
        int oldValue = this.mode;
        this.mode = value;
        propertySupport.firePropertyChange(PROP_MODE, oldValue, value);
    }

    public void setRegex(boolean value) {
        if (value == this.regex) return;
        
        if (value) {
            setIgnoreDia(false);
            setMode(TextMatcherEditor.EXACT);
        }
        
        boolean oldValue = this.regex;
        this.regex = value;
        propertySupport.firePropertyChange(PROP_REGEX, oldValue, value);
    }

    public void setIgnoreDia(boolean value) {
        if (value == this.ignoreDia) return;
        
        if (value && isRegex()) setRegex(false);
        
        boolean oldValue = this.ignoreDia;
        this.ignoreDia = value;
        propertySupport.firePropertyChange(PROP_IGNOREDIA, oldValue, value);
    }

    
    private PropertyChangeSupport propertySupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }

//    todo reuse (prefixed) bean property ids
//    private static final String cMatcher_mode  = "matcher_mode"; 
//    private static final String cMatcher_regex = "matcher_regex";
//    private static final String cMatcher_ignoreDia = "matcher_ignoreDia";
//
//        final int mode = getPrefs().getInt(cMatcher_mode, 0);
//        return getPrefs().getBoolean(cMatcher_regex, false);    
//        return getPrefs().getBoolean(cMatcher_ignoreDia, false);
//        getPrefs().putInt(cMatcher_mode, aMode);
//    
//    private Preferences getPrefs() {
//        return NbPreferences.forModule(getClass());
//    }
    
}
