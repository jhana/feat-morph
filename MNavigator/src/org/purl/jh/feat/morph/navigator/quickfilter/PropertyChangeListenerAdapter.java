package org.purl.jh.feat.morph.navigator.quickfilter;

import java.beans.PropertyChangeEvent;

public class PropertyChangeListenerAdapter implements java.beans.PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case QuickFilterModel.PROP_TEXT:
                textChanged((String) evt.getNewValue(), (String) evt.getOldValue());
                break;
            case QuickFilterModel.PROP_MODE:
                modeChanged((int) evt.getNewValue(), (int) evt.getOldValue());
                break;
            case QuickFilterModel.PROP_REGEX:
                regexChanged((boolean) evt.getNewValue(), (boolean) evt.getOldValue());
                break;
            case QuickFilterModel.PROP_IGNOREDIA:
                ignoreDiaChanged((boolean) evt.getNewValue(), (boolean) evt.getOldValue());
                break;
        }
    }

    protected void textChanged(String newValue, String oldValue) {
    }

    protected void modeChanged(int newValue, int oldValue) {
    }

    protected void regexChanged(boolean newValue, boolean oldValue) {
    }

    protected void ignoreDiaChanged(boolean newValue, boolean oldValue) {
    }
    
}
