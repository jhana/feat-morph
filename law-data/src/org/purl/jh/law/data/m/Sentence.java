package org.purl.jh.law.data.m;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.purl.jh.law.data.io.SgmlTag;
import org.purl.jh.pml.AbstractListElement;
import org.purl.jh.pml.Element;


/**
 * M-level element
 * Note: Sentence can contain only LTs (@todo ? allow any Ss?)
 *
 * @author Jirka
 */
@Getter
public class Sentence extends AbstractListElement<Element>  {
    private final SgmlTag tag;
    private final List<LTx> ltxs = new ArrayList<>();
    
    public Sentence(SgmlTag tag) {
        this.tag = tag;
    }

    @Override
    public MLayer getLayer() {
        return (MLayer) super.getLayer(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addExtra(Element element) {
        if (element instanceof LTx) ltxs.add((LTx)element);
    }
}
