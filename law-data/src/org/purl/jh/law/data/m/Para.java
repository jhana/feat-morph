package org.purl.jh.law.data.m;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.purl.jh.law.data.io.SgmlTag;
import org.purl.jh.pml.AbstractListElement;
import org.purl.jh.pml.Element;


/**
 * M-level element.
 * Has Sent as its subelement.
 *
 * @author Jirka
 */
@Getter
public class Para extends AbstractListElement<Element> {
    private final SgmlTag tag;
    private final List<Sentence> sentences = new ArrayList<>();

    /** Creates a new instance of Para */
    public Para(SgmlTag tag) {
        this.tag = tag;
    }

    @Override
    public void addExtra(Element element) {
        if (element instanceof Sentence) sentences.add((Sentence)element);
    }
}
