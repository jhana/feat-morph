package org.purl.jh.law.data.m;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.purl.jh.law.data.io.SgmlTag;
import org.purl.jh.pml.Element;
import org.purl.jh.pml.IdedListElement;

/**
 * M-layer element
 * @author Jirka
 */
@Getter @Setter @Accessors(chain=true)
public class Doc extends IdedListElement<Element> {
    private final SgmlTag tag;
    private final List<Para> paras = new ArrayList<>();
    
    /** 
     * Creates a new instance of Document. 
     */
    public Doc(/*@NonNull*/ MLayer aLayer, String aId, SgmlTag tag) {
        super(aLayer, aId);
        this.tag = tag;
    }

    @Override
    public void addExtra(Element element) {
        if (element instanceof Para) paras.add((Para)element);
    }
}
