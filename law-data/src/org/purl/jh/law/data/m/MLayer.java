package org.purl.jh.law.data.m;

import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.openide.filesystems.FileObject;
import org.purl.jh.law.data.event.MLayerEvent;
import org.purl.jh.pml.Commented;
import org.purl.jh.pml.Element;
import org.purl.jh.pml.IdedElement;
import org.purl.jh.pml.Layer;
import org.purl.jh.pml.event.DataListener;
import org.purl.jh.pml.location.Location;

/**
 * All elements have this 
 * (Doc -??) Para - Sent - LT
 * 
 * Change from PML - the only layer
 * Assumption: There is only one referenced layer - the w-layer
 * Assumption: If there are alternatives in LTs all use the same form(s)
 * Assumption: Only single layer is referenced (w-layer as wdata), @todo check
 *
 * @author Jirka
 */
@Getter @Setter @Accessors(chain = true)
public final class MLayer extends Layer<Element>  {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(MLayer.class);

    private final List<Doc> docs = new ArrayList<>();
    
    /** LTs structures, computed; the real children are sentences, etc */
    private final List<LTx> flatLts = new ArrayList<>();
    
    /** Morphological tagset used in this layer; might be null */
    private SimpleTagset tagset;
    
    /** Opening elements added during reading to make the document well-formed */
    private final Set<Element> autoAddedOpen = new HashSet<>();
    /** Closing elements added during reading to make the document well-formed */
    private final Set<Element> autoAddedClose = new HashSet<>();
    
// -----------------------------------------------------------------------------
// 
// -----------------------------------------------------------------------------
    
    /**
     * Creates a new Layer.
     *
     * @param aFile file this layer is saved in
     *    null for a layer that is not yet assigned a file name
     * @param aId a unique identifier of the layer
     * @todo unique among layers or generaly 
     */
    public MLayer(FileObject aFile, String aId) {
        super(aFile, aId);
    }

    @Override
    public void addExtra(Element aEl) {
        //if (aEl instanceof Para) paras.add((Para)aEl);
        if (aEl instanceof Doc) docs.add((Doc)aEl);
    }
    
    
// =============================================================================
// <editor-fold desc="Sss">"
// =============================================================================
// -----------------------------------------------------------------------------
// <editor-fold desc="Basic Sss">
// -----------------------------------------------------------------------------
    
    @Override
    public void addIdedElement(IdedElement element) {
        super.addIdedElement(element);
        if (element instanceof LTx) flatLts.add((LTx)element);
    }
    
    /**
     * Calculates the number of ambiguous entries in this layer.
     * Note: The value is calculated each time.
     */
    public int getAmbiguousNr() {
        int ambiguous = 0;
        for (LTx ss : flatLts) {
            if ( ss.isAmbivalent() ) ambiguous++;
        }
        return ambiguous;
    }
    
    public Iterable<LTx> getAmbiLts() {
        return LTx.cAmbiFilter.col(flatLts);
    }
    

    protected Lt findSimilarLt(LTx ltx, Lt sampleLt) {
        for (Lt lt : ltx) {
            Preconditions.checkNotNull(sampleLt, "ltx:\n %s", ltx);
            if (lt.equalsOnItem(sampleLt)) return lt;
        }

        return null;
    }
    
// </editor-fold>    
    public void setForm(LTx ltx, String newForm, final DataListener srcView, final Object srcInfo) {
        ltx.setForm(newForm);
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cSetForm, srcView, srcInfo);
        event.lts = ltx;
        fireEvents(event);
    }

    public void setForm(Iterable<LTx> ltxs, String newForm, final DataListener srcView, final Object srcInfo) {
        for (LTx ltx : ltxs) {
            ltx.setForm(newForm);
        }
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiSetForm, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }

    public void setStatus(LTx ltx, Status status, final DataListener srcView, final Object srcInfo) {
        ltx.setStatus(status);
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cSetStatus, srcView, srcInfo);
        event.lts = ltx;
        fireEvents(event);
    }

    public void setStatus(Iterable<LTx> ltxs, Status status, final DataListener srcView, final Object srcInfo) {
        for (LTx ltx : ltxs) {
            ltx.setStatus(status);
        }
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiSetStatus, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }

    /** 
     * Automatically set status based on ambiguity:
     * <ul>
     * <li>TODO to DONE if non-ambi, 
     * <li>DONE to TODO if ambi
     * <li>keep REVIEW untouched
     * </ul>
     */
    public void setAutoStatus(LTx ltx, final DataListener srcView, final Object srcInfo) {
        if (ltx.getStatus() != Status.TOREVIEW) {
            ltx.setStatus(ltx.isAmbivalent() ? Status.TODO : Status.DONE);
        }
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cSetStatus, srcView, srcInfo);
        event.lts = ltx;
        fireEvents(event);
    }

    public void setAutoStatus(Iterable<LTx> ltxs, final DataListener srcView, final Object srcInfo) {
        for (LTx ltx : ltxs) {
            if (ltx.getStatus() != Status.TOREVIEW) {
                ltx.setStatus(ltx.isAmbivalent() ? Status.TODO : Status.DONE);
            }
        }
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiSetStatus, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }
    
// -----------------------------------------------------------------------------
// <editor-fold desc="add/remove lt">
// -----------------------------------------------------------------------------

    /**
     * Adds multiple lts, firing a single event
     * @param ltxs
     * @param src
     * @param lemma
     * @param tag
     * @param selected
     * @param srcView
     * @param srcInfo
     * @return 
     */
    public List<Lt> ltAdd(Iterable<LTx> ltxs, String src, String lemma, String tag, boolean selected, final DataListener srcView, final Object srcInfo) {
        final List<Lt> added = new ArrayList<>();

        for (LTx ltx : ltxs) {
            final Lt newLT = new Lt()
                    .setSrc(src)
                    .setLemma(lemma)
                    .setTag(tag)
                    .setSelected(selected);

            ltx.add(newLT);
            added.add(newLT);
        }
        
        final MLayerEvent e1 = new MLayerEvent(this, MLayerEvent.cMultiLtxAdd, srcView, srcInfo);
        fireEvents(e1);
    
        return added;
    }
    
    /**
     */
    public Lt ltAdd(LTx lts, String src, String lemma, String tag, boolean selected, final DataListener srcView, final Object srcInfo) {
        final Lt newLT = new Lt()
                .setSrc(src)
                .setLemma(lemma)
                .setTag(tag)
                .setSelected(selected);
        
        final boolean ambiBefore = lts.isAmbivalent();

        lts.add(newLT);

        final MLayerEvent e1 = new MLayerEvent(this, MLayerEvent.cLtxAdd, srcView, srcInfo);
        e1.lts = lts;
        e1.lt = newLT;
        //e1.location = Location.of(newLT);
        e1.ambiChanged = (ambiBefore != lts.isAmbivalent());
        fireEvents(e1);

        return newLT;
    }
    
    /**
     */
    public void ltDel(Lt lt, final DataListener srcView, final Object srcInfo) {
        final LTx lts = lt.getParent();
        
        final boolean ambiBefore = lts.isAmbivalent();

        lts.col().remove(lt);

        final MLayerEvent e1 = new MLayerEvent(this, MLayerEvent.cLtxDel, srcView, srcInfo);
        e1.lts = lts;
        //e1.location = Location.of(lt);
        e1.ambiChanged = (ambiBefore != lts.isAmbivalent());
        fireEvents(e1);
    }
    
// </editor-fold>    
    
// -----------------------------------------------------------------------------
// <editor-fold desc="change tag/lemma/comment/selection">
// -----------------------------------------------------------------------------

    public void ltSetLemmaTag(Iterable<LTx> ltxs, String oldLemma, String oldTag, String newLemma, String newTag, final DataListener srcView, final Object srcInfo)  {
        for (LTx ltx : ltxs) {
            for (Lt lt : ltx.col()) {
                if (lt.getLemma().equals(oldLemma) && lt.getTag().equals(oldTag)) {
                    lt.setLemma(newLemma);
                    lt.setTag(newTag);
                }
            }
        }
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiLtLemmaTag, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }
    
    public void ltSetLemma(Lt lt, String lemma, final DataListener srcView, final Object srcInfo)  {
        lt.setLemma(lemma);
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cLtLemma, srcView, srcInfo);
        event.lt = lt;
        //event.location = Location.of(lt, "lemma");  // lemma
        fireEvents(event);
    }

    public void ltSetTag(Lt lt, String tag, final DataListener srcView, final Object srcInfo)  {
        lt.setTag(tag);
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cLtTag, srcView, srcInfo);
        event.lt = lt;
        //event.location = Location.of(lt, "tag");
        fireEvents(event);
    }
            
    
    public <T extends Commented & IdedElement> void ltSetComment(T e, String comment, final DataListener srcView, final Object srcInfo)  {
        
        e.setComment(comment);
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cComment, srcView, srcInfo);
        event.element = e;
        event.location = Location.of(e, "comment");
        fireEvents(event);
    }

    public void ltSelect(Iterable<LTx> ltxs, Lt sample, boolean selection, final DataListener srcView, final Object srcInfo)  {

        // --- (de)select all items for all ltxs ---
        for (LTx ltx : ltxs) {
            Lt lt = findSimilarLt(ltx, sample);
            if (lt != null) lt.setSelected(selection); 
        }
        
        // --- the whole ss changed ---
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiLtxSelect, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }
    
    /**
     * Changes selection of an element.  
     * 
     * E.g. In LTDisambPanel, a tag was selected. 
     * 
     * @param lt element that should change its selection
     * @param selection the desired selection
     */
    public void ltSelect(Lt lt, boolean selection, final DataListener srcView, final Object srcInfo)  {
        LTx lts = lt.getParent();      
        
        boolean ambiBefore = lts.isAmbivalent();
        
        lt.setSelected(selection); 

        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cLtSelect, srcView, srcInfo);
        event.lts = lts;
        event.lt = lt;
        //event.location = Location.of(lt, "selection");
        event.ambiChanged = (ambiBefore != lts.isAmbivalent());
        fireEvents(event);
    }

    public void ltSelectAll(Iterable<LTx> ltxs, boolean select, final DataListener srcView, final Object srcInfo)  {

        // --- (de)select all items for all ltxs ---
        for (LTx ltx : ltxs) {
            for (Lt e : ltx.col()) {
                e.setSelected(select);
            }
        }
        
        // --- the whole ss changed ---
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiLtxSelect, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }
    
    /**
     * Batch selection of several LT items. 
     * Fires a single MLayerEvent notifying listeners the the LT structure selection has changed.
     * 
     * @param ltx structure whose items schould be selected or deselected
     *
     * E.g. In LTDisambPanel, a lemma was deselected, therefore deselect all items with that lemma.
     */
    public void ltSelectAll(LTx ltx, boolean select, final DataListener srcView, final Object srcInfo)  {
        boolean ambiBefore = ltx.isAmbivalent();

        // --- deselect everything, except aWhat ---
        for (Lt e : ltx.col()) {
            e.setSelected(select);
        }
        
        // --- the whole ss changed ---
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cLtSelect, srcView, srcInfo);
        event.lts = ltx;
        event.lt = null;
        //todo event.location = Location.of(ss);
        event.ambiChanged = (ambiBefore != ltx.isAmbivalent());
        fireEvents(event);
    }


    public void ltSelectOnly(Iterable<LTx> ltxs, Lt sample, final DataListener srcView, final Object srcInfo)  {
        for (LTx ltx : ltxs) {
            Lt lt = findSimilarLt(ltx, sample);
            for (Lt e : ltx.col()) {
                e.setSelected( e == lt );
            }
        }

        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cMultiLtxSelect, srcView, srcInfo);
        event.ltxs = ltxs;
        fireEvents(event);
    }
    
    /*
     * Selects the specified lt item, all other items within the same structure  are deselected. 
     *
     * Fires a single MLayerEvent notifying listeners the the LT structure selection has changed.
     *
     * @param aWhat item to keep selected; cannot be null
     */
    public void ltSelectOnly(Lt lt, final DataListener srcView, final Object srcInfo)  {
        LTx lts = lt.getParent();        // aWhat's ss (lemmas for a lemma, tags for a tag)
        
        boolean ambiBefore = lts.isAmbivalent();

        // --- deselect everything, except aWhat ---
        for (Lt e : lts.col()) {
            e.setSelected( e == lt );
        }
        
        final MLayerEvent event = new MLayerEvent(this, MLayerEvent.cLtSelect, srcView, srcInfo);
        event.lts = lts;
        event.lt = null;
        //todo event.location = Location.of(lts);
        event.ambiChanged = (ambiBefore != lts.isAmbivalent());
        fireEvents(event);
    }
}
