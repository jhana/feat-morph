package org.purl.jh.law.data.m;

import java.util.Set;
import lombok.Value;

/**
 *
 */
@Value
public class SimpleTagset {
    String id;
    String descr;
    Set<String> tags;
}
