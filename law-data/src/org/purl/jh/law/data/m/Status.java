package org.purl.jh.law.data.m;

/**
 * Status of an item.
 * @author jirka
 */
public enum Status {
    TODO,   // has not been touched
    DONE,   // finished
    TOREVIEW    // done, but needs review
}
