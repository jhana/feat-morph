package org.purl.jh.law.data.m;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 * Registry of tagsets.
 */
public enum Tagsets {
    INSTANCE;

    @Getter @Setter SimpleTagset defTagset;
    
    @Getter Map<String,SimpleTagset> tagsets = new HashMap<>();
}    
