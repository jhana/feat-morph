package org.purl.jh.law.data.m;

import java.util.ArrayList;
import java.util.Collection;
import org.purl.jh.util.col.Cols;

/**
 * @todo improve
 * @author Jiri
 */
public class LtPrinter  {
    boolean mLemmas;
    boolean mTags;

    boolean mAbbrLemmas;
    boolean mAbbrTags;
    
    boolean mSelectedOnly;
    
    public LtPrinter() {
        this(false, false, true, true, true);
    }
    
    /** Creates a new instance of LtPrinter */
    public LtPrinter(boolean aLemmas, boolean aTags, boolean aAbbrLemmas, boolean aAbbrTags, boolean aSelectedOnly) {
        mLemmas = aLemmas;
        mTags = aTags;
        mAbbrLemmas = aAbbrLemmas;
        mAbbrTags = aAbbrTags;
        mSelectedOnly = aSelectedOnly;
    }

// -----------------------------------------------------------------------------    
// Configuration
// -----------------------------------------------------------------------------    
    
    public boolean getShowLemmas() {
        return mLemmas;
    }

    public void setShowLemmas(boolean aShow) {
        mLemmas = aShow;
    }
    
    public boolean getShowTags() {
        return mTags;
    }

    public void setShowTags(boolean aShow) {
        mTags = aShow;
    }
    
    public boolean getShowSelectedOnly() {
        return mSelectedOnly;
    }
    
    public void setShowSelectedOnly(boolean aSelectedOnly) {
        mSelectedOnly = aSelectedOnly;
    }

    private void printConfig() {
        System.out.printf("Lemmas %s, Tags %s, Selected Only: %s\n", mLemmas, mTags, mSelectedOnly);
    }
        
    
// -----------------------------------------------------------------------------    
//
// -----------------------------------------------------------------------------    
    
    public String toString(LTx aSs) {
        return Cols.toString( getDeep(aSs) );
    }
        
    public Collection<?> getDeep(LTx aLTs) {
        if (mLemmas && mTags) {
            return getDeepAll(aLTs);
        }
        else if (mLemmas) {
            return aLTs.getLemmas(mSelectedOnly);
        }
        else
            return aLTs.getTags(mSelectedOnly);
    }

    /**
     * @todo eff improve 
     * @return list where odd elements are lemmas and even tag lists
     */
    public Collection<?> getDeepAll(LTx aLTs) {
        ArrayList<Object> list = new ArrayList<>(2*aLTs.size());
        for (String lemma : aLTs.getLemmas(mSelectedOnly)) {
            list.add(lemma);                // lemma
            list.add(aLTs.getTags(lemma, mSelectedOnly) );  // tags
        }
        return list;
    }     
}
