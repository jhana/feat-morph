package org.purl.jh.law.data.io;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.openide.filesystems.FileObject;
import org.purl.jh.law.data.m.Doc;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.data.m.Para;
import org.purl.jh.law.data.m.Sentence;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.law.data.m.Tagsets;
import org.purl.jh.pml.Element;
import org.purl.jh.pml.ListElement;
import org.purl.jh.pml.StringElement;
import org.purl.jh.pml.io.NbLoader;
import org.purl.jh.pml.io.XReader;
import org.purl.jh.util.col.Cols;
import org.purl.jh.util.err.ErrorHandler;
import org.purl.jh.util.err.XException;
import org.purl.jh.util.io.LineReader;


/**
 * Reads in MLayer from a file in vertikala format. 
 * The code does some basic format checking and limited error recovery (only <p> and <s> is added if necessary).
 * <p>
 * Note: The sgml-tags do not hava to be nested, so <a> <b> </a> </b> is a legal sequence.
 */
public class VertReader implements XReader<MLayer> {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(VertReader.class);
    
    /**
     * File containing the layer read in.
     */
    protected FileObject fileObject;
    
    /** todo experimental - reacts to error/warnings when processing the document, currently just reports them */
    protected ErrorHandler err;

    /** Keeps track of tag lengths - so we can report a warning if there are multiple */
    private final Multiset<Integer> tagLens = HashMultiset.create();

    /**
     * The data being read in.
     */
    protected MLayer data;

    // to handle paragraph which are incorrectly not closed
    private Doc curDoc = null;
    private Para curPara = null;
    private Sentence curSent = null;
    
    // todo to allow before annotations for 
    private String curBefore = null;

    // So that each form knows the page it is on
    private String curPage = null;
    
    @Override
    public MLayer read(FileObject fileObject, NbLoader aLayerLoader, ErrorHandler err) {
        log.info("readLayer %s", fileObject);
        this.err = err;
        this.fileObject = fileObject;
        this.data = createLayer();

        processLines(fileObject);

        warning(tagLens.size() == 1, "Tags vary in length.");
        
        return data;
    }


    protected MLayer createLayer() {
        return new MLayer(fileObject, "M");
    }

    private final Multiset<String> curOpen = HashMultiset.create();
    
    /** Keeps track of the currently opened ignoring/non-ignoring mode */
    private final Multiset<String> curIgnoring = HashMultiset.create();
    
    // for error reporting only
    private int lineNr = 0;
    private String lineStr = null;    
    
    private void processLines(FileObject fileObject) {
        data.setTagset( Tagsets.INSTANCE.getDefTagset() );  // todo should be read from the file

        for (String line : readLines(fileObject, StandardCharsets.UTF_8)) {
            lineStr = line;
            lineNr++;

            line = line.trim();
            //System.err.println("Line:" +    line);
            
            if (SgmlTag.isTag(line)) {
                SgmlTag tag = SgmlTag.create(line);

                switch (tag.getType()) {
                    case OPEN:   openTag(tag); break;
                    case CLOSE:  closeTag(tag); break;
                    case SINGLE: singleTag(tag); break;
                }
            }                
            else {
                other(line);
            }
        }
        
        warning(curOpen.isEmpty(), "Unclosed tags when document ended (%s).", curOpen.elementSet());
    }

    private Element openTag(SgmlTag tag) {
        checkOpen(tag);
        Element e = openTagSw(tag);
        curOpen.add(tag.getCore());
        return e;
    }
    
    private Element openTagSw(SgmlTag tag) {
        
        switch (tag.getCore()) {
            case VertTags.DOC: 
                return handleOpenDoc(tag); 

            case VertTags.PARA: 
                return handleOpenPara(tag); 

            case VertTags.SENT:
                return handleOpenSentence(tag);
                
            // anything between these tags is ignored on m-layer
            case VertTags.h:
            case VertTags.e:
            case VertTags.o:
                return handleOpenIgnoreTag(tag);
                
            case VertTags.page:
                curPage = tag.getStr();
                //return handleOpenWExtraTag(tag);
                return handleOpenIgnoreTag(tag);
                
            // additional unsupported tags 
            default:
                return handleOpenWExtraTag(tag);
        }
    }

    private void checkOpen(SgmlTag tag) {
        if (
            VertTags.cannotBeNested.contains(tag.getCore()) && 
            curOpen.contains(tag.getCore())) {
                warning("Tag <%s> cannot be nested, close it first.", tag.getCore());
                curOpen.remove(tag.getCore());  // to prevent more errors for the same reason
                return; 
        }

        // other specific requirements
        switch (tag.getCore()) {
            case VertTags.page: 
                warning(! curOpen.contains(VertTags.e), "Tag %s cannot be within <%s>.", tag.getStr(), VertTags.e);
                break;
            case VertTags.e: 
                warning(! curOpen.contains(VertTags.page), "Tag %s cannot be within <%s>.", tag.getStr(), VertTags.page);
                break;
            default:
        }
    }
    
    private void closeTag(SgmlTag tag) {
        warning( curOpen.contains(tag.getCore()), "Closing tag %s was never opened.", tag.getStr() );
        
        switch (tag.getCore()) {
            case VertTags.DOC: 
                handleCloseDoc(tag); 

            case VertTags.PARA: 
                handleClosePara(); 
                break;

            case VertTags.SENT: 
                handleCloseSentence();
                break;
                
            case VertTags.h:
            case VertTags.e:
            case VertTags.o:
            case VertTags.page:
                handleCloseIgnoreTag(tag);
                break;

//            case VertTags.page:
//                curBefore = null;
//                handleCloseWExtraTag(tag);
//                break;
                
            default:
                handleCloseWExtraTag(tag);
                break;
        }
        curOpen.remove(tag.getCore());
    }

    private void singleTag(SgmlTag tag) {
        data.add(new StringElement(tag.getStr()));
    }

    private void other(String str) {
        if (curIgnoring.isEmpty()) {
            word(str);
        }
        else {
            // page change, remember current page to annotate each following form with it
            if (curOpen.contains(VertTags.page)) {
                this.curPage = str;
            }
            getCurElement().add(new StringElement(str));
        }
    }

    private void word(String str) {
        robustEnsureInSentence();
        Preconditions.checkNotNull(curSent.getParent());
        
        final String[] parts = str.split("\t");
        
        LTx ltx = new LTx();
        curSent.add(ltx);
        data.getFlatLts().add(ltx);
        ltx.setBefore(curBefore);
        ltx.setPage(curPage);

        String form = parts[0];
        ltx.setForm(form); // todo add directly to ltx
        ltx.setStatus(Status.TODO);

        if (parts.length == 1) return;
            
        int end = parts.length;
        String last = parts[end-1];
        if (SgmlTag.isTag(last)) {
            switch (SgmlTag.create(last).getCore()) {
                case VertTags.STATUS_TODO:
                    ltx.setStatus(Status.TODO);
                    break;
                case VertTags.STATUS_DONE:
                    ltx.setStatus(Status.DONE);
                    break;
                case VertTags.STATUS_TOREVIEW:
                    ltx.setStatus(Status.TOREVIEW);
                    break;
                default:
                    warning("Unsupported tag (%s) ignored.", last);
                    break;
            }
            end --;
        }
        
        List<String> lemmaTags = Cols.subList(parts, 1, end);
        for (String lemmaTag : lemmaTags) {
            String[] parts2 = lemmaTag.split("\\s+");
            String lemma = parts2[0];
            List<String> tags = Cols.subList(parts2, 1);

            for (String tag : tags) {
                Lt lt = new Lt();
                lt.setLemma(lemma);
                lt.setTag(tag);
                tagLens.add(tag.length());
                
                lt.setSelected(true);
                ltx.add(lt);
            }
        }
        
    }   


    
    private Element handleOpenDoc(SgmlTag tag) {
        check(data == getCurElement(), "Cannot open doc tag: %s != %s", tag, getCurElement());
        warning("Opening doc: %s", tag.getStr());
        
        curDoc = new Doc(data, tag.getStr(), tag);     // use tag as the id
        data.add(curDoc);

        return curDoc;
    }

    private void handleCloseDoc(SgmlTag tag) {
        robustEnsureParaClosed();
        
        if (curDoc == null) warning("Closing document, but none was opened");

        curDoc = null;
    }
    
    private Element handleOpenPara(SgmlTag tag) {
        robustEnsureParaClosed();

        curPara = new Para(tag);
        curDoc.add(curPara);

        return curPara;
    }

    private void handleClosePara() {
        robustEnsureSentenceClosed();

        if (curPara == null) warning("Closing paragraph, but none was opened");

        curPara = null;
    }

    
    private Element handleOpenSentence(SgmlTag tag) {
//        robustEnsureInDoc();
        robustEnsureInPara();
        curSent = new Sentence(tag);
        curPara.add(curSent);

        return curSent;
    }

    private void handleCloseSentence() {
        //Preconditions.checkState(curSent != null, "[%s] Closing sentece, but none was opened\n%s", lineNr, lineStr);
        curSent = null;
    }

    
    
    private Element handleOpenIgnoreTag(SgmlTag tag) {
        Element e = handleOpenWExtraTag(tag);
        curIgnoring.add(tag.getCore());
        return e;
    }

    private void handleCloseIgnoreTag(SgmlTag tag) {
        handleCloseWExtraTag(tag);
        curIgnoring.remove(tag.getCore());
    }

    private Element handleOpenWExtraTag(SgmlTag tag) {
        Element e = new StringElement(tag.getStr());
        getCurElement().add(e);
        return e;
    }

    private void handleCloseWExtraTag(SgmlTag tag) {
        getCurElement().add(new StringElement(tag.getStr()));
    }

    private ListElement<Element> getCurElement() {
        if (curSent != null) {
            return curSent;
        }
        else if (curPara != null) {
            return curPara;
        }
        else if (curDoc != null) {
            return curDoc;
        }
        else {
            return data;
        }
    }
    
    private void robustEnsureInDoc() {
        if (curDoc == null) {
            autoOpen(VertTags.DOC, "document");
        }
    }

    private void robustEnsureDocClosed() {
        robustEnsureParaClosed();
        if (curDoc != null) {
            autoClose(curPara, VertTags.DOC, "document");
        }
    }

    private void robustEnsureInPara() {
        robustEnsureInDoc();
        if (curPara == null) {
            autoOpen(VertTags.PARA, "paragraph");
        }
    }
    
    private void robustEnsureParaClosed() {
        robustEnsureSentenceClosed();
        if (curPara != null) {
            autoClose(curPara, VertTags.PARA, "paragraph");
        }
    }

    private void robustEnsureInSentence() {
        robustEnsureInPara();
        if (curSent == null) {
            autoOpen(VertTags.SENT, "sentence");
        }
    }
    
    private void robustEnsureSentenceClosed() {
        if (curSent != null) {
            autoClose(curSent, VertTags.SENT, "sentence");
        }
    }

    private void autoOpen(String core, String desc) {
        warning("Opening %s tag is missing", desc);
        Element e = openTag(SgmlTag.createOpening(core));
        data.getAutoAddedOpen().add(e);
    }
    
    private void autoClose(Element what, String core, String desc) {
        warning("Closing %s tag is missing", desc);
        data.getAutoAddedClose().add(what);
        closeTag(SgmlTag.createClosing(core));
    }
    
    private void warning(boolean test, String format, Object ... params) {
        if (!test) warning(format, params);
    }
    
    private void warning(String format, Object ... params) {
        String msg = 
                String.format("[%s] ", lineNr) +
                String.format(format,params) +
                "\n" + lineStr;

        err.warning(msg);
    }

    private void check(boolean test, String format, Object ... params) {
        if (test) return;
        
        String msg =
                String.format("[%s] ", lineNr) +
                String.format(format,params) +
                "\n" + lineStr;
        err.severe(msg);
        throw new XException(msg);
    }
    
    /** 
     * Reads all lines from a fileobject.
     * @param fileObject
     * @param charset
     * @return 
     * 
     * @todo this is in nbutil/org.purl.net.jh.nbutil.io.FObjs, but that would require dependence on nb* module
     */
    public static Iterable<String> readLines(FileObject fileObject, Charset charset) {
        final List<String> lines = new ArrayList<>();
        
        try ( LineReader r = new LineReader(new InputStreamReader(fileObject.getInputStream(), charset)) ) {
            for (;;) {
                String line = r.readLine();
                if (line == null) break;
                lines.add(line);
            }
        }
        catch (IOException  e) {
            throw new XException(e);
        }
        
        return lines;
    }


    
}
