package org.purl.jh.law.data.io;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

public final class VertTags {
    public static final String DOC = "doc";

    public static final String PARA = "p";
    /** title */
    public static final String k = "k";
    public static final String SENT = "s";
    /** versovany */
    public static final String v = "v";
    /** footnote */
    public static final String n = "n";
    /** formatted text */
    public static final String f = "f";
    /** crossed out */
    public static final String x = "x";
    
    /** metadata */
    public static final String h = "h"; 
    public static final String page = "str";
    public static final String e = "e";
    public static final String o = "o";

    public static final SgmlTag TAG_DOC_OPEN = SgmlTag.createOpening(DOC);
    public static final SgmlTag TAG_DOC_CLOSE = SgmlTag.createClosing(DOC);;

    public static final SgmlTag TAG_PARA_OPEN = SgmlTag.createOpening(PARA);
    public static final SgmlTag TAG_PARA_CLOSE = SgmlTag.createClosing(PARA);;

    public static final SgmlTag TAG_SENT_OPEN = SgmlTag.createOpening(SENT);
    public static final SgmlTag TAG_SENT_CLOSE = SgmlTag.createClosing(SENT);;

    public static final String STATUS_TODO = "todo";
    public static final String STATUS_DONE = "done";
    public static final String STATUS_TOREVIEW = "review";
    
    public static final SgmlTag TAG_STATUS_TODO = SgmlTag.createSingle(STATUS_TODO);
    public static final SgmlTag TAG_STATUS_DONE = SgmlTag.createSingle(STATUS_DONE);
    public static final SgmlTag TAG_STATUS_TOREVIEW = SgmlTag.createSingle(STATUS_TOREVIEW);

    
    public static final Set<String> IGNORING = ImmutableSet.of(h, page, e, o);
    
    // cannot be nested in itsef (e.g. para within para). It might be sometimes nested in one another (sent within para)
    public static final Set<String> cannotBeNested = ImmutableSet.of(PARA, k, SENT, v, n, f, x, page, e, o);
    
    
    public static boolean ignore(String core) {
        return h.equals(core) || page.equals(core) || e.equals(core) || o.equals(core);
    }
                
    // w extra is the rest
}
