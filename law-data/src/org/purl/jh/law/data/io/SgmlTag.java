package org.purl.jh.law.data.io;

import com.google.common.base.Preconditions;
import lombok.Data;

/**
 *
 */
@Data
public class SgmlTag {

    public enum Type {
        OPEN, CLOSE, SINGLE
    }

    private final String str;
    private final String core;
    private final Type type;
    
//    private static final Pattern openingTag = Pattern.compile("<[^/][^>]*>");
//    private static final Pattern closingTag = Pattern.compile("</[^>]+>");
//    private static final Pattern singleTag = Pattern.compile("<[^>]+/>");

    public static boolean isTag(String str) {
        return str != null && str.startsWith("<") && str.endsWith(">");
    }

    public static SgmlTag createOpening(String core) {
        return create("<" + core + ">");
    }

    public static SgmlTag createClosing(String core) {
        return create("</" + core + ">");
    }

    public static SgmlTag createSingle(String core) {
        return create("<" + core + "/>");
    }
    
    public static SgmlTag create(String str) {
        Preconditions.checkArgument(str.startsWith("<") && str.endsWith(">"));
        if (str.startsWith("</")) {
            String core = str.substring(2, str.length() - 1).trim();
            return new SgmlTag(str, core, Type.CLOSE);
        } else if (str.endsWith("/>")) {
            String core = str.substring(1, str.length() - 2).trim();
            return new SgmlTag(str, core, Type.SINGLE);
        } else {
            String core = str.substring(1, str.length() - 1).trim();
            if (core.contains(" ")) {
                core = core.substring(0, core.indexOf(" "));
            }
            return new SgmlTag(str, core, Type.OPEN);
        }
    }
    
} 