package org.purl.jh.law.data.io;

/**
 *
 * @author jirka
 */
public class Pdt {
    public final static String cNameSpace = "http://ufal.mff.cuni.cz/pdt/pml/";

    public final static String cWSchema = "wdata_schema.xml";
    public final static String cMSchema = "mdata_schema.xml";
    public final static String cASchema = "adata_schema.xml";
    public final static String cTSchema = "tdata_schema.xml";

}
