package org.purl.jh.law.data.io;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import org.openide.filesystems.FileObject;
import org.purl.jh.law.data.m.Doc;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.data.m.Para;
import org.purl.jh.law.data.m.Sentence;
import org.purl.jh.pml.Element;
import org.purl.jh.pml.StringElement;
import org.purl.jh.pml.io.XWriter;
import org.purl.jh.util.err.ErrorHandler;
import org.purl.jh.util.err.XException;

public class VertWriter implements XWriter<MLayer> {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(VertWriter.class);

    private PrintWriter w;
    
    /**
     * File containing the layer read in.
     */
    protected FileObject fileObject;
    
    /** todo experimental - reacts to error/warnings when processing the document; todo add to save as a param*/
    protected ErrorHandler err;

    /**
     * The data being read in.
     */
    protected MLayer data;

    /** 
     * SHould auto-added elements be written out? 
     * Note that currently only auto-added paragraphs and sentences are checked.
     * 
     * @see MLayer#autoAddedOpen
     * @see MLayer#autoAddedClose
     * @todo make configurable
     */
    private boolean writeAdded = false;

    @Override
    public void save(MLayer data, FileObject fileObject) {
        this.data = data;
        this.fileObject = fileObject;

        try (
            PrintWriter w = this.w = new PrintWriter(new OutputStreamWriter(fileObject.getOutputStream(), StandardCharsets.UTF_8))
        ) {
            write();
        } catch (IOException ex) {
            throw new XException(ex);
        }
    }

    private void write() {
        for (Element e : data.col()) {
            if (e instanceof Doc) {
                writeDoc((Doc)e);
            }
            else if (e instanceof StringElement) {
                w.println( ((StringElement)e).getString() );
            }
            else {
                warning("Unknown object to save %s", e.getClass());
            }
        }
    }
    
    private void writeDoc(Doc doc) {
        writeOpening(doc, doc.getTag().getStr());
        for (Element e : doc.col()) {
            if (e instanceof Para) {
                writePara((Para)e);
            }
            else if (e instanceof StringElement) {
                w.println( ((StringElement)e).getString() );
            }
            else {
                warning("Unknown object to save %s", e.getClass());
            }
        }
        writeClosing(doc, VertTags.TAG_DOC_CLOSE.getStr());
    }

    private void writePara(Para para) {
        writeOpening(para, para.getTag().getStr());
        for (Element e : para.col()) {
            if (e instanceof Sentence) {
                writeSentence((Sentence)e);
            }
            else if (e instanceof StringElement) {
                w.println( ((StringElement)e).getString() );
            }
            else {
                warning("Unknown object to save %s", e.getClass());
            }
        }
        writeClosing(para, VertTags.TAG_PARA_CLOSE.getStr());
    }
    
    private void writeSentence(Sentence sentence) {
        writeOpening(sentence, sentence.getTag().getStr());
        for (Element e : sentence.col()) {
            if (e instanceof LTx) {
                writeForm((LTx)e);
            }
            else if (e instanceof StringElement) {
                w.println( ((StringElement)e).getString() );
            }
            else {
                warning("Unknown object to save %s", e.getClass());
            }
        }
        writeClosing(sentence, VertTags.TAG_SENT_CLOSE.getStr());
    }

    private void writeForm(LTx ltx) {
        w.append(ltx.getForm());

        String curLemma = null;
        for (Lt lt : ltx.col()) {
            if (!lt.isSelected()) continue;
            if ( !lt.getLemma().equals(curLemma) ) {
                w.append("\t");
                w.append(lt.getLemma());
                curLemma = lt.getLemma();
            }
            w.append(" ");
            w.append(lt.getTag());
        }

        w.append("\t");
        switch(ltx.getStatus()) {
            case TODO: w.append(VertTags.TAG_STATUS_TODO.getStr()); break;
            case DONE: w.append(VertTags.TAG_STATUS_DONE.getStr()); break;
            case TOREVIEW: w.append(VertTags.TAG_STATUS_TOREVIEW.getStr()); break;
        }

        w.println();
    }

    private void writeOpening(Element element, String tag) {
        if (writeAdded || !data.getAutoAddedOpen().contains(element)) {
            w.println(tag);
        }
    }

    private void writeClosing(Element element, String tag) {
        if (writeAdded || !data.getAutoAddedClose().contains(element)) {
            w.println(tag);
        }
    }
    
    
    private void warning(String format, Object ... params) {
        String msg = "[Warning " + String.format(format,params);
        System.err.println(msg);
    }

   
}
