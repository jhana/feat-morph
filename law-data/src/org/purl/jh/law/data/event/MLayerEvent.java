package org.purl.jh.law.data.event;

import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.pml.event.DataListener;
import org.purl.jh.pml.Element;
import org.purl.jh.pml.event.DataEvent;
import org.purl.jh.pml.location.Location;

/**
 * Events occurring on the Pdt m-layer
 * @author Jirka Hana (jirka ddot hana aat gmail ddot com)
 */
public class MLayerEvent extends DataEvent<MLayer> {
    // changes the form of ltx(s)
    public static final String cSetForm = "setForm"; 
    public static final String cMultiSetForm = "multiSetForm"; 

    // changes the attention-needed flag
    public static final String cSetStatus = "setStatus"; 
    public static final String cMultiSetStatus = "multiSetStatus"; 
    
    // added/removed lt
    public static final String cLtxAdd = "ltxAdd"; 
    public static final String cLtxDel = "ltxDel"; 

    public static final String cMultiLtxAdd = "multiLtxAdd";    // added to many ltxs
    public static final String cMultiLtxDel = "multiLtxDel";    // delete from many ltxs

    // lt(s) was (de)selected
    public static final String cMultiLtxSelect  = "multiLtxSelect";     // changes selection within multiple ltx-s
    public static final String cMultiLtLemmaTag    = "multiLtxSetLemmaTag"; 
    
    public static final String cLtSelect  = "ltSelect"; 
    public static final String cLtLemma   = "ltSetLemma"; 
    public static final String cLtTag     = "ltSetTag"; 

    public static final String cComment   = "setComment"; 

    public static final String cOther = "other"; // full refres, ?todo: push down?

    public MLayerEvent(MLayer aSrcLayer, String aType, DataListener aSrcView, Object aSrcViewInfo) {
        super(aSrcLayer, aType, aSrcView, aSrcViewInfo);
    }
    
    public MLayerEvent(MLayer aSrcLayer, String aType) {
        this(aSrcLayer, aType, null, null);
    }

    public Element element;
    //public String property;

    public Iterable<LTx> ltxs;
    public LTx lts;
    public Lt lt;
    public Location location;
    public boolean ambiChanged;


    public Object old;
}
