//package org.purl.jh.law.data.w;
//
//
//
//import org.purl.jh.law.data.m.Para;
//import org.purl.jh.law.data.m.Doc;
//import org.purl.jh.pml.Element;
//
///**
// *
// * @author Jiri
// */
//public class DocFormatter {
//    public static String format(Doc aDoc) {
//        return new DocFormatter(aDoc).mSb.toString();
//    }
//
//// =============================================================================
//// Implementation
//// =============================================================================    
//    private Doc mDoc;
//    private StringBuilder mSb;
//
//    /** Creates a new instance of DocFormatter */
//    private DocFormatter(Doc aDoc) {
//        mDoc = aDoc;
//        fillDoc();
//    }
//
//    private void fillDoc() {
//        mSb = new StringBuilder(mDoc.size() * 7);
//
//        for (Element element : mDoc.col()) {
//            if (element instanceof Para) {
//                addPara((Para)element);
//            }
//        }
//    }   
//
//    private void addPara(Para aPara) {
//        for (Element element : aPara.col()) {
//            if (element instanceof WForm) {
//                WForm f = (WForm) element;
//                f.setDocOffset(mSb.length());
//                mSb.append(f.getFormStr());
//                if (f.hasSpaceAfter()) mSb.append(' ');
//            }
//        }
//        mSb.append('\n');
//    }
//}
