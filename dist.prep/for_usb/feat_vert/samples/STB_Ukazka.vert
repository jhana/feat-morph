<doc autor="[neuved4no]" titul="[®ivot Krista Pána, rukopis B]" datacePramene="konec 14. století" predlohaTyp="rukopis" predlohaStat="Česko" predlohaMesto="Praha" predlohaInstituce="Národní knihovna České republiky" predlohaSignatura="XVII D 32" predlohaEdiceT8tul="[neuvedeno]" predlohaEdiceE3itor="[neuvedeno]" predlohaEdiceM8stoVydani="[neuvedeno]" predlohaEdiceRokVydani="[neuvedeno]" dataceStoleti="14" zkratkaPamatky="Krist" zkratkaPramene="KristB" literarniDruh="próza" literarniZanr="[neuvedeno]" edicniPoznamka="KristB" id="df3d9957-e158-4831-afa8-2c10f795c33c">
<folio cislo="1r">
<podnadpis>
De	de RR--1----------
resurecione	resurecione X@-------------
domini	domini aa
</podnadpis>
<odstavec>
"	" Z:-------------
Vizi	vize NNFS4-----A----
to	ten PDNS1----------
,	, Z:-------------
hospodine	hospodin NNMS5-----A----
milý	milý NNMS5-----A----
,	, Z:-------------
jenľ	jenľ PJIS1----------
sě	sě X@-------------
prvé	prvý AAFS2----1A----
rady	rada NNFS2-----A----
své	svůj P8FS2---------1
Spravedlnosti	spravedlnost NNFS2-----A----
jiľ	jiľ Db-------------
dlúho	dlúho X@-------------
drľíą	drľet VB-S---2P-AA---
,	, Z:-------------
pro	pro RR--4----------
niľto	jenľ P9FS4---------2
sě	sě X@-------------
na	na RR--4----------
své	svůj P8NS4---------1
stvořenie	stvořenie X@-------------
velmi	velmi Db-------------
hněváą	hněvat VB-S---2P-AA---
.	. Z:-------------
A	a J^-------------
jiľ»	jiľ» X@-------------
jest	být VB-S---3P-AA--2
čas	čas NNIS1-----A----
,	, Z:-------------
aby	aby J,-------------
sě	sě X@-------------
rozpomanul	rozpomanul X@-------------
na	na RR--4----------
svá	svůj P8NP4---------1
milosrdenstvie	milosrdenstvie X@-------------
,	, Z:-------------
neb	nebo J^------------2
ty	ty PP-S1--2-------
jsi	být VB-S---2P-AA---
ten	ten PDMS1----------
,	, Z:-------------
jenľto	jenľ PJMS1---------2
v	v RR--6----------
svém	svůj P8NS6----------
boství	boství X@-------------
milosrdenstvie	milosrdenstvie X@-------------
neseą	nést VB-S---2P-AA---
nade	nad RV--4----------
vąě	vąě X@-------------
svá	svůj P8NP4---------1
učiněnie	učiněnie X@-------------
.	. Z:-------------
A	a J^-------------
kdyľ	kdyľ J,-------------
milosrdenstvie	milosrdenstvie X@-------------
v	v RR--6----------
tobě	ty PP-S6--2-------
bydlí	bydlet VB-P---3P-AA---
,	, Z:-------------
kak	kak X@-------------
to
móľ	móľ X@-------------
býti	být Vf--------A---2
,	, Z:-------------
by	být Vc-------------
sě	sě X@-------------
nikda	nikda X@-------------
nesmiloval	smilovat VpMS---1R-NA---
?	? Z:-------------
Která	který P4FS1----------
tobě	ty PP-S3--2-------
chvála	chvála NNFS1-----A----
ottud	ottud X@-------------
přicházie	přicházie X@-------------
,	, Z:-------------
eľ	eľ X@-------------
,	, Z:-------------
jsa	být VeIS------A----
přěvąemohúcí	přěvąemohúcí X@-------------
hospodin	hospodin NNMS1-----A----
,	, Z:-------------
nad	nad RR--7----------
hubeným	hubený AANS7----1A----
sě	sě X@-------------
stvořením	stvoření NNNS7-----A----
mstíą	mstít VB-S---2P-AA---
?	? Z:-------------
Voliv	Voliv X@-------------
sobě	se P6-S3----------
milé	milý AAMP4----1A----
sluhy	sluha NNMP4-----A----
,	, Z:-------------
jichľto	jenľ PJMP2---------2
s	s RR--2----------
srdce	srdce NNNS2-----A----
Duchem	duch NNMS7-----A----
svatým	svatý AAMS7----1A----
osvěcoval	osvěcovat VpMS---3R-AA---
,	, Z:-------------
jimľto	jenľ PJMP3---------2
s	s RR--7----------
svá	svůj AONP-----------
tajenstvie	tajenstvie X@-------------
u	u RR--2----------
budúcích	budúcích X@-------------
časiech	časiech X@-------------
zjěvoval	zjěvoval X@-------------
,	, Z:-------------
vąecky	vąecek PLMP4----------
v	v RR--6----------
ľaláři	ľalář NNIS6-----A----
temném	temný AAIS6----1A----
drľíą	drľet VB-S---2P-AA---
.	. Z:-------------
Mojľieąě	Mojľieąě X@-------------
,	, Z:-------------
s	s RR--7----------
nímľto	jenľ P9MS7---------2
s	s RR--7----------
někda	někda X@-------------
milostivě	milostivě Dg-------1A----
mluvil	mluvit VpIS---3R-AA---
,	, Z:-------------
jemuľto	jenľ PJMS3---------2
s	s RR--7----------
své	svůj AONS-----------
boľstvie	boľstvie X@-------------
nad	nad RR--7----------
přirozenie	přirozenie X@-------------
člověčie	člověčie X@-------------
u	u RR--2----------
vidění	vidění NNNS2-----A----
brzkosti	brzkost NNFS2-----A----
oka	oko NNNS2-----A----
mľenie	mľenie X@-------------
ukázal	ukázat VpMS---1R-AA---
.	. Z:-------------
Tak	tak Db-------------
liľ	liľ J,-------------
chceą	chtít VB-S---2P-AA---
na	na RR--4----------
věky	věk NNIP4-----A----
v	v RR--6----------
svém	svůj P8MS6----------
hněvě	hněvě X@-------------
zadrľěti	zadrľěti X@-------------
svá	svůj P8NP4---------1
milosrdenstvie	milosrdenstvie X@-------------
?	? Z:-------------
Ne	ne TT-------------
tak	tak Db-------------
,	, Z:-------------
milý	milý AAMS5----1A----
hospodine	hospodin NNMS5-----A----
,	, Z:-------------
ale	ale J^-------------
jiľtě	jiľtě X@-------------
čas	čas NNIS4-----A----
,	, Z:-------------
aby	aby J,-------------
sě	sě X@-------------
nad	nad RR--7----------
hřieąnými	hřieąnými X@-------------
smiloval	smilovat VpMS---3R-AA---
!	! Z:-------------
"	" Z:-------------
Tehda	tehdy Db------------7
bóh	bóh X@-------------
Otec	otec NNMS1-----A----
sě	sě X@-------------
na	na RR--4----------
Spravedlnost	spravedlnost NNFS4-----A----
ozřěl	ozřěl X@-------------
,	, Z:-------------
k	k RR--3----------
její	jeho PSFS3FS3-------
dávnému	dávný AANS3----1A----
promluvení	promluvení NNNS3-----A----
propověděl	propověděl X@-------------
a	a J^-------------
řka	říci VeMS------A----
:	: Z:-------------
</odstavec>
</folio>
</doc>
</s>
</p>
<s />
<s />
<s />
<s />
<s />
<s />
<s />
<s />
<s />
<s />
