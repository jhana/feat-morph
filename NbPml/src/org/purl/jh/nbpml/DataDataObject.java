package org.purl.jh.nbpml;

import java.io.IOException;
import org.netbeans.spi.actions.AbstractSavable;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Lookup;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.purl.jh.pml.Data;
import org.purl.jh.pml.event.DataEvent;
import org.purl.jh.pml.event.DataListener;
import org.purl.jh.pml.io.NbLoader;
import org.purl.jh.pml.io.XReader;
import org.purl.jh.pml.io.XWriter;
import org.purl.jh.util.err.Err;
import org.purl.jh.util.err.ErrorHandler;
import org.purl.jh.util.err.FormatError;
import org.purl.jh.util.err.UserOut;
import org.purl.jh.util.err.XException;
import org.purl.net.jh.nbutil.XDataObject;

/**
 * A wrapper around a layer/tagset object to fit into the NB Platform.
 * 
 * @param <L> Data (layer, tagset) this DataObject wraps.
 */
public abstract class DataDataObject<L extends Data<?>> extends XDataObject {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(DataDataObject.class);

    /** Data (layer, tagset) this DataObject wraps */
    protected transient L data;

    /** Where to print user messages - errors, warnings, ... (esp. on data loading/saving) */
    protected UserOut userOut = null;

    /** Modified flag (unfortunately, DataObjects modified flag is private) */
    protected boolean modif = false;

    /** 
     * Object marking this dobj as savable thus integrating it into NB saving support 
     * (it is in lookup when dobj or data is modified) 
     */
    private final MySavable savable;
    
    public DataDataObject(FileObject fo, MultiFileLoader loader) throws DataObjectExistsException {
        super(fo, loader);
        savable = new MySavable(this);
        
//        getCookieSet().assign(SaveAsCapable.class, 
//            (SaveAsCapable) (FileObject folder, String fileName) -> DataDataObject.this.saveAs(folder, fileName)
//        );
    }

    /** 
     * Get the data wrapped by this dobj. The data are loaded if they haven't been loaded before 
     * 
     * @param aErr error handler called in case there are errors/warnings on loading (usually simple UserOut)
     * Use null for a top level dobj - a UserOut will be provided automatically.
     * @see #getData() 
     */
    public L getData(ErrorHandler aErr) {
        loadDataIfNeeded(aErr);
        return data;
    }

    /** 
     * Get the data wrapped by this dobj. The data are loaded if they haven't been loaded before.
     * 
     * Like {@link #getData(ErrorHandler)} but with null error handler.
     */
    public L getData() {
        return getData(null);
    }

    /**
     * Check whether data were loaded.
     */
    public boolean isDataLoaded() {
        return data != null;
    }
    
    protected abstract XReader<L> getReader();

    protected abstract XWriter<L> getWriter();

    public UserOut getUserOut() {
        if (userOut == null) {
            // todo use IOUtils to get the proper colors etc.
            final InputOutput io = IOProvider.getDefault().getIO("User Messages", false);

            userOut = new UserOut(io.getErr()) {
                @Override
                public void show() {
                    io.select();
                    io.setErrVisible(true);
                }
            };
        }
        return userOut;
    }

    @Override
    public boolean isModified() {
        // no need to load data to check this, if data not loaded, then it is not modified
        return super.isModified() || (data != null && getData().isModified());
    }

    protected void loadDataIfNeeded(ErrorHandler aErr) {
        if (data == null) {
            try {
                data = loadData(aErr);
            } catch (FormatError e) {
                throw e;
            } catch (Throwable e) {
                throw new XException(e, "Error loading data");
            }

            addDataChangeListener();
        }
    }

    /**
     * Loads layer representing this dobj's primary file.
     * 
     * @param aErr error handler (currently only UserOut). It might be null, for 
     * top-level layer, then the handler provided by {@link #getUserOut()} is used.
     * 
     * Unfortunately, we use aErr also as a flag if we are dealing with top layer
     * or not (displaying a summary message after loading or failing to load the 
     * layer). If the user provides an error handle for the top layer then they 
     * have to handle the the message themselves.
     * 
     * @return the loaded layer 
     * @throws IOException 
     */
    protected L loadData(ErrorHandler aErr) throws IOException {
        final NbLoader nbLoader = Lookup.getDefault().lookup(NbLoader.class);
        Err.iAssert(nbLoader != null, "Cannot find any NbLoader");

        ErrorHandler err =  aErr == null ? getUserOut() : aErr;

        err.info("Loading %s", getPrimaryFile());
        L tmp = getReader().read(getPrimaryFile(), nbLoader, err);
        
        if (err.getErrorCount() > 0 || err.getWarningCount() > 0) {
            if (aErr == null) { // i.e. it is the top-level dobj
                err.info("There were %d errors and %d warnings!", err.getErrorCount(), err.getWarningCount() );
            }
            
            if (err instanceof UserOut) {
                ((UserOut)err).show();
            }
        }

        return tmp;
    }

    public L read(NbLoader aLayerLoader, ErrorHandler aErr) throws IOException {
        return getReader().read(getPrimaryFile(), aLayerLoader, aErr);
    }

    /** listen to data modifications to set the modified flag and add save cookie */
    private final DataListener layerChangeListener = (DataEvent<?> aE) -> onLayerModified();
    
    private void addDataChangeListener() {
        data.addChangeListener(layerChangeListener);
    }
    
    private void removeDataChangeListener() {
        data.removeChangeListener(layerChangeListener);
    }
    
    
    public void onLayerModified() {
        setModified(data.isModified());
    }

    protected synchronized void save() throws IOException {
        Err.iAssert(data != null, "Cannot save data before loading it");
        getWriter().save(data, getPrimaryFile());

        getData().setModified(false);
        setModified(false);
    }

    public void setUserOut(UserOut userOut) {
        this.userOut = userOut;
    }

    @Override
    public void setModified(boolean modif) { 
        if (this.modif != modif) { 
            this.modif = modif; 
            if (modif) { 
                 savable.add();
                 addToLookup(savable); 
                 super.setModified(modif); 
            } 
            else { 
                 super.setModified(modif); 
                 savable.remove();
                 removeFromLookup(savable); 
           } 
        } 
    } 

//    public DataObject saveAs(FileObject folder, String fileName) throws IOException {
//        loadDataIfNeeded(null);
//        removeDataChangeListener();
//        
//        FileObject newFile = saveTo(folder, fileName);
//        DataDataObject newDObj = (DataDataObject) DataObject.find(newFile);
//        Err.iAssert(newDObj instanceof DataDataObject, "DataObject for %s is not a DataDataObject", newFile);
//
//        newDObj.data = data;
//        newDObj.addDataChangeListener();
//        
//        return newDObj;
//    }
    
    public FileObject saveTo(FileObject folder, String fileName) throws IOException {
        loadDataIfNeeded(null);

        FileObject newFile = folder.getFileObject(fileName);

        if (newFile == null) {
            newFile = FileUtil.createData(folder, fileName);
        }
        
        getWriter().save(data, newFile);
        
        return newFile;
    }    

    static class MySavable extends AbstractSavable {
        private final DataDataObject dobj;
        
        public MySavable(DataDataObject dobj) {
            this.dobj = dobj;
        }
        
        public void add() {
            register();
        }

        public void remove() {
            unregister();
        }
        
        @Override
        protected String findDisplayName() {
            return dobj.getName();
        }
        
        @Override
        protected void handleSave() throws IOException {
            if (dobj.isModified()) {
                dobj.save();
            }
        }

        @Override
        public boolean equals(Object other) {
            if (other instanceof MySavable) {
                return ((MySavable)other).dobj == dobj;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return dobj.hashCode();
        }
    }     
    
}
