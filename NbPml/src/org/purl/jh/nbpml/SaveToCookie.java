package org.purl.jh.nbpml;

import java.io.IOException;
import org.openide.filesystems.FileObject;

/**
 * A copy of the document can be saved to a specified file.
 *
 * @author jirka
 */
public interface SaveToCookie {

    /** 
     * Copies the content of the document to a specified file.
     * @param folder folder to save to.
     * @param name new file name to save to.
     * @throws IOException if the object could not be saved
     */
    void saveTo(FileObject folder, String name) throws IOException;
}
