package org.purl.jh.nbpml;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.util.logging.Level;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.OpenSupport;
import org.openide.loaders.SaveAsCapable;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.purl.jh.pml.location.AcceptingLocation;
import org.purl.jh.pml.location.Location;
import org.purl.jh.util.err.FormatError;

/**
 * Support for opening, opening at location, closing and saving as of dataobjects.
 * The name is not very appropriate - todo rename.
 * 
 * @author jirka
 */
public class OpenAtSupport  extends OpenSupport implements OpenCookie, CloseCookie, OpenAtCookie, SaveAsCapable, SaveToCookie {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(OpenAtSupport.class);

    private final DataDataObject dobj;
    private final Class<? extends ViewSupport> viewSupportClass;
    
    /**
     * 
     * @param dobj dataobject to provide open/save/close support for 
     * @param viewSupportClass provider of the default view for the dataobject
     */
    public OpenAtSupport(final DataDataObject dobj, final Class<? extends ViewSupport> viewSupportClass) {
        super(dobj.getPrimaryEntry());
        this.dobj = dobj;
        this.viewSupportClass = viewSupportClass;
    }

    public ViewSupport getViewSupport() {
        return Preconditions.checkNotNull(Lookup.getDefault().lookup(viewSupportClass));
    }
    
    @Override
    public void openAt(Location aLocation) {
        // todo there might be multiple views of the dobj, some might accept loc, some not, look for AcceptingLocation
        final CloneableTopComponent viewer = openImpl();
        
        if (viewer instanceof AcceptingLocation) {  // todo use lookup
            ((AcceptingLocation)viewer).goToLoc(aLocation);
        }
    }
    

    @Override
    public void open() {
        // run in the awt thread
        Mutex.EVENT.writeAccess((Runnable)() -> openImpl());
    }
    
    protected CloneableTopComponent openImpl() {
        try {
            CloneableTopComponent editor = openCloneableTopComponent();
            editor.requestActive();
            return editor;
        }
        catch(Throwable e) {
            String msg;
            if (e instanceof FormatError) {
                msg = e.getMessage();
            }
            else if (e.getCause() instanceof FormatError) {
                msg = e.getCause().getMessage();
            }
            else {
                // not really expected - 
                msg = e.toString();
            }

            msg = "Error opening file!\n" + msg + "\nSee log for details.";

            // see http://qbeukes.blogspot.com/2009/11/netbeans-platform-notifications.html
            DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(msg, NotifyDescriptor.ERROR_MESSAGE));  

            log.log(Level.INFO, e, "User error: %s", msg);          // if I used severe, NB would consider it a real error and report it.
            return null;
        }
    }
    
    
    @Override
    protected CloneableTopComponent createCloneableTopComponent() {
        final ViewSupport viewSupport = getViewSupport();
        
        // todo use dobj's lookup directly??
        final LayerProvider layerProvider = entry.getDataObject().getNodeDelegate().getLookup().lookup(LayerProvider.class);
        layerProvider.getLayer(null);

        final CloneableTopComponent tc = viewSupport.getTopComponent(entry.getDataObject());

        tc.setDisplayName(entry.getDataObject().getName());
        tc.setToolTipText( FileUtil.getFileDisplayName(entry.getFile()) );

        final TopComponent propertiesTc = WindowManager.getDefault().findTopComponent("properties");
        if (propertiesTc != null) propertiesTc.open();
        final Mode mode = WindowManager.getDefault().findMode("properties");
        if (mode != null) mode.dockInto(propertiesTc);

        // todo why is this here?
        if (propertiesTc != null) propertiesTc.open();

        return tc;
    }

//    private static class SaveAsCapableImpl implements SaveAsCapable {
//
//        private final SourceDataObject obj;
//
//        public SaveAsCapableImpl(SourceDataObject obj) {
//            this.obj = obj;
//        }
//
//        @Override
//        public void saveAs( FileObject folder, String fileName ) throws IOException {
//            OpenAtSupport ces = obj.getLookup().lookup(OpenAtSupport.class);
//            if (ces != null) {
//                ces.saveAs(folder, fileName);
//            }
//        }
//    } 

    public void saveTo(FileObject folder, String fileName) throws IOException {
        dobj.saveTo(folder, fileName);
    }    
    
    /**
     * Save the document under a new file name.
     * 
     * @param folder new folder to save the DataObject to.
     * @param fileName new file name to save the DataObject to.
     * @throws java.io.IOException If the operation failed
     */
    @Override
    public void saveAs(FileObject folder, String fileName) throws IOException {
        if (!(env instanceof Env)) return;
            
//        if (!dobj.isModified()) {
//            //the document is not modified, make a copy of the underlying file
//            DataFolder df = DataFolder.findFolder(folder);
//            FileObject newFile = folder.getFileObject(fileName);
//
//            //the target file already exists => remove
//            if (newFile!= null) {
//                newFile.delete();
//            }
//                
//            newDObj = DataObjectAccessor.DEFAULT.copyRename(dobj, df, getFileNameNoExtension(fileName), newExtension );
//        } else {
//            // the document is modified, save it to the new file
            FileObject newFile = dobj.saveTo(folder, fileName);
            dobj.setModified(false);
            DataObject newDObj = DataObject.find(newFile);
//        }
            
        if (newDObj != null) {
            OpenCookie c = newDObj.getLookup().lookup(OpenCookie.class);

            if (c != null) {
                close(false);   //close the original document
                c.open();       //open the new one
            }
        }
    }     
}
