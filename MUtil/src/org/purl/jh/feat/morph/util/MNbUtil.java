package org.purl.jh.feat.morph.util;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.spi.navigator.NavigatorLookupHint;
import org.openide.util.lookup.InstanceContent;

public class MNbUtil {
    public static NavigatorLookupHint createNavigatorHint(final String mime) {
        return new NavigatorLookupHint() {
            @Override
            public String getContentType() {
                return mime;
            }
        };
    }

    public static <T> void replaceInLookup(InstanceContent lookupContent, T oldObj, T newObj) {
        if (oldObj == newObj) return;
        if (oldObj != null) lookupContent.remove(oldObj);
        if (newObj != null) lookupContent.add(newObj);
    }
 
    public static boolean checkRegex(String regexStr) {
        try {
            Pattern.compile(regexStr);
            return true;
        }
        catch (PatternSyntaxException e) {
            return false;
        }
    }     
}
