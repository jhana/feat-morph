package org.purl.net.jh.nbutil;

import java.awt.Component;
import java.awt.Dimension;
import java.io.PrintWriter;
import javax.swing.JTable;
import javax.swing.UIManager;
import org.openide.windows.IOProvider;

/**
 * Basic utils specific to NB platform.
 * @author jirka
 */
public final class NbUtil {
    private NbUtil() {}

    public static PrintWriter getOut() {
        return IOProvider.getDefault().getIO("Log", false).getOut();
    }

    public static void adjustToFont(final Component component) {
        adjustToFont(component, 1.1f);
    }
    public static void adjustToFont(final Component component, float factor) {
        final Integer fontSize = (Integer) UIManager.get("customFontSize");
        if (fontSize != null) {
            Dimension dim = component.getPreferredSize();
            dim.height = Math.round(fontSize.intValue() * factor);
            component.setPreferredSize(dim); 
        }
    }
    
    public static void adjustToFont(final JTable tbl, float factor) {
        final Integer fontSize = (Integer) UIManager.get("customFontSize");
        if (fontSize != null) {
            int newRowHeight = Math.max( Math.round(fontSize.intValue() * factor), fontSize.intValue() + 5 );
            tbl.setRowHeight( newRowHeight );
        }
    }

}
