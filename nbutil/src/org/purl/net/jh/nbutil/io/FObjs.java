package org.purl.net.jh.nbutil.io;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.openide.filesystems.FileObject;
import org.purl.jh.util.err.XException;
import org.purl.jh.util.io.LineReader;

public final class FObjs {

    public static List<String> readLines(FileObject fileObject, Charset charset) {
        final List<String> lines = new ArrayList<>();
        
        try ( LineReader r = new LineReader(new InputStreamReader(fileObject.getInputStream(), charset)) ) {
            for (;;) {
                String line = r.readLine();
                if (line == null) break;
                lines.add(line);
            }
        }
        catch (IOException  e) {
            throw new XException(e);
        }
        
        return lines;
    }

}
