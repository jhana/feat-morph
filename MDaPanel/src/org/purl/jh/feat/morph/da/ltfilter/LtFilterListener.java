package org.purl.jh.feat.morph.da.ltfilter;

import java.util.EventListener;
import java.util.regex.Pattern;

public interface LtFilterListener extends EventListener {
    void changed(Pattern lemmaPattern, Pattern tagPattern);
}
