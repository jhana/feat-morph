package org.purl.jh.feat.morph.da.ltfilter;

import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;
import javax.swing.event.EventListenerList;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.util.str.Strings;

// todo make the set of filters configurable
// todo handle tags of various lengths (currently it does nto crash but does not filter well)
public class FilterPanel extends JPanel {
    private LTx ltx;

    // this should be in a controller (as a single model could potentially have multiple gui)
    private final EventListenerList changeListeners = new EventListenerList();
    
    private int maxFilteredSlot = 0;
    private Pattern lemmaPattern;
    private Pattern tagPattern;

    private final JComboBox lemmaCombo = new JComboBox();
    private final List<SlotPanel> slotPanels = new ArrayList<>();
    
    public FilterPanel() {
        this.setLayout(new GroupLayout(this));
        
        lemmaCombo.addActionListener(e -> updated());
    }

    public void setLtx(LTx ltx) {
        if (this.ltx == ltx) {
            // todo refresh in case the tags changed, but keep selection
        }
        else {
            this.ltx = ltx;
            initComponents();
        }
    }
    
    
    final List<JLabel> labels  = new ArrayList<>();
    final List<Component> filters = new ArrayList<>();

    private void initComponents() {
        removeAll();
        labels.clear();
        filters.clear();
        lemmaCombo.removeAllItems();
        slotPanels.clear();
        
        
        labels.add(new JLabel("Lemma:"));
        Set<String> lemmas = ltx.getLemmas();
        if (lemmas.size() == 1) {
            filters.add(new JLabel(lemmas.iterator().next()));
        }
        else {
            lemmaCombo.addItem("(All)"); 
            for (String lemma : ltx.getLemmas()) {
                lemmaCombo.addItem(lemma); 
            }
            filters.add(lemmaCombo);
        }
        addSlotPanel("POS:", ltx, 0);
        addSlotPanel("Gender:", ltx, 2);
        addSlotPanel("Nr:", ltx, 3);

        layoutControls();        
        revalidate();
        
        for (SlotPanel panel : slotPanels) {
            panel.selectAll();
        }
        if (lemmaCombo.getItemCount() > 0) lemmaCombo.setSelectedIndex(0);
    }

    private void layoutControls() {
        final GroupLayout layout = (GroupLayout) getLayout();
        
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(labels.get(0))
                        .addComponent(labels.get(1))
                        .addComponent(labels.get(2))
                        .addComponent(labels.get(3))
                )
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(filters.get(0))
                        .addComponent(filters.get(1))
                        .addComponent(filters.get(2))
                        .addComponent(filters.get(3))
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(labels.get(0))
                                .addComponent(filters.get(0))
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(labels.get(1))
                                .addComponent(filters.get(1))
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(labels.get(2))
                                .addComponent(filters.get(2))
                        )
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(labels.get(3))
                                .addComponent(filters.get(3))
                        )
        );
    }

    private void addSlotPanel(String label, LTx ltx, int i) {
        if (maxFilteredSlot < i) maxFilteredSlot = i;
        
        labels.add(new JLabel(label));
        SlotPanel panel = new SlotPanel(ltx, i);
        filters.add(panel);
        slotPanels.add(panel);
    }

    
    private void updated() {
        updateModel();
        fireChangePerformed();
    }
    
    private void updateModel() {
        if (lemmaCombo.getItemCount() == 0 || lemmaCombo.getSelectedIndex() == 0) {
            lemmaPattern = Pattern.compile(".*");
        }
        else {
            String lemma = (String)lemmaCombo.getSelectedItem();
            lemmaPattern = Pattern.compile("\\Q" + lemma + "\\E");
        }

        StringBuilder tagPatternStr = new StringBuilder(Strings.repeatChar('.', maxFilteredSlot+1));
        for (SlotPanel panel : slotPanels) {
            tagPatternStr.setCharAt(panel.slotIdx, panel.getVal());
        }
        
        // replaces trailing '.' by ".*" to handle shorter tags
        tagPattern = Pattern.compile( StringUtils.stripEnd(tagPatternStr.toString(), ".") + ".*" );
    }

    
    
    private class SlotPanel extends JPanel {
        @Getter private final int slotIdx;
        
        private final List<JToggleButton> buttons = new ArrayList<>();
        private final ButtonGroup group = new ButtonGroup();
        
        private SlotPanel(LTx ltx, int slotIdx) {
            FlowLayout layout = new FlowLayout(FlowLayout.LEFT, 0, 0);
            setBorder(new EmptyBorder(0, 0, 0, 0));
            setLayout(layout);
            
            
            this.slotIdx = slotIdx;
            
            final SortedSet<String> vals =vals(ltx, slotIdx);
            if (vals.size() == 1) {
                add(new JLabel(vals.iterator().next()));
            }
            else {
                addButton("All");

                for (String val : vals) {
                    addButton("" + val);
                }
            }
        }

        public  void selectAll() {
            if (!buttons.isEmpty()) {
                buttons.get(0).setSelected(true);
            }
        }
        
        /** Collects the values of a particular tag slot. Handling tags of shorter length */
        private SortedSet<String> vals(LTx ltx, int slotIdx) {
            final SortedSet<String> vals = new TreeSet<>();
            for (Lt lt : ltx.col()) {
                String val = lt.getTag().length() <= slotIdx ? "." : String.valueOf(lt.getTag().charAt(slotIdx));
                vals.add(val);      // todo check overflow
            }
            return vals;
        }
        
        private void addButton(String label) {
            JToggleButton button = new JToggleButton(label);
            buttons.add(button);
            add(button);
            group.add(button);

            button.addActionListener(e -> updated());
        }
        
        public char getVal() {
            if (buttons.isEmpty() || buttons.get(0).isSelected()) {
                return '.';
            }
            else {
                for (JToggleButton button : buttons) {
                    if (button.isSelected()) return button.getText().charAt(0);
                }
                return '.';
            }
        }
        
    }
    
    public void addChangeListener(LtFilterListener l) {
        changeListeners.add(LtFilterListener.class, l);
    }

    public void removeChangeListener(LtFilterListener l) {
        changeListeners.remove(LtFilterListener.class, l);
    }
    
    protected void fireChangePerformed() {
        final Object[] listeners = changeListeners.getListenerList();

        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == LtFilterListener.class) {
                ((LtFilterListener)listeners[i+1]).changed(lemmaPattern, tagPattern);
            }
        }
    }
    
}
