package org.purl.jh.feat.morph.da;

import com.google.common.base.Preconditions;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.regex.Pattern;
import javax.swing.JList;
import lombok.Setter;
import org.purl.jh.law.data.event.MLayerEvent;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.law.nbdata.view.Current;
import org.purl.jh.pml.event.DataListener;
import org.purl.jh.util.col.pred.AbstractFilter;
import org.purl.jh.util.col.pred.Filter;
import org.purl.jh.util.gui.MultilineTooltip;
import org.purl.jh.util.gui.list.CheckList;
import org.purl.jh.util.gui.list.FilterListModel;
import org.purl.jh.util.gui.list.FocusCellRenderer;

/**
 * List displaying an LTs as a list of lt's. The lt's can be selected, deselected, added, edited.
 * The currently displayed lts is set by calling {@link #setLts(LTs)}.
 * 
 * @todo listen to modification
 */
public class LtList extends CheckList<Lt,Filter<Lt>>  {
    /** The Layer containing these lts. Modification messages are sent to it  */
    @Setter protected MLayer layer;

    protected DataListener dataListener;
    
    /** Creates a new instance of LTList */
    public LtList() {
        super(new TModel());
        setFocusable(true);
        addChecks();
    }
    
    public void init(MLayer layer, DataListener dataListener) {
        this.dataListener = dataListener;
        this.layer = layer;
        hideLemmas(false);
        notifyMe(true);
    }

    @Override
    public boolean requestFocusInWindow() {
        if (super.getCurIdx() == -1) {
            setCur(0);
        }
        return super.requestFocusInWindow();
    }    
    
    
    /**
     * An Ltxs to be displayed. It is assumed they are homogenous
     * @param aLts 
     */
    public void setCurrent(Current current) {
        if (current.isEmpty()) {
            setModel(current, null);
        }
        else if (current.isSingleton()) {
            setModel(current, current.single());
        }
        else if (current.isHomogenous()) {
            setModel(current, current.one());  // todo some abstraction, selection must set all of them
        }
        else {
            setModel(current, null);
        }
    }
        
    private void setModel(Current current, LTx ltx) {
        // todo update only if items change. equals on ltx???
        if (getModel() != null && getModel().sampleLtx == ltx) {
            getModel().refresh();
        }
        else {
            setModel(ltx == null ? new TModel() : new TModel(current, ltx));
        }
    }
    
    public void hideLemmas(boolean aHideLemmas) {
        setCellRenderer( aHideLemmas ? new TRenderer() : new LTRenderer());
        addChecks();
    }

    public  void filter(final Pattern lemmaPattern, final Pattern tagPattern) {
        getModel().setFilter(new AbstractFilter<Lt>() {
            @Override
            public boolean isOk(final Lt aVal) {
                return lemmaPattern.matcher(aVal.getLemma()).matches() && tagPattern.matcher(aVal.getTag()).matches();
            }
        });
        getModel().refresh();
    }
    
    
//    public void limitToLemma(String aLemma) {
////            if (! (getModel().getFilter() instanceof LemmaFilter) ) {
////                getModel().setFilter(mLemmaFilter);
////            }
//        mLemmaFilter.setDisabled(aLemma == null);
//        mLemmaFilter.setLemma(aLemma);
//    }

    //todo     
//    public void setHelper(ItemHelper aHelper) {
//        mItemHelper = aHelper;
//        ToolTipManager.sharedInstance().registerComponent(this);
//    }
    

    @Override public TModel getModel() {
        return (TModel) super.getModel();
    }
    
// -----------------------------------------------------------------------------
// Functions
// -----------------------------------------------------------------------------
    
    /**
     * Invoked tooltip;
     */
    public void cmdHelp() {
        super.showToolTip();
    }
    
// -----------------------------------------------------------------------------
// 
// -----------------------------------------------------------------------------
    
    /**
     * Automatic tooltip.
     */
    @Override public MultilineTooltip createToolTip() {
        return new MultilineTooltip(this);
    }    

    @Override 
    public String getToolTipText(MouseEvent evt) {
        return "todo";
        
//        if (mItemHelper == null) return "";
//
//        int idx = locationToIndex(evt.getPoint());  // Get item index
//        if (idx == -1) return "";
//        
//        return  mItemHelper.fullHelp(getModel().getElementAt(idx));
    }

    @Override protected void setCurToolTipText() {
//todo 
//        String helpStr = (mItemHelper == null) ? "No help available" : mItemHelper.fullHelp( getCurItem() );
//        putClientProperty(TOOL_TIP_TEXT_KEY, helpStr);
    }
    

    
    
    /**
     */
    @Override
    public boolean isSelected(int aIdx) {
        return getModel().getElementAt(aIdx).isSelected();
    }

    /**
     * Asks the layer to set selection of the item with the specified index.
     *
     * Asks data to change selection of the item on the specified index.
     * @param aIdx does nothing if negative (@todo remove??)
     * @param aSelection the new selection of the item
     */
    @Override
    public void setSelection(int aIdx, boolean aSelection) {
        if (aIdx < 0) return; 

        Lt lt = getModel().getElementAt(aIdx);
        getModel().current.ltSelect(lt, aSelection);
        getModel().current.setAutoStatus();
    }
    
    protected void onToggle(int aIdx) {
        Preconditions.checkArgument(aIdx >= 0);
        toggleSelection(aIdx);
    }
    
    protected void onSelectOnly(int aIdx) {
        Preconditions.checkArgument(aIdx >= 0);
        repaint(getCellBounds(0, getModel().getSize()-1));

        Lt lt = getModel().getElementAt(aIdx);
        getModel().current.ltSelectOnly(lt);
        getModel().current.setAutoStatus();

        //setSelection(aIdx, !isSelected(aIdx));
        setCur(aIdx);  // return visual selection 
    }

    @Override
    protected void onClick(boolean shiftDown, boolean controlDown, boolean altDown, boolean altGraphDown, boolean metaDown) {
        if (getCurIdx() == -1) return;
        
        if (controlDown) {
            onToggle(getCurIdx());
        }
        else {
            onSelectOnly(getCurIdx());
        }
    }

    @Override
    protected void onSpace(boolean shiftDown, boolean controlDown, boolean altDown, boolean altGraphDown, boolean metaDown) {
        if (getCurIdx() == -1) return;
        
        if (controlDown) {
            onToggle(getCurIdx());
        }
        else {
            onSelectOnly(getCurIdx());
        }
    }
    
// -----------------------------------------------------------------------------
// Notifications
// -----------------------------------------------------------------------------
 
    /**
     * Updates selection (retaining cursor position).
     *
     * @param aE selection event; null means unconditional complete update
     */
    public void selectionChanged(MLayerEvent aE) {
        int idx = getCurIdx();              // remember cursor position
        getModel().updateSelection(aE);
        if (idx != getCurIdx() && idx != -1) setSelectedIndex(idx);  // restore cursor position
    }

    public void updateContent(MLayerEvent aE) {
        int idx = getCurIdx();                  // remember cursor position
        getModel().refresh();
        if (idx != getCurIdx() && idx != -1) setSelectedIndex(idx);  // restore cursor position
    }

    
// =============================================================================
//
// =============================================================================    

    static class TModel extends FilterListModel<Lt,Filter<Lt>> {
        Current current;
        LTx sampleLtx;
        
        TModel() {
            super(Collections.<Lt>emptyList());
        }
        
        TModel(Current current, LTx sampleLtx) {
            super(sampleLtx.col());
            this.sampleLtx = sampleLtx;
            this.current = current;
        }
        
        /**
         * Response to the list, which reacts in turn to the layer 
         */
        public void updateSelection(MLayerEvent aE) {
//todo
//            if ( aE != null && aE.getAffectedType().isElement() ) {
//                int idx = mFilteredItems.indexOf(aE.getAffectedElement());
//                if (idx != -1) fireContentsChanged(this, idx, idx);
//            }
//            else {
//                fireContentsChanged(this, 0, getSize());    
//            }

        }     
        
    }

    static abstract class BaseRenderer extends FocusCellRenderer {
        @Override
        protected void setTextEtc(JList aList, Object aValue) {
            Lt lt = (Lt)aValue;
          
            String fullString = getLtStr(lt);
// todo            
//            ItemHelper helper = ((LtList)aList).mItemHelper;
//            if (helper instanceof LtHelper) {
//                fullString += "  " + ((LtHelper)helper).getTagHelper().help(lt);
//            }

            if (lt.getComment() != null && !lt.getComment().isEmpty()) {
                fullString += "  // " + lt.getComment();
            }
            
            setText(fullString);
        }
        
        protected abstract String getLtStr(Lt aLt);

    }
    
    static class LTRenderer extends BaseRenderer {
        @Override
        protected String getLtStr(Lt aLt) {
            return aLt.getLemma() + " : " + aLt.getTag();
        }
    }

    static class TRenderer extends BaseRenderer {
        @Override
        protected String getLtStr(Lt aLt) {
            return aLt.getTag();
        }
    }
    
}
