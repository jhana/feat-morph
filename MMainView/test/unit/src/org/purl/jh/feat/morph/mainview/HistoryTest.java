package org.purl.jh.feat.morph.mainview;

import com.google.common.collect.ImmutableList;
import static com.google.common.collect.ImmutableList.of;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author j
 */
public class HistoryTest {
    
    public HistoryTest() {
    }

    @Test
    public void testGoTo() {
    }

    @Test
    public void testRecord() {
        HistoryImpl hist = new HistoryImpl();
        hist.check(E, null, E);

        hist.record("a");
        hist.check(E, "a", E);
        
        hist.record("b");
        hist.check(of("a"), "b", E);
        
        hist.record("c");
        hist.check(of("a", "b"), "c", E);
        
        hist.go(-1);
        assertEquals("b", hist.x);
        hist.check(of("a"), "b", of("c"));
        
        hist.go(-1);
        assertEquals("a", hist.x);
        hist.check(E, "a", of("b", "c"));

        hist.go(+2);
        assertEquals("c", hist.x);
        hist.check(of("a", "b"), "c", E);

        hist.go(-2);
        assertEquals("a", hist.x);
        hist.check(E, "a", of("b", "c"));
        
        hist.record("o");
        hist.check(of("a"), "o", E);
    }
    
    private static ImmutableList<String> E = ImmutableList.<String>of();

    private  class HistoryImpl extends History<String> {
        public String x;
        
        protected void goTo(String item) {
            x = item;
        }

        public void check(ImmutableList<String> back, String cur, ImmutableList<String> forward) {
            assertEquals(back.reverse(), this.backItems);
            assertEquals(cur, this.cur);
            assertEquals(forward, this.forwardItems);
        }
    }
    
}
