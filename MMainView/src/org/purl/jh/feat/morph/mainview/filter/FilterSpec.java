package org.purl.jh.feat.morph.mainview.filter;

import lombok.Data;

@Data
public class FilterSpec {
    public static enum Mode {
        EXACT, PREFIX, CONTAINS, REGEX;
    }
    
    String curTag;
    String prevWord;
    String prevTag;
    String nextWord;
    String nextTag;
    String curWord;
    
    Mode mode = Mode.EXACT;
    boolean ignoreDia; 
    
    boolean ambi;

    // or-ed values of status
    boolean status_todo;
    boolean status_review;
}
