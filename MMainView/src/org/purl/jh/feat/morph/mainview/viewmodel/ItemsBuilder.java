package org.purl.jh.feat.morph.mainview.viewmodel;

import java.util.ArrayList;
import java.util.List;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.MLayer;

/**
 *
 * @author Jirka
 */
public class ItemsBuilder {
    public static List<Item> build(MLayer layer, int leftWindow, int rightWindow) {
        final List<LTx> lts = layer.getFlatLts();
        final WordContext ctx = new WordContext(layer, leftWindow, rightWindow);
        final int n = lts.size();
        final List<Item> items = new ArrayList<>(n);
        
        for (int i = 0; i < n; i++) {
            String prevX = ctx.getBeforeZB(lts.get(i));
            LTx prev = i == 0 ? null : lts.get(i-1);
            LTx cur = lts.get(i);
            LTx next = i == n - 1 ? null : lts.get(i+1);
            String nextX = ctx.getAfterBZ(lts.get(i));
            
            items.add(new Item(prevX, prev, cur, next, nextX));
        }
        
        return items;
    }
}
