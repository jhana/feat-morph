package org.purl.jh.feat.morph.mainview;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Util {
    public static boolean checkRegex(String regexStr) {
        try {
            Pattern.compile(regexStr);
            return true;
        }
        catch (PatternSyntaxException e) {
            return false;
        }
    }

    /** Converts a list of items to a list of indexes of those items within a list */
    public static <T> List<Integer> items2idxs(List<T> list, List<T> subset) {
        // empty subset
        if (subset.isEmpty()) return ImmutableList.<Integer>of();
        
        // for efficiency reasons handle singe item subset
        if (subset.size() == 1) {
            return ImmutableList.<Integer>of(list.indexOf(subset.get(0)));
        }
        
        // subset wit two and more items
        final List<Integer> idxs = new ArrayList<>(subset.size()); 
        for (int i = 0; i < list.size(); i++) {
            T item = list.get(i);
            if (subset.contains(item)) idxs.add(i);
        }
        return idxs;
    }

    /** Converts a list of items to a list of indexes of those items within a list */
    public static <A,B> List<Integer> items2idxs(List<A> list, Function<A,B> fnc, List<B> subset) {
        // empty subset
        if (subset.isEmpty()) return ImmutableList.<Integer>of();
        
        // for efficiency reasons handle singe item subset
        if (subset.size() == 1) {
            return ImmutableList.<Integer>of(indexOf(list,fnc,subset.get(0)));
        }
        
        // subset with two and more items
        final List<Integer> idxs = new ArrayList<>(subset.size()); 
        for (int i = 0; i < list.size(); i++) {
            B item = fnc.apply( list.get(i) );
            if (subset.contains(item)) idxs.add(i);
        }
        return idxs;
    }

    /** Finds the index of the first item satisfying a predicate */
    public static <A,B> int indexOf(List<A> list, Function<A,B> fnc, B item) {
        for (int i = 0; i < list.size(); i++) {
            B x = fnc.apply( list.get(i) );
            if (x.equals(item)) return i;
        }
        return -1;
    }
}
