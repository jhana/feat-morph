package org.purl.jh.feat.morph.mainview.viewmodel;

import lombok.Value;
import org.purl.jh.law.data.m.LTx;

/**
 * Item representing one row in the ItemList, i.e. a form and its context.
 */
@Value
public class Item {
    String prevX;
    LTx prev;
    LTx cur;
    LTx next;
    String nextX;
}
