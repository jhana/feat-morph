package org.purl.jh.feat.morph.mainview;

import java.awt.BorderLayout;
import org.openide.loaders.DataObject;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.CloneableTopComponent;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.nbdata.MLayerDataObject;
import org.purl.jh.law.nbdata.view.MLayerView;

public final class TopComponent extends CloneableTopComponent {

    @ServiceProvider(service=MLayerView.class)
    public static class MLayerViewSupport implements MLayerView {
        @Override
        public CloneableTopComponent getTopComponent(DataObject aDObj) {
            return (aDObj == null || !(aDObj instanceof MLayerDataObject)) ?
                null : new TopComponent( (MLayerDataObject)aDObj );
        }
    }
    private final MLayerDataObject dobj;

    private final PanelSimple panel = new PanelSimple();
    private final InstanceContent lookupContent = new InstanceContent();    

    public TopComponent() {
        initComponents();
        this.dobj = null;

        associateLookup(new AbstractLookup(lookupContent));
    }
    
    public TopComponent(MLayerDataObject dobj) {
        initComponents();
        this.dobj = dobj;
        MLayer layer = dobj.getData();
        
        associateLookup(new ProxyLookup(dobj.getLookup(), new AbstractLookup(lookupContent)));
        lookupContent.add(dobj);
                
        panel.init(layer, getLookup(), lookupContent);
    }

    @Override
    public boolean requestFocusInWindow() {
        return panel.requestFocusInWindow();
    }    


    @Override
    public void componentOpened() {
        super.componentOpened();
    }

    @Override
    public void componentClosed() {
        super.componentClosed();
        panel.saveProperties();
    }
    
    @Override
    public int getPersistenceType() {
        return PERSISTENCE_NEVER;
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

}
