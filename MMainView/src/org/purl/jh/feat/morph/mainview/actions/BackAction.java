package org.purl.jh.feat.morph.mainview.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.purl.jh.feat.morph.mainview.HistoryBackProvider;

@ActionID(category = "Navigate", id = "org.purl.jh.feat.morph.mainview.BackAction")
@ActionRegistration( 
        iconBase = "org/purl/jh/feat/morph/mainview/actions/go-back.png", 
        displayName = "#CTL_BackAction") 
@ActionReferences({
    @ActionReference(path = "Menu/Navigate", position = 10),
    @ActionReference(path = "Toolbars/Navigate", position = 10),
    @ActionReference(path = "Shortcuts", name = "A-Left")
})
@NbBundle.Messages("CTL_BackAction=Go back")
public class BackAction implements ActionListener { 
    private final List<HistoryBackProvider> context; 

    public BackAction(List<HistoryBackProvider> context) { 
        this.context = context; 
    } 

    @Override
    public void actionPerformed(ActionEvent event) { 
        HistoryBackProvider hist = context.iterator().next();
        
        if (!hist.getBackItems().isEmpty()) hist.go(-1);
    } 
}
