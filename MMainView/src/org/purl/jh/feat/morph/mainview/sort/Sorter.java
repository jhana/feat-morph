package org.purl.jh.feat.morph.mainview.sort;

import java.text.Collator;
import java.util.Comparator;
import org.purl.jh.feat.morph.mainview.viewmodel.Item;
import org.purl.jh.law.data.m.LTx;

/**
 * A class providing comparators based on the sorting specification.
 */
public class Sorter {
    
    private static final Collator COLLATOR = Collator.getInstance();

    /** Compares two ltx structure, considering nulls */
    private static int compare(LTx o1, LTx o2) {
        if (o1 == o2) return 0;
        if (o1 == null) return 1;
        if (o2 == null) return -1;
        return COLLATOR.compare(
                o1.getForm(), 
                o2.getForm()
        );
    }
    
    /** Compares two ltx structure, considering nulls */
    private static int retrogradeCompare(LTx o1, LTx o2) {
        if (o1 == o2) return 0;
        if (o1 == null) return 1;
        if (o2 == null) return -1;
        return COLLATOR.compare(
                new StringBuilder(o1.getForm()).reverse().toString(), 
                new StringBuilder(o2.getForm()).reverse().toString()
        );
    }
    
    
    /** Comparison by the word preceding the current word */
    private final Comparator<Item> beforeComparator = (o1,o2) -> compare(o1.getPrev(),o2.getPrev());
    /** Comparison by the current word  */
    private final Comparator<Item> wordComparator   = (o1,o2) -> compare(o1.getCur(),o2.getCur());
    /** Comparison by the word following the current word */
    private final Comparator<Item> afterComparator  = (o1,o2) -> compare(o1.getNext(),o2.getNext());

    /** Comparison by the word preceding the current word */
    private final Comparator<Item> beforeRetrogradeComparator = (o1,o2) -> retrogradeCompare(o1.getPrev(),o2.getPrev());
    /** Comparison by the current word  */
    private final Comparator<Item> wordRetrogradeComparator   = (o1,o2) -> retrogradeCompare(o1.getCur(),o2.getCur());
    /** Comparison by the word following the current word */
    private final Comparator<Item> afterRetrogradeComparator  = (o1,o2) -> retrogradeCompare(o1.getNext(),o2.getNext());
    
    /** 
     * Returns a comparator based on sorting specification 
     * The comparator is null if there should be no sort.
     */
    public Comparator<Item> comparator(SortSpec spec) {
        if (spec.getFirst() == SortSpec.What.UNSORTED) return null;

        Comparator<Item>  first = comparators(spec.getFirst(), spec.isFirstRetrogade());
        if (first == null) return null;
        if (spec.isFirstReversed()) first = first.reversed();
        
        Comparator<Item> second = comparators(spec.getSecond(), spec.isSecondRetrogade());
        if (second == null) return first;
        if (spec.isSecondReversed()) second = second.reversed();
 
        return first.thenComparing(second);
    }

    private Comparator<Item> comparators(SortSpec.What what, boolean retroGrade) {
        switch (what) {
            case BEFORE: return retroGrade ? beforeRetrogradeComparator : beforeComparator;
            case WORD:   return retroGrade ? wordRetrogradeComparator : wordComparator;
            case AFTER:  return retroGrade ? afterRetrogradeComparator : afterComparator;
            default: return null;
        }
    }
}
