package org.purl.jh.feat.morph.mainview.actions;

import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.purl.jh.feat.morph.mainview.Navigable;

@ActionID(category = "Navigate", id = "org.purl.jh.feat.morph.mainview.GotoNextAction")
@ActionRegistration(displayName = "#CTL_GotoNextAction", lazy = false)
@ActionReference(path = "Menu/Navigate", position = 110)
@NbBundle.Messages("CTL_GotoNextAction=Goto Next Form")
public class GotoNextAction extends NavigateAction {
 
    public GotoNextAction() {
        this(null);
    }
    
    protected GotoNextAction(Lookup context) {
        super(context);
        putValue(Action.NAME, NbBundle.getMessage(GotoNextAction.class, "CTL_GotoNextAction")); // FIXME: not sure why it does not use annotations
    }

    @Override
    public Action createContextAwareInstance(Lookup context) {
        return new GotoNextAction(context);
    }
 
    @Override
    protected void navigate(Navigable navigable) {
        navigable.move(1);
    }
}