package org.purl.jh.feat.morph.mainview.viewmodel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TblModel extends AbstractTableModel {
    private final List<Item> items;
    
    @Override
    public int getRowCount() {
        return items.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int colIdx) {
        switch (colIdx) {
            case 0: return "Page";
            case 1: return "Previous context";
            case 2: return "Previous";
            case 3: return "Word"; 
            case 4: return "Following";
            case 5: return "Following context";
            default: return null;
        }        
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return String.class;
    }
    

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        final Item item = items.get(rowIndex);
        switch(columnIndex) {
            case 0: return item.getCur().getPage(); 
            case 1: return item.getPrevX(); // context.getBeforeZB(lts);
            case 2: return item.getPrev() == null ? "" : LTxPrinter.form(item.getPrev());
            case 3: return LTxPrinter.form(item.getCur());
            case 4: return item.getNext() == null ? "" : LTxPrinter.form(item.getNext());
            case 5: return item.getNextX();
            default: return "???";
        }
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
}
