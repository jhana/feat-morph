package org.purl.jh.feat.morph.mainview.filter;

import com.google.common.base.Objects;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JToggleButton;
import javax.swing.event.EventListenerList;
import lombok.Getter;
import org.purl.jh.util.err.FormatError;
import org.purl.jh.util.gui.SimpleDocListener;

/**
 * This composite filter box for a quick and easy filtering list of items:
 */
public class QuickFilterBox extends javax.swing.JToolBar  {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(QuickFilterBox.class);
    private final static java.util.ResourceBundle bundle = org.openide.util.NbBundle.getBundle(QuickFilterBox.class);

    
    @Getter private javax.swing.JTextField filterTextBox;
    private final Color normalFilterBoxColor;
        
    private javax.swing.ButtonGroup modeGroup;
    private javax.swing.JToggleButton containsModeButton;
    private javax.swing.JToggleButton exactModeButton;
    private javax.swing.JToggleButton prefixModeButton;
    private javax.swing.JToggleButton contFilterToggle;
    @Getter private javax.swing.JToggleButton regexModeButton;

    @Getter private javax.swing.JToggleButton ignoreDiaButton;
    
    @Getter final private FilterModel model;

    // this should be in a controller (as a single model could potentially have multiple gui)
    protected final EventListenerList listenerList = new EventListenerList();
    
    public QuickFilterBox() {
        model = new FilterModel();

        initComponents();
        normalFilterBoxColor = filterTextBox.getForeground();
        setRollover(true);
        setBorderPainted(false);
        
        ModelListener modelListener = new ModelListener();
        model.addPropertyChangeListener(modelListener);

        filterTextBox.addActionListener(e -> filterRequested());        
        
        loadAll();
    }

    /** Use requests application of the filter (hits enter, or in continuous mode) */
    private void filterRequested() {
        if (textIsValid()) fireActionPerformed();
    }
    
    /**
     * Adds an <code>ActionListener</code> to the button.
     * @param l the <code>ActionListener</code> to be added
     */
    public void addActionListener(ActionListener l) {
        listenerList.add(ActionListener.class, l);
    }

    /**
     * Removes an <code>ActionListener</code> from the button.
     * If the listener is the currently set <code>Action</code>
     * for the button, then the <code>Action</code>
     * is set to <code>null</code>.
     *
     * @param l the listener to be removed
     */
    public void removeActionListener(ActionListener l) {
        listenerList.remove(ActionListener.class, l);
    }
    
    protected void fireActionPerformed() {
        final Object[] listeners = listenerList.getListenerList();
        ActionEvent e = null;

        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == ActionListener.class) {
                // Lazily create the event:
                if (e == null) {
                    e = new ActionEvent(QuickFilterBox.this, ActionEvent.ACTION_PERFORMED, "");
                }
                ((ActionListener)listeners[i+1]).actionPerformed(e);
            }
        }
    }
    
    /** Responds to changes in model */
    private class ModelListener extends PropertyChangeListenerAdapter {
        @Override
        protected void textChanged(String newValue, String oldValue) {
            loadText();
            actionNotify();
        }

        @Override
        protected void modeChanged(FilterSpec.Mode newValue, FilterSpec.Mode oldValue) {
            loadMode();
            actionNotify();
        }

        @Override
        protected void ignoreDiaChanged(boolean newValue, boolean oldValue) {
            loadIgnoreDia();
            actionNotify();
        }

        @Override
        protected void continuousChanged(boolean newValue, boolean oldValue) {
            loadContinuous();
            actionNotify();
        }
        
        private void actionNotify() {
            if (model.isContinuous()) {
                filterRequested();
            }
        }
    }

    
    /** Loads filter specification from the model and sets the view appropriately */
    private void loadAll() {
        loadText();
        loadMode();
        loadIgnoreDia();
        loadContinuous();
    }

    private boolean textIsValid() {
        return model.textIsValid();
    }
    
    /** Loads text from the model */
    protected void loadText() {
        indicateError(!textIsValid());
                
        if ( Objects.equal(model.getText(), filterTextBox.getText())) return;

        filterTextBox.setText(model.getText());
    }

    private void indicateError(boolean error) {
        if (error) {
            filterTextBox.setForeground(Color.RED);
        }
        else {
            filterTextBox.setForeground(normalFilterBoxColor);  
        }                    
    }
    
    protected void loadMode() {
        switch (model.getSpec().getMode()) {
            case EXACT:     setSelected(exactModeButton, true); break;
            case CONTAINS:  setSelected(containsModeButton, true); break;
            case PREFIX:    setSelected(prefixModeButton, true); break;
            case REGEX:     setSelected(regexModeButton, true); break;
            default: 
        }
    }

    protected void loadIgnoreDia() {
        setSelected(ignoreDiaButton, model.getSpec().isIgnoreDia());
    }

    protected void loadContinuous() {
        setSelected(contFilterToggle, model.isContinuous());
    }
    
    private static void setSelected(JToggleButton button, boolean value) {
        if (button.isSelected() == value) return;
        button.setSelected(value);
    }
    
    
    
    private void initComponents() {
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        //setLayout(new FlowLayout(FlowLayout.LEADING));
        //setBorder(null);

        filterTextBox = new javax.swing.JTextField();

        modeGroup = new javax.swing.ButtonGroup();
        containsModeButton = new javax.swing.JToggleButton();
        exactModeButton = new javax.swing.JToggleButton();
        prefixModeButton = new javax.swing.JToggleButton();
        regexModeButton = new javax.swing.JToggleButton();

        ignoreDiaButton = new javax.swing.JToggleButton();

        contFilterToggle = new javax.swing.JToggleButton();
        
        this.add(filterTextBox);
        filterTextBox.getDocument().addDocumentListener( new SimpleDocListener() {
            @Override
            public void textUpdated() {
                try {
                    model.setText(filterTextBox.getText());
                    indicateError(false);
                }
                catch(FormatError e) {
                    indicateError(true);
                }
            }
        });                
        
        modeGroup.add(exactModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(exactModeButton, bundle.getString("QuickFilterBox.exactModeButton.text")); // NOI18N
        exactModeButton.setToolTipText(bundle.getString("QuickFilterBox.exactModeButton.toolTipText")); // NOI18N
        exactModeButton.setFocusable(false);
        exactModeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        exactModeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        exactModeButton.addActionListener(evt -> model.setMode(FilterSpec.Mode.EXACT));
        this.add(exactModeButton);

        modeGroup.add(prefixModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(prefixModeButton, bundle.getString("QuickFilterBox.prefixModeButton.text")); // NOI18N
        prefixModeButton.setToolTipText(bundle.getString("QuickFilterBox.prefixModeButton.toolTipText")); // NOI18N
        prefixModeButton.addActionListener(evt -> model.setMode(FilterSpec.Mode.PREFIX));
        this.add(prefixModeButton);

        modeGroup.add(containsModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(containsModeButton, bundle.getString("QuickFilterBox.containsModeButton.text")); // NOI18N
        containsModeButton.setToolTipText(bundle.getString("QuickFilterBox.containsModeButton.toolTipText")); // NOI18N
        containsModeButton.addActionListener(evt -> model.setMode(FilterSpec.Mode.CONTAINS));
        this.add(containsModeButton);

        modeGroup.add(regexModeButton);
        org.openide.awt.Mnemonics.setLocalizedText(regexModeButton, bundle.getString("QuickFilterBox.regexModeButton.text")); // NOI18N
        regexModeButton.setToolTipText(bundle.getString("QuickFilterBox.regexModeButton.toolTipText")); // NOI18N
        regexModeButton.addActionListener(evt -> model.setMode(FilterSpec.Mode.REGEX));
        this.add(regexModeButton);

        org.openide.awt.Mnemonics.setLocalizedText(ignoreDiaButton, bundle.getString("QuickFilterBox.ignoreDiaButton.text")); // NOI18N
        ignoreDiaButton.setToolTipText(bundle.getString("QuickFilterBox.ignoreDiaButton.toolTipText")); // NOI18N
        ignoreDiaButton.setFocusable(false);
        ignoreDiaButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ignoreDiaButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ignoreDiaButton.addActionListener(evt -> model.setIgnoreDia(ignoreDiaButton.isSelected()));
        this.add(ignoreDiaButton);

        org.openide.awt.Mnemonics.setLocalizedText(contFilterToggle, bundle.getString("QuickFilterBox.contFilterToggle.text")); // NOI18N
        contFilterToggle.setToolTipText(bundle.getString("QuickFilterBox.contFilterToggle.toolTipText")); // NOI18N
        contFilterToggle.setFocusable(false);
        contFilterToggle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        contFilterToggle.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        contFilterToggle.addActionListener(evt -> model.setContinuous(contFilterToggle.isSelected()));
        this.add(contFilterToggle);
    }
}
 