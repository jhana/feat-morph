package org.purl.jh.feat.morph.mainview.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.purl.jh.feat.morph.mainview.HistoryForwardProvider;

@ActionID(category = "Navigate", id = "org.purl.jh.feat.morph.mainview.ForwardAction")
@ActionRegistration( 
        iconBase = "org/purl/jh/feat/morph/mainview/actions/go-forward.png", 
        displayName = "#CTL_ForwardAction") 
@ActionReferences({
    @ActionReference(path = "Menu/Navigate", position = 20),
    @ActionReference(path = "Toolbars/Navigate", position = 20),
    @ActionReference(path = "Shortcuts", name = "A-Right")
})
@NbBundle.Messages("CTL_ForwardAction=Go forward")
public class ForwardAction implements ActionListener { 
    private final List<HistoryForwardProvider> context; 

    public ForwardAction(List<HistoryForwardProvider> context) { 
        this.context = context; 
    } 

    @Override
    public void actionPerformed(ActionEvent event) { 
        HistoryForwardProvider hist = context.iterator().next();
        
        if (!hist.getForwardItems().isEmpty()) hist.go(1);
    } 
}
