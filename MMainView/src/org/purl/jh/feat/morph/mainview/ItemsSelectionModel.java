package org.purl.jh.feat.morph.mainview;

import com.google.common.base.Preconditions;
import java.util.List;
import static javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
import org.purl.jh.util.col.Cols;

public class ItemsSelectionModel extends XDefaultListSelectionModel {
    public void setSelection(List<Integer> idxs, int leadIndex) {
        Preconditions.checkState(getSelectionMode() == MULTIPLE_INTERVAL_SELECTION);
        // todo updateLeadAnchorIndices(index0, index1);
        
        value.clear();
        
        if (idxs.isEmpty()) {
            minIndex = MAX;
            maxIndex = MIN;
        }
        else {
            minIndex = idxs.get(0);
            maxIndex = Cols.last(idxs);
        }
        
        for (int i : idxs) {
            value.set(i);
        }
        
        this.leadIndex = leadIndex;     // check it is within idxs?
        
        fireValueChanged(MIN, MAX);     //todo min old/cur min, max old/cur max
    }
    
    
}
