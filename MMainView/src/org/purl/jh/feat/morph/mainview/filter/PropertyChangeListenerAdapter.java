package org.purl.jh.feat.morph.mainview.filter;

import java.beans.PropertyChangeEvent;

/**
 * Listener adapter for events from quick filter.
 */
public class PropertyChangeListenerAdapter implements java.beans.PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case FilterModel.PROP_TEXT:
                textChanged((String) evt.getNewValue(), (String) evt.getOldValue());
                break;
            case FilterModel.PROP_MODE:
                modeChanged((FilterSpec.Mode)evt.getNewValue(), (FilterSpec.Mode)evt.getOldValue());
                break;
            case FilterModel.PROP_IGNOREDIA:
                ignoreDiaChanged((boolean) evt.getNewValue(), (boolean) evt.getOldValue());
                break;
            case FilterModel.PROP_CONTINUOUS:
                continuousChanged((boolean) evt.getNewValue(), (boolean) evt.getOldValue());
                break;
        }
    }

    protected void textChanged(String newValue, String oldValue) {
    }

    protected void modeChanged(FilterSpec.Mode newValue, FilterSpec.Mode oldValue) {
    }

    protected void ignoreDiaChanged(boolean newValue, boolean oldValue) {
    }
    
    protected void continuousChanged(boolean newValue, boolean oldValue) {
    }
} 