package org.purl.jh.feat.morph.mainview;

import com.google.common.base.Preconditions;
import org.purl.jh.feat.morph.mainview.sort.SortSpec;
import com.google.common.collect.ImmutableList;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import lombok.Data;
import lombok.Getter;
import org.purl.jh.feat.morph.mainview.filter.Filter;
import org.purl.jh.feat.morph.mainview.filter.FilterSpec;
import org.purl.jh.feat.morph.mainview.sort.Sorter;
import org.purl.jh.feat.morph.mainview.viewmodel.Item;
import org.purl.jh.feat.morph.mainview.viewmodel.TblModel;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.nbdata.view.Current;


/**
 * todo Separate view and model.
 * todo Separate LT specific stuff
 */
@Data
public class ItemList {
    private final static DefaultTableCellRenderer rightRenderer = new LeftDotRenderer(); 
    private final static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();

    static {
        rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
    }

    /** Specification of required sorting */
    @Getter private SortSpec sortSpec = new SortSpec();

    // todo provide fiter in a similar way
    
    private final javax.swing.JTable view;
    
    /** Items to show */
    private final List<Item> model;
    
    /** Comparator provider */
    private final Sorter sorter = new Sorter();

    /** Sorted model */
    private List<Item> sorted;

    /** Filtered sorted model */
    private List<Item> shown;

    public ItemList(JTable view, List<Item> model) {
        this.view = view;
        this.model = model;

        sorted = new ArrayList<>(this.model);
        shown = new ArrayList<>(sorted);

        view.setSelectionModel(new ItemsSelectionModel());
    }

    public ItemsSelectionModel getSelectionModel() {
        return (ItemsSelectionModel) view.getSelectionModel();
    }
    
    public LTx getMain() {
        final int mainIdx = view.getSelectionModel().getLeadSelectionIndex();
        return mainIdx == -1 ? null : shown.get(mainIdx).getCur(); 
    }
    
    /**
     * Returns the currently selected items.
     * @return 
     */
    public Current getCurrent() {
        List<LTx> selected = getSelectedItems();
        //System.out.println("getCurrent: " + Arrays.toString(view.getSelectedRows()));
        if (selected.isEmpty()) return Current.EMPTY;
        
        int leadIdx = view.getSelectionModel().getLeadSelectionIndex();
        if (leadIdx == -1) {
            leadIdx = view.getSelectionModel().getMaxSelectionIndex();
            System.err.printf("WARNING leadIdx == -1, setting to %d%n", leadIdx);
        }
        LTx leadItem = shown.get(leadIdx).getCur();
        
        if (selected.size() == 1) {
            return new Current(ImmutableList.of(leadItem), leadItem);
        }
        else {
            return new Current(getSelectedItems(), leadItem);
        }
    }

    
    public void setCurrent(Current cur) {
        final int mainIdx = Util.indexOf(shown, Item::getCur, cur.getMain());   // todo use guava's Iterables.indexOf ?
        //System.err.printf("setCurrent: %s; mainIdx %s%n", cur, mainIdx);

        if (cur.isSingleton() && cur.single() == cur.getMain()) {
            getSelectionModel().setSelectionInterval(mainIdx, mainIdx);
        }
        else {
            final List<Integer> items = Util.items2idxs(shown, item -> item.getCur(), cur.getItems());
            getSelectionModel().setSelection(items, mainIdx);
        }

        ensureRowVisible(mainIdx);
    }
    
    public void ensureRowVisible(final int rowIdx) {
        // see http://stackoverflow.com/a/20836212/264596 
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final Rectangle r = view.getCellRect(rowIdx, 0, true);
                 view.scrollRectToVisible(r);
            }
        });
        
    }
    
    public ImmutableList<LTx> getSelectedItems() {
        final ImmutableList.Builder<LTx> builder = ImmutableList.builder();
        for (int i : view.getSelectedRows()) {
            builder.add( shown.get(i).getCur() );
        }
        return builder.build();
    }

    public synchronized void sort(SortSpec sortSpec) {
        this.sortSpec = sortSpec;
        sorted = new ArrayList<>(model);
        
        final Comparator<Item> comp = sorter.comparator(sortSpec);
        if (comp != null) Collections.sort(sorted, comp);
    }
    
    public synchronized void filter(FilterSpec spec) {
        Preconditions.checkNotNull(spec);

        // --- remember current state (selection, column width) to restore it later ---
        final Current selection = getCurrent();
        //System.err.printf("filter: selection: %s%n", selection);
        final List<Integer> colWidths = JTableUtil.getColWidths(view);

        shown = Filter.filter(sorted, spec );
        view.setModel( new TblModel(shown) );

        view.getColumnModel().getColumn(1).setCellRenderer( rightRenderer );
        view.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
        view.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
        
        // --- restore state (selection, col width) ---
        JTableUtil.setColWidths(view, colWidths);
        setCurrent(selection);
    }

}