package org.purl.jh.feat.morph.mainview;

import java.util.Deque;

public interface HistoryBackProvider<Item> {
    void go(int i);
    Deque<Item> getBackItems();
}
