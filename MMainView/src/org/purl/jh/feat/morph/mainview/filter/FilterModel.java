package org.purl.jh.feat.morph.mainview.filter;

import com.google.common.base.Objects;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import lombok.Data;
import org.purl.jh.util.err.FormatError;

/**
 *
 */
@Data
public class FilterModel {
    public static final String PROP_TEXT = "text";
    public static final String PROP_MODE = "mode";
    public static final String PROP_IGNOREDIA = "ignoreDia";
    public static final String PROP_CONTINUOUS = "continuous";

    private String text = "";
    private boolean continuous;
   
    private FilterSpec spec = new FilterSpec();
 
    public boolean textIsValid() {
        try {
            Parser.parse(text, spec.mode == FilterSpec.Mode.REGEX);
        }
        catch (FormatError e) {
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param value 
     * @throws FormatError if the value does not have a correct format
     */
    public void setText(String value) {
        if ( Objects.equal(value, this.text)) return;
        String oldValue = this.text;
        this.text = value;

        // parser might throw an exception, than the spec is not updated
        Parser.FilterParts parts = Parser.parse(text, spec.mode == FilterSpec.Mode.REGEX);

        spec.prevTag = parts.prevTag;
        spec.prevWord = parts.prevWord;
        spec.curTag = parts.curTag;
        spec.curWord = parts.curWord;
        spec.nextTag = parts.nextTag;
        spec.nextWord = parts.nextWord;
        
        propertySupport.firePropertyChange(PROP_TEXT, oldValue, value);
    }

    public void setMode(FilterSpec.Mode value) {
        if (value == spec.getMode()) return;

        if (value == FilterSpec.Mode.REGEX) {
            setIgnoreDia(false);
        }
        
        FilterSpec.Mode oldValue = spec.getMode();
        spec.mode = value;
        propertySupport.firePropertyChange(PROP_MODE, oldValue, value);
    }

    
    public void setIgnoreDia(boolean value) {
        if (value == spec.ignoreDia) return;
        
        if (value && spec.mode == FilterSpec.Mode.REGEX) setMode(FilterSpec.Mode.EXACT);
        
        boolean oldValue = spec.ignoreDia;
        spec.ignoreDia = value;
        propertySupport.firePropertyChange(PROP_IGNOREDIA, oldValue, value);
    }

    public void setContinuous(boolean value) {
        if (value == this.continuous) return;
        
        boolean oldValue = this.continuous;
        this.continuous = value;
        propertySupport.firePropertyChange(PROP_CONTINUOUS, oldValue, value);
    }

    /** Fires events on all model changes */
    private PropertyChangeSupport propertySupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
}
 