package org.purl.jh.feat.morph.mainview;

import java.util.Deque;

public interface HistoryForwardProvider<Item> {
    void go(int i);
    Deque<Item> getForwardItems();
}
