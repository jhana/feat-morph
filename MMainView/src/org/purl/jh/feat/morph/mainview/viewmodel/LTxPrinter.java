package org.purl.jh.feat.morph.mainview.viewmodel;

import org.purl.jh.law.data.m.LTx;

/**
 *
 */
public class LTxPrinter {
    public static String form(LTx ltx) {
        return ltx.getBefore() == null ? ltx.getForm() : (ltx.getBefore() + " " + ltx.getForm());
    }
}
