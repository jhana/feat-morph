package org.purl.jh.feat.morph.mainview.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.purl.jh.feat.morph.mainview.Navigable;

public abstract class NavigateAction extends AbstractAction implements LookupListener, ContextAwareAction {
    private final Lookup context;
    protected Lookup.Result<Navigable> lkpInfo;
 
    public NavigateAction() {
        this(null);
    }
    
    
    protected NavigateAction(Lookup context) {
        this.context = context == null ? Utilities.actionsGlobalContext() : context;
        //putValue(Action.NAME, "ABC"); // FIXME: not sure why it does not use annotations
    }

    @Override
    public abstract Action createContextAwareInstance(Lookup context);
    
    protected void init() {
        assert SwingUtilities.isEventDispatchThread() : "this shall be called just from AWT thread";
 
        if (lkpInfo != null) return;
 
        //The thing we want to listen for the presence or absence of on the global selection
        lkpInfo = context.lookupResult(Navigable.class);
        lkpInfo.addLookupListener(this);
        resultChanged(null);
    }
 
    @Override
    public boolean isEnabled() {
        init();
        return super.isEnabled() && !lkpInfo.allInstances().isEmpty();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        init();
        
        // todo synchronized??
        if (lkpInfo.allInstances().isEmpty()) return;
        Navigable navigable = lkpInfo.allInstances().iterator().next();
        navigate(navigable);
    }

    protected abstract void navigate(Navigable navigable);
    
    
    @Override
    public void resultChanged(LookupEvent ev) {
    }

}