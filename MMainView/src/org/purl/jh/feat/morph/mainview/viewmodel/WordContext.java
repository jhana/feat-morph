package org.purl.jh.feat.morph.mainview.viewmodel;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Map;
import lombok.Data;
import lombok.Getter;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.MLayer;

/**
 * Class providing word contexts for all words at a particular layer
 * (both as a pointer into the layer's text and as left/right windows).
 * 
 * The textual representation of the data is shared by all contexts.
 */
public class WordContext {
    /** 
     * Left/Previous:
     * words: z..b a 
     * chars: z0..bn an.a0
     * 
     * Right/Following:
     * words: a b..z 
     * chars: a0.an b0..zn
     */
    @Data
    public static class Single {
        int leftZ0;
        int leftBn;
        int leftA0; 
        int leftAn;
                
        int rightA0; 
        int rightAn;
        int rightB0;
        int rightZn;
    }

    private final Map<LTx,Single> form2ctx;

    @Getter private final String text;

    @Getter private final int leftWindow;
    @Getter private final int rightWindow;
    
    public WordContext(MLayer mlayer, int leftWindow, int rightWindow) {
        this.leftWindow = leftWindow;
        this.rightWindow = rightWindow;
        
        CtxBuilder builder = new CtxBuilder(mlayer, leftWindow, rightWindow).build();
        text = builder.getText();
        form2ctx = builder.getForm2ctx();
    }

    public Single getCtx(LTx ltx) {
        final Single ctx = form2ctx.get(ltx);
        Preconditions.checkNotNull(ctx);
        return ctx;
    }

    /**
     * Returns the right context of a form.
     */
    public String getBeforeZA(LTx ltx) {
        final Single wordCtx = getCtx(ltx);
        return text.substring(wordCtx.leftZ0, wordCtx.leftAn);
    }
        
    public String getBeforeA(LTx ltx) {
        final Single wordCtx = getCtx(ltx);
        return text.substring(wordCtx.leftA0, wordCtx.leftAn);
    }

    public String getBeforeZB(LTx ltx) {
        final Single wordCtx = getCtx(ltx);
        return text.substring(wordCtx.leftZ0, wordCtx.leftBn);
    }
    
    /**
     * Returns the right context of a form.
     */
    public String getAfterAZ(LTx ltx) {
        final Single wordCtx = getCtx(ltx);
        return text.substring(wordCtx.rightA0, wordCtx.rightZn);
    }
        
    public String getAfterA(LTx ltx) {
        final Single wordCtx = getCtx(ltx);
        return text.substring(wordCtx.rightA0, wordCtx.rightAn);
    }

    public String getAfterBZ(LTx ltx) {
        final Single wordCtx = getCtx(ltx);
        return text.substring(wordCtx.rightB0, wordCtx.rightZn);
    }
}
