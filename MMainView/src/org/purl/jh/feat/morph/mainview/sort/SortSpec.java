package org.purl.jh.feat.morph.mainview.sort;

import lombok.Data;

/**
 * Class representing how the item list is sorted.
 */
@Data
public class SortSpec {
    public static enum What {
        BEFORE, WORD, AFTER, UNSORTED;
    }
    
    /** Primary sorting */
    private What first = What.UNSORTED;
    /** Compare Ascending or Descending; Ignored if first is unsorted */
    private boolean firstReversed;
    /** Compare by reversed string; ignored if first is unsorted,  */
    private boolean firstRetrogade;
    
    /** Secondary sorting */
    private What second = What.UNSORTED;
    /** Compare Ascending or Descending; Ignored if second is unsorted */
    private boolean secondReversed;
    /** Compare by reversed string; ignored if first is unsorted,  */
    private boolean secondRetrogade;
    
    public boolean valid() {
        if (first == What.UNSORTED) return second == What.UNSORTED;
        return first != second;
    }
}
