package org.purl.jh.feat.morph.mainview;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import java.util.Deque;
import java.util.LinkedList;
import lombok.Getter;

public abstract class History<Item> {
//    public static interface BackProvider<Item> {
//        void go(int i);
//        Deque<Item> getBackItems();
//    }
//
//    public static interface ForwardProvider<Item> {
//        void go(int i);
//        Deque<Item> getForwardItems();
//    }
    
    @Getter protected final Deque<Item> backItems = new LinkedList<>();
    @Getter protected Item cur;
    @Getter protected final Deque<Item> forwardItems = new LinkedList<>();
 
    public HistoryBackProvider<Item> createBackProvider() {
        return new HistoryBackProvider<Item>() {
            @Override
            public void go(int i) {
                History.this.go(i);
            }

            @Override
            public Deque<Item> getBackItems() {
                return History.this.getBackItems();
            }
        };
    }
    
    public HistoryForwardProvider<Item> createForwardProvider() {
        return new HistoryForwardProvider<Item>() {
            @Override
            public void go(int i) {
                History.this.go(i);
            }

            @Override
            public Deque<Item> getForwardItems() {
                return History.this.getForwardItems();
            }
        };
    }
    
    
    public  void go(int delta) {
        if (delta < 0) {
            delta = -delta;
            Preconditions.checkArgument(delta <= backItems.size());

            if (cur != null) forwardItems.push(cur);
            for (int i = 0; i < delta-1; i++) {
                forwardItems.push(backItems.pop());
            }
            cur = backItems.pop();
        }
        else if (delta > 0) {
            Preconditions.checkArgument(delta <= forwardItems.size());

            if (cur != null) backItems.push(cur);
            for (int i = 0; i < delta-1; i++) {
                backItems.push(forwardItems.pop());
            }
            cur = forwardItems.pop();
        }
        else {
            Preconditions.checkArgument(false);
        }

        goTo(cur);
        onHistoryChanged();
    }

    protected abstract void goTo(Item item);
    
    public void record(Item newOne) {
        if (newOne == cur || newOne.equals(cur)) return;
            
        forwardItems.clear();
        if (cur != null) backItems.push(cur);
        cur = newOne;
        onHistoryChanged();
    }

    protected void onHistoryChanged() {
    }
    
    
    @Override
    public String toString() {
        return "[Back: " + backItems.size() + "\n" + Joiner.on('\n').join( backItems) + "]\n" +
                "\nCur: " + cur.hashCode() + " " + cur +  " / \n" +
                "[Forward: " + + forwardItems.size() + "\n" + Joiner.on('\n').join(forwardItems) + "]";
    }
}
