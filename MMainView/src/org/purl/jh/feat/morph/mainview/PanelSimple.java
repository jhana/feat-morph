package org.purl.jh.feat.morph.mainview;

import org.purl.jh.feat.morph.mainview.viewmodel.ItemsBuilder;
import org.purl.jh.feat.morph.mainview.viewmodel.Item;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.lookup.InstanceContent;
import org.purl.jh.feat.morph.mainview.filter.FilterSpec;
import org.purl.jh.feat.morph.mainview.sort.SortPanel;
import org.purl.jh.feat.morph.mainview.sort.SortSpec;
import org.purl.jh.feat.morph.util.MNbUtil;
import org.purl.jh.law.data.event.MLayerEvent;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Lt;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.nbdata.view.Current;
import org.purl.jh.pml.event.DataEvent;
import org.purl.jh.pml.event.DataListener;
import org.purl.jh.util.gui.StdDialog;
import org.purl.net.jh.nbutil.NbUtil;

/**
 * The glazzed-list based view is way to slow. 
 */
public class PanelSimple extends JPanel implements DataListener {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(PanelSimple.class);
    
    private ItemList itemListx;
    
    /** Contains at least the hint and the current location, current items, history  */
    protected Lookup lookup;
    protected InstanceContent lookupContent;

    protected MLayer layer;
    
    protected Current current = Current.EMPTY;

    public PanelSimple() {
        initComponents();
        
        NbUtil.adjustToFont(itemList, 1.15f);
        
        filterBox.getIgnoreDiaButton().setEnabled(false); // not supported yet
        filterBox.addActionListener(e -> updateNavigatorModelRun());        

        statusReviewToggle.setSelected(true);
        statusTodoToggle.setSelected(true);
    }

    
    public void init(final MLayer layer, Lookup lookup, InstanceContent lookupContent) {
        Preconditions.checkNotNull(layer);
        loadProperties();

        this.lookupContent = lookupContent;
        this.lookup = lookup;

        this.layer = layer;
        lookupContent.add(layer);
        lookupContent.add(MNbUtil.createNavigatorHint("text/pdt-m+xml"));
        //lookupContent.add(history);
        layer.addChangeListener(this);


        SwingUtilities.invokeLater(() -> {
                List<Item> items = ItemsBuilder.build(layer, 10, 10);

                itemListx = new ItemList(itemList, items);  
                itemListx.getSelectionModel().addListSelectionListener((ListSelectionEvent e) -> {
                        if (itemList.getSelectedRow() == -1 || (itemList.getRowCount() <= itemList.getSelectedRow()) ) return;

                        selected(itemListx.getCurrent(), false);
                    }
                );

                refreshSort();
            }
        );
        
        
        lookupContent.add(new Navigable() {
            @Override
            public void gotoIdx(int idx) {
            }

            @Override
            public void gotoLast() {
            }

            @Override
            public void move(int offset) {
                selectNext(offset);
            }
        });
    }

    
    @Override
    public boolean requestFocusInWindow() {
        return this.itemList.requestFocusInWindow();
    }    
    
    protected synchronized void refreshSort() {
        refreshSort(itemListx.getSortSpec());
    }
    
    protected synchronized void refreshSort(SortSpec sortSpec) {
        itemListx.sort(sortSpec);
        updateNavigatorModel();
    }

    protected void updateNavigatorModelRun() {
            updateNavigatorModel();
//        SwingUtilities.invokeLater(new Runnable() { // todo cancel if resultChanged again (ineffective + racing condition)
//            @Override
//            public void run() {
//                updateNavigatorModel();
//            }
//        });
    }
    
    protected synchronized void updateNavigatorModel() {
        final FilterSpec filterSpec = filterBox.getModel().getSpec(); 
        filterSpec.setAmbi(ambiToggle.isSelected());
        filterSpec.setStatus_todo(statusTodoToggle.isSelected());
        filterSpec.setStatus_review(statusReviewToggle.isSelected());
        
        itemListx.filter(filterSpec);
        updateStatistics();
    }
    
    
    // todo under development - todo publish as an action
    public  void selectNext(int offset) {
        // todo later for multiple items as well, currently only for single items
        // todo via itemList or via curLocs?\ if (curLocs.isSingleton()) {

        if (itemList.getSelectedRowCount() == 1) {

            int idx = itemList.getSelectedRow();
            //LTs curLtx = filteredItems.get(idx);
            idx += offset;
            if (0 <= idx && idx < itemListx.getShown().size()) {
                LTx newLtx = itemListx.getShown().get(idx).getCur();
                Current cur = new Current(ImmutableList.of(newLtx), newLtx);
                itemListx.setCurrent(current);
                selected(cur, false);
            }
        }
    }

    private void goTo(Current cur) {
        itemListx.setCurrent(cur);
        selected(cur, false);
    }
    
    /** Navigation history to support back/forward navigation */ 
    private final History<Current> history = new XHistory();
    /** Support for back historynavigation, to put on lookup to enable any back navigation controls */
    private final HistoryBackProvider<Current> backHistoryProvider = history.createBackProvider();
    /** Support for forward history navigation, to put on lookup to enable any forward navigation controls */
    private final HistoryForwardProvider<Current> forwardHistoryProvider = history.createForwardProvider();
    
    /** Class tracking navigation history and modifying lookup as needed to enable/disable any listening gui controls*/
    private class XHistory extends History<Current> {
        @Override
        protected void goTo(Current item) {
            PanelSimple.this.goTo(item);
        }

        @Override
        protected void onHistoryChanged() {
            if (backItems.isEmpty()) {
                lookupContent.remove(backHistoryProvider);
            }
            else {
                lookupContent.add(backHistoryProvider);
            }
            
            if (forwardItems.isEmpty()) {
                lookupContent.remove(forwardHistoryProvider);
            }
            else {
                lookupContent.add(forwardHistoryProvider);
            }
        }
    };
    
    /**
     * Respond to a change of item selection.
     * 
     * @param current the currently selected elements
     * @param focus should the first item in TC be focused or just selected
     */
    protected void selected(final Current current, final boolean focus) {
        final Current oldCurrent = this.current;
        layer.removeChangeListener(oldCurrent);

        this.current = current; 
        layer.addChangeListener(current);

        MNbUtil.replaceInLookup(lookupContent, oldCurrent, current);
        history.record(current);

        updateStatistics();
    }
    
    private void updateStatistics() {
        statisticsText.setText(String.format("All: %d Filtered %d Selected: %d", itemListx.getModel().size(), itemListx.getShown().size(), current.size() ) );
    }

    @Override
    public void handleChange(DataEvent<?> aE) {
        boolean refresh = false;

        if (
                ImmutableList.of(MLayerEvent.cSetForm, MLayerEvent.cMultiSetForm, MLayerEvent.cOther)
                    .contains(aE.getId())
            ) {
            refresh = true;
        }
        else if (filterBox.getModel().getSpec().isAmbi()) {
            refresh = 
                    ((MLayerEvent)aE).ambiChanged ||
                    ImmutableList.of(MLayerEvent.cMultiLtxAdd, MLayerEvent.cMultiLtxDel, MLayerEvent.cMultiLtxSelect)
                        .contains(aE.getId());
        }
        
        if (refresh) {
            Current cur = itemListx.getCurrent();
            System.out.println("X Current " + cur);
            updateNavigatorModelRun();
            itemListx.setCurrent(cur);
        }
    }
    
    
    private Preferences getPrefs() {
        return NbPreferences.forModule(getClass());
    } 

    void loadProperties() {
        final TableColumnModel colModel = itemList.getColumnModel();
        
        //getPrefs().putInt("colCount", colModel.getColumnCount());
        for (int colIdx = 0; colIdx < colModel.getColumnCount(); colIdx++) {
            TableColumn col = colModel.getColumn(colIdx);
            int width = getPrefs().getInt("col_size_" + colIdx, col.getPreferredWidth());
            col.setPreferredWidth(width);
        }
    }
    
    void saveProperties() {
        final TableColumnModel colModel = itemList.getColumnModel();
        
        getPrefs().putInt("colCount", colModel.getColumnCount());
        for (int colIdx = 0; colIdx < colModel.getColumnCount(); colIdx++) {
            TableColumn col = colModel.getColumn(colIdx);
            getPrefs().putInt("col_size_" + colIdx, col.getPreferredWidth());
        }
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        itemList = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        filterBox = new org.purl.jh.feat.morph.mainview.filter.QuickFilterBox();
        ambiToggle = new javax.swing.JToggleButton();
        statusTodoToggle = new javax.swing.JToggleButton();
        statusReviewToggle = new javax.swing.JToggleButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        sortButton = new javax.swing.JButton();
        selectSimilarBtn = new javax.swing.JButton();
        statisticsText = new javax.swing.JTextField();

        itemList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Page", "Left context", "Previous", "Word", "Following", "Following context"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(itemList);
        if (itemList.getColumnModel().getColumnCount() > 0) {
            itemList.getColumnModel().getColumn(0).setPreferredWidth(10);
            itemList.getColumnModel().getColumn(0).setHeaderValue(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.itemList.columnModel.title5")); // NOI18N
            itemList.getColumnModel().getColumn(1).setHeaderValue(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.itemList.columnModel.title0")); // NOI18N
            itemList.getColumnModel().getColumn(2).setHeaderValue(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.itemList.columnModel.title1")); // NOI18N
            itemList.getColumnModel().getColumn(3).setHeaderValue(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.itemList.columnModel.title2")); // NOI18N
            itemList.getColumnModel().getColumn(4).setHeaderValue(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.itemList.columnModel.title3")); // NOI18N
            itemList.getColumnModel().getColumn(5).setHeaderValue(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.itemList.columnModel.title4")); // NOI18N
        }

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        filterBox.setFloatable(false);
        jToolBar1.add(filterBox);

        org.openide.awt.Mnemonics.setLocalizedText(ambiToggle, org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.ambiToggle.text")); // NOI18N
        ambiToggle.setToolTipText(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.ambiToggle.toolTipText")); // NOI18N
        ambiToggle.setFocusable(false);
        ambiToggle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ambiToggle.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ambiToggle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ambiToggleActionPerformed(evt);
            }
        });
        jToolBar1.add(ambiToggle);

        org.openide.awt.Mnemonics.setLocalizedText(statusTodoToggle, org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.statusTodoToggle.text")); // NOI18N
        statusTodoToggle.setFocusable(false);
        statusTodoToggle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        statusTodoToggle.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        statusTodoToggle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusTodoToggleActionPerformed(evt);
            }
        });
        jToolBar1.add(statusTodoToggle);

        org.openide.awt.Mnemonics.setLocalizedText(statusReviewToggle, org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.statusReviewToggle.text")); // NOI18N
        statusReviewToggle.setFocusable(false);
        statusReviewToggle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        statusReviewToggle.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        statusReviewToggle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusReviewToggleActionPerformed(evt);
            }
        });
        jToolBar1.add(statusReviewToggle);
        jToolBar1.add(jSeparator1);

        org.openide.awt.Mnemonics.setLocalizedText(sortButton, org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.sortButton.text")); // NOI18N
        sortButton.setFocusable(false);
        sortButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sortButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        sortButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(sortButton);

        org.openide.awt.Mnemonics.setLocalizedText(selectSimilarBtn, org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.selectSimilarBtn.text")); // NOI18N
        selectSimilarBtn.setFocusable(false);
        selectSimilarBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        selectSimilarBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        selectSimilarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectSimilarBtnActionPerformed(evt);
            }
        });
        jToolBar1.add(selectSimilarBtn);

        statisticsText.setEditable(false);
        statisticsText.setText(org.openide.util.NbBundle.getMessage(PanelSimple.class, "PanelSimple.statisticsText.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
            .addComponent(statisticsText)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statisticsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ambiToggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ambiToggleActionPerformed
        updateNavigatorModelRun(); 
    }//GEN-LAST:event_ambiToggleActionPerformed

    private void selectSimilarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectSimilarBtnActionPerformed
        if (itemList.getSelectedRowCount() != 1) return;

        final int sampleIdx = itemList.getSelectedRow();
        final LTx sample = itemListx.getShown().get(sampleIdx).getCur(); 

        //todo ReadWriteLock
        final List<Integer> similar = new ArrayList<>();
        for (int i = 0; i < itemListx.getShown().size(); i++) {
            LTx ltx = itemListx.getShown().get(i).getCur();
            if (compatible(sample, ltx)) similar.add(i);
        }

        itemListx.getSelectionModel().setSelection(similar, sampleIdx);
    }//GEN-LAST:event_selectSimilarBtnActionPerformed

    private void sortButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortButtonActionPerformed
        SortPanel panel = new SortPanel();
        panel.init(itemListx.getSortSpec());

        StdDialog dlg = new StdDialog("Choose the sorting order of columns", true) {
            @Override
            protected JPanel createMainPanel() {
                return panel;
            }
        };

        if (!dlg.accepted()) return;

        refreshSort(panel.getSorting());
    }//GEN-LAST:event_sortButtonActionPerformed

    private void statusReviewToggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusReviewToggleActionPerformed
        updateNavigatorModelRun(); 
    }//GEN-LAST:event_statusReviewToggleActionPerformed

    private void statusTodoToggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusTodoToggleActionPerformed
        updateNavigatorModelRun(); 
    }//GEN-LAST:event_statusTodoToggleActionPerformed

    
    
    
    /**
     * 
     * Considers only lemmas and tags. 
     * The items must be equally ordered.
     * Selection is ignored
     * 
     * @return false if the selection is the same
     */
    private static boolean compatible(final LTx one, final LTx two) {
        if (!one.getForm().equals(two.getForm())) return false;
        if (one.size() != two.size()) return false;
         
        for (int i = 0; i < one.size(); i++) {
            Lt a = one.get(i);
            Lt b = two.get(i);
            
            if (! a.equalsOnItem(b)) return false;
        }
        
        return true;
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton ambiToggle;
    private org.purl.jh.feat.morph.mainview.filter.QuickFilterBox filterBox;
    private javax.swing.JTable itemList;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton selectSimilarBtn;
    private javax.swing.JButton sortButton;
    private javax.swing.JTextField statisticsText;
    private javax.swing.JToggleButton statusReviewToggle;
    private javax.swing.JToggleButton statusTodoToggle;
    // End of variables declaration//GEN-END:variables



}
