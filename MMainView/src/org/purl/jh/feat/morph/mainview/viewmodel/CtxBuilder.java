package org.purl.jh.feat.morph.mainview.viewmodel;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.purl.jh.feat.morph.mainview.viewmodel.WordContext.Single;
import org.purl.jh.law.data.m.Doc;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.data.m.Para;
import org.purl.jh.law.data.m.Sentence;

/**
 * Creates information for the WordContext class base on MLayer
 * @author jirka
 */
@RequiredArgsConstructor
class CtxBuilder {
    private static final List<String> NO_SPACE_BEFORE = ImmutableList.of(".", "\"", ",", "?", "!", ":", ";");

    private final MLayer mlayer;
    private final int leftWindow1;
    private final int rightWindow1;

    private final List<Integer> wordStarts = new ArrayList<>();
    private final List<Integer> wordEnds = new ArrayList<>();
    private final Map<LTx,Integer> form2idx = new HashMap<>();

    // result
    @Getter private String text;
    @Getter private final Map<LTx,Single> form2ctx = new HashMap<>();


    public CtxBuilder build() {
        text = collectText();
        calculateCtxs();
        return this;
    }

    /** Creates a textual representation of the layer, calculates form starts/ends */
    private String collectText() {
        final StringBuilder sb = new StringBuilder();

        for (Doc doc : mlayer.getDocs()) {
            for (Para para : doc.getParas()) {
                for (Sentence sent : para.getSentences()) {
                    for (LTx ltx : sent.getLtxs()) {

                        if (!NO_SPACE_BEFORE.contains(ltx.getForm())) {
                            sb.append(' ');
                        }

                        form2idx.put(ltx, form2idx.size());
                        wordStarts.add(sb.length());
                        sb.append(LTxPrinter.form(ltx));
                        wordEnds.add(sb.length());
                    }
                    sb.append(' ');     // double space between sentences
                }

                sb.append(" <p> ");
            }
            sb.append(" <doc>  ");
        }
        
        return sb.toString();
    }

    void calculateCtxs() {
        final int n = wordEnds.size() - 1;

        for (LTx ltx : mlayer.getFlatLts()) {
            final int formIdx = form2idx.get(ltx);
            WordContext.Single ctx = new WordContext.Single();  // all initialized to zero 
            form2ctx.put(ltx, ctx);
            if (formIdx != 0) {
                // words: z...b a 
                final int a = minLimit(formIdx - 1, 0);
                final int b = minLimit(formIdx - 2, 0);
                final int z = minLimit(formIdx - leftWindow1, 0);
                if (a < formIdx) {
                    ctx.setLeftA0( wordStarts.get(a) );
                    ctx.setLeftAn( wordEnds.get(a) );
                }
                if (b < a) {
                    ctx.setLeftZ0( wordStarts.get(z) );
                    ctx.setLeftBn( wordEnds.get(b) );
                }
            }
            if (formIdx < n) {
                // words: z...b a 
                final int a = maxLimit(formIdx + 1, n);
                final int b = maxLimit(formIdx + 2, n);
                final int z = maxLimit(formIdx + rightWindow1, n);
                if (a < n) {
                    ctx.setRightA0( wordStarts.get(a) );
                    ctx.setRightAn( wordEnds.get(a) );
                }
                if (a < b) {
                    ctx.setRightB0( wordStarts.get(b) );
                    ctx.setRightZn( wordEnds.get(z) );
                }
            }
        }
    }

    private static int minLimit(int nr, int min) {
        return nr < min ? min : nr;
    }

    private static int maxLimit(int nr, int max) {
        return max < nr ? max : nr;
    }
}
