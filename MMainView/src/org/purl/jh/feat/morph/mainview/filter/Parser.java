package org.purl.jh.feat.morph.mainview.filter;

import org.purl.jh.feat.morph.util.MNbUtil;
import org.purl.jh.util.err.FormatError;

/**
 * Parses text specification of the filter. 
 */
public class Parser {
    
    public static class FilterParts {
        String curTag;
        String prevWord;
        String prevTag;
        String nextWord;
        String nextTag;
        String curWord;
    }    
    
    public static FilterParts parse(String text, boolean regex) throws FormatError {
        if (text == null || text.isEmpty()) return new FilterParts();

        final FilterParts parts = tokenize(text);
        
        if (regex) {
            checkRegex(parts.prevWord);
            checkRegex(parts.prevTag);
            checkRegex(parts.curWord);
            checkRegex(parts.curTag);
            checkRegex(parts.nextWord);
            checkRegex(parts.nextTag);
        }

        return parts;
    }
    
    private static void checkRegex(String str) {
        if (str != null && !MNbUtil.checkRegex(str)) {
            throw new FormatError("Regex error.");
        }
    }
    
    private static FilterParts tokenize(String text) throws FormatError {
        String[] tokens = text.split("\\s+");

        final FilterParts parts = new FilterParts();
        
        for (String token : tokens) {
            if (token.startsWith("t:")) {
                if (parts.curTag != null) throw new FormatError("Duplicate current tag");
                parts.curTag = token.substring(2);
            }
            else if (token.startsWith("p:")) {
                if (parts.prevWord != null) throw new FormatError("Duplicate previous word");
                parts.prevWord = token.substring(2);
            }
            else if (token.startsWith("pt:")) {
                if (parts.prevTag != null) throw new FormatError("Duplicate previous tag");
                parts.prevTag = token.substring(3);
            }
            else if (token.startsWith("n:")) {
                if (parts.nextWord != null) throw new FormatError("Duplicate next word");
                parts.nextWord = token.substring(2);
            }
            else if (token.startsWith("nt:")) {
                if (parts.nextTag != null) throw new FormatError("Duplicate next tag");
                parts.nextTag = token.substring(3);
            }
            else {
                if (parts.curWord != null) throw new FormatError("Duplicate current word");
                parts.curWord = token;
            }
        }
        
        return parts;
    }
    
}
