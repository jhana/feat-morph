package org.purl.jh.feat.morph.mainview;

/** 
 * An object (e.g. a GUI element) that can show an item from withing an list of items. 
 * Methods of this interface can be called to display the desired item.
 * <p>
 * Example of classes that can implement this interface: 
 * <ul>
 * <li>A form displaying a row from a database 
 * <li>A list that displays multiple items, one of them selected as current.
 * </ul>
 */
public interface Navigable {
    void gotoIdx(int idx);
    void gotoLast();
    void move(int offset);
}
