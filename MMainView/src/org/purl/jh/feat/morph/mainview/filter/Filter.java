package org.purl.jh.feat.morph.mainview.filter;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.purl.jh.feat.morph.mainview.viewmodel.Item;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.Status;
import org.purl.jh.util.err.XException;

/**
 *
 * @author Jirka
 */
public class Filter {
    public static List<Item> filter(List<Item> items, FilterSpec spec) {
        Preconditions.checkNotNull(items);
        Preconditions.checkNotNull(spec);
        // todo diacritics
        
        Predicate<Item> prevWordP = wordPredicate(Item::getPrev, spec.getPrevWord(), spec);
        Predicate<Item> curWordP  = wordPredicate(Item::getCur,  spec.getCurWord(),  spec);
        Predicate<Item> nextWordP = wordPredicate(Item::getNext, spec.getNextWord(), spec);
                
        Predicate<Item> prevTagP = tagPredicate(Item::getPrev, spec.getPrevTag(), spec);
        Predicate<Item> curTagP  = tagPredicate(Item::getCur,  spec.getCurTag(),  spec);
        Predicate<Item> nextTagP = tagPredicate(Item::getNext, spec.getNextTag(), spec);
        
        Predicate<Item> ambiP = spec.isAmbi() ? item -> item.getCur().isAmbivalent() : Predicates.alwaysTrue();

        // status buttons - connected by or 
        Predicate<Item> status_todoP   = spec.isStatus_todo()   ? Predicates.alwaysTrue() : item -> item.getCur().getStatus() != Status.TODO;
        Predicate<Item> status_reviewP = spec.isStatus_review() ? Predicates.alwaysTrue() : item -> item.getCur().getStatus() != Status.TOREVIEW;
        
        Predicate<Item> predicate = Predicates.and(
                prevWordP, prevTagP, 
                curWordP, curTagP,
                nextWordP, nextTagP,
                ambiP,
                status_todoP, status_reviewP
        );
        
        return items.stream()
            .filter(x -> predicate.apply(x))
            .collect(Collectors.toList());
    }

    public static Predicate<Item> wordPredicate(Function<Item,LTx> f, String specText, FilterSpec spec) {
        if (specText == null || specText.isEmpty()) return Predicates.alwaysTrue();
        
        switch (spec.getMode()) {
            case EXACT: 
                return item -> {
                    LTx ltx = f.apply(item);
                    return ltx == null ? false : ltx.getForm().equals(specText);
                };
            case CONTAINS: 
                return item -> {
                    LTx ltx = f.apply(item);
                    return ltx == null ? false : ltx.getForm().contains(specText);
                };
            case PREFIX:
                return item -> {
                    LTx ltx = f.apply(item);
                    return ltx == null ? false : ltx.getForm().startsWith(specText);
                };
            case REGEX: 
                Pattern pattern = Pattern.compile(specText);
                return item -> {
                    LTx ltx = f.apply(item);
                    return ltx == null ? false : pattern.matcher(ltx.getForm()).matches();
                };
        }
        throw new XException("Should never got here");
    }
    
    public static Predicate<Item> tagPredicate(Function<Item,LTx> f, String specText, FilterSpec spec) {
        if (specText == null || specText.isEmpty()) return Predicates.alwaysTrue();

        switch (spec.getMode()) {
            // prefix is used also for exact and contains
            case EXACT: 
//                return item -> {
//                    LTs ltx = f.apply(item);
//                    return ltx == null ? false : 
//                            ltx.col().stream().anyMatch(lt -> (lt.getTag().equals(specText)));
//                };
            case CONTAINS: 
//                return item -> {
//                    LTs ltx = f.apply(item);
//                    return ltx == null ? false : 
//                            ltx.col().stream().anyMatch(lt -> (lt.getTag().contains(specText)));
//                };
            case PREFIX:
                return item -> {
                    LTx ltx = f.apply(item);
                    return ltx == null ? false : 
                            ltx.col().stream().anyMatch(lt -> (lt.getTag().startsWith(specText)));
                };
            case REGEX: 
                Pattern pattern = Pattern.compile(specText);
                return item -> {
                    LTx ltx = f.apply(item);
                    return ltx == null ? false :
                            ltx.col().stream().anyMatch(lt -> pattern.matcher(lt.getTag()).matches());
                };
        }
        throw new XException("Should never got here");
    }
}
