package org.purl.jh.feat.morph.mainview.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@ActionID(
        category = "Navigate",
        id = "org.purl.jh.feat.morph.mainview.actions.FocusWordsAction"
)
@ActionRegistration(
        displayName = "#CTL_FocusWordsAction"
)
@ActionReferences({
    @ActionReference(path = "Menu/Navigate", position = 200),
    @ActionReference(path = "Shortcuts", name = "D-W")
})
@Messages("CTL_FocusWordsAction=Go To &Words")
public final class FocusWordsAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        TopComponent da = findTopComponent();
        if (da != null) {
            da.open();
            da.requestActive();
            da.requestFocusInWindow();
        }
    }

    // todo move to nbutil
    private TopComponent findTopComponent() {
        for (TopComponent tc : WindowManager.getDefault().getRegistry().getOpened()) {
            if (tc instanceof org.purl.jh.feat.morph.mainview.TopComponent) {
                return tc;
            }
        }
        return null;
    }
}
