package org.purl.jh.feat.morph.mainview;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;

public class JTableUtil {
    public static void ensureRowVisible(JTable tbl, int idx) {
        tbl.scrollRectToVisible(tbl.getCellRect(idx, -1, true));
    }
    
    public static List<Integer> getColWidths(JTable tbl) {
        List<Integer> colWidths = new ArrayList<>();
        for (int c = 0; c < tbl.getColumnCount(); c++) {
            colWidths.add( tbl.getColumnModel().getColumn(0).getWidth() );
        }
        return colWidths;
    }

    public static void setColWidths(JTable tbl, List<Integer> colWidths) {
        for (int c = 0; c < tbl.getColumnCount(); c++) {
            tbl.getColumnModel().getColumn(c).setPreferredWidth(colWidths.get(c));
        }    
    }
}
