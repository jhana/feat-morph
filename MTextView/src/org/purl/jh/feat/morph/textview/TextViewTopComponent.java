package org.purl.jh.feat.morph.textview;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;
import org.purl.jh.law.data.m.Doc;
import org.purl.jh.law.data.m.LTx;
import org.purl.jh.law.data.m.MLayer;
import org.purl.jh.law.data.m.Para;
import org.purl.jh.law.data.m.Sentence;
import org.purl.jh.law.nbdata.view.ViewConnector;
import org.purl.jh.pml.event.DataEvent;
import org.purl.jh.pml.event.DataListener;
import org.purl.jh.util.col.IntInt;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
        dtd = "-//org.purl.jh.feat.morph.textview//TextView//EN",
        autostore = false)
@TopComponent.Description(
        preferredID = "TextViewTopComponent",
        iconBase = "org/purl/jh/feat/morph/textview/icons/text_area.png",
        persistenceType = TopComponent.PERSISTENCE_NEVER)
@TopComponent.Registration(mode = "output", openAtStartup = true)
@ActionID(category = "Window", id = "org.purl.jh.feat.morph.textview.TextViewTopComponent")
@ActionReference(path = "Menu/Window" , position = 10)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_TextViewAction",
        preferredID = "TextViewTopComponent")
@Messages({
    "CTL_TextViewAction=TextView",
    "CTL_TextViewTopComponent=TextView Window",
    "HINT_TextViewTopComponent=This is a TextView window"
})
public final class TextViewTopComponent extends TopComponent implements DataListener {
    private final int maxSelectedItems = 10;
    
    private String docText;
    
    private final ViewConnector<MLayer> connector = new ViewConnector<MLayer>(MLayer.class, "text/pdt-m+xml") {
        @Override
        public void layerChanged() {
            setText();
        }

        @Override
        public void locChanged() {
            removeOldHighlight();
           
            List<LTx> ltxs = getCurLocs().getItems();
            LTx main = getCurLocs().getMain();
            
            //oneLiner.setText("");
            if ( ltxs.size() < maxSelectedItems) { // it would take way too long
                //if (!ltxs.isEmpty()) setOneLiner(ltxs.iterator().next());
                
                for (LTx lts : ltxs) {
                    highlightInText( lts );
                }

            }
            else {
                highlightInText( main );
            }
            
            if (main != null) scroll(main);
        }
    };
    
    private Map<LTx,IntInt> form2pos;
    
    private void scroll(LTx item) {
        IntInt pos_len = this.form2pos.get(item);

        if (pos_len == null) return;

        int end = pos_len.mFirst + pos_len.mSecond;
        
        Rectangle rect = null;
        try {
             rect = docPane.modelToView(end);
        } catch (BadLocationException ex) {
            Exceptions.printStackTrace(ex);
        }
        if (rect != null) {
            // increase the size of the rectangle so that we show some surrounding context of the word
            rect.grow(0, 2*rect.height);    
            if (rect.y < 0) rect.y = 0;
            docPane.scrollRectToVisible(rect);
        }
        
    }
    
    public TextViewTopComponent() {
        initComponents();
        highlightColor = UIManager.getDefaults( ).getColor( "EditorPane.selectionBackground" );
        myHighlightPainter = new MyHighlightPainter(highlightColor);

        setName(Bundle.CTL_TextViewTopComponent());
        setToolTipText(Bundle.HINT_TextViewTopComponent());
        connector.init();

        associateLookup(connector.getLookup());
    }

    /**
     * Reaction to model change.
     * @param aChangeEvent 
     */
    @Override
    public void handleChange(DataEvent aChangeEvent) {
        setText();
    }

    
    private final Pattern noSpaceBefore = Pattern.compile("[\\.,;)\\]:!?“]");
    private final Pattern noSpaceAfter = Pattern.compile("[\\(\\[„]");
    // simple quotes are not handled 
    
    private void setText() {
        final StringBuilder sb = new StringBuilder();
        form2pos = new HashMap<>();

        if (connector.getCurLayer() != null) {
            for (Doc doc : connector.getCurLayer().getDocs()) {
                for (Para para : doc.getParas()) {
                    boolean start = true;
                    boolean spaceAfter = true;

                    for (Sentence sent : para.getSentences()) {
                        for (LTx ltx : sent.getLtxs()) {
                            final String form =  ltx.getForm();
                            String formStr = LTxPrinter.form(ltx);

                            if (start) {
                                start = false;
                            }
                            else {
                                if (spaceAfter && !noSpaceBefore.matcher(form).matches())
                                    sb.append(' ');
                            }


                            form2pos.put(ltx, new IntInt(sb.length(), formStr.length()));
                            sb.append(formStr);
                            spaceAfter = !noSpaceAfter.matcher(formStr).matches();
                        }
                    }

                    sb.append("\n\n");
                }
            }
        }
        
        docText = sb.toString();
        
        this.docPane.setText(docText);
    }
        
    
//    private void setOneLiner(final LTx ltx) {
//        IntInt pos_len = this.form2pos.get(ltx);
//        if (pos_len == null) return;
//        
//        int from = Math.max(0, pos_len.mFirst - 20);
//        int to =   Math.min(docText.length(), pos_len.mFirst + pos_len.mSecond + 20);
//        
//        oneLiner.setText(this.docText.substring(from, to));
//    }

// =============================================================================
// Highlighting
// todo coppied fromm LayeredViewTopComponent, reuse
// =============================================================================
    private final Color highlightColor;

    private final Highlighter.HighlightPainter myHighlightPainter;

    static class MyHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {
        public MyHighlightPainter(Color color) {
            super(color);
        }
    }

    private void highlightInText(final LTx ltx) {
        final Highlighter hiliter = docPane.getHighlighter();

        IntInt pos_len = this.form2pos.get(ltx);

        if (pos_len == null) return;

        // --- add extra visibility for short words ---
//            if (pos_len.mSecond < 3) {  // todo precalculate?
//                if (pos_len.mFirst > 0) start--;
//                if (end < jTextPane1.getDocument().getLength() - 1) end++;
//            }
        int end = pos_len.mFirst + pos_len.mSecond;

        try {
            hiliter.addHighlight(pos_len.mFirst, end, myHighlightPainter);
        } catch (BadLocationException ex) {
            Exceptions.printStackTrace(ex);
        }
    }


    // Removes only our private highlights
    public void removeOldHighlight() {
        Highlighter hiliter = docPane.getHighlighter();

        for (Highlighter.Highlight hilite : hiliter.getHighlights()) {
            if (hilite.getPainter() instanceof MyHighlightPainter) {
                hiliter.removeHighlight(hilite);
            }
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        docPane = new javax.swing.JTextPane();

        jScrollPane1.setViewportView(docPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane docPane;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }
}
